<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();

class LoginController extends Controller
{
    public function AuthLogin(){
        $id = Session::get('user_id');
        return Redirect::route('home');
    }
    public function index(){
    	return view('web.login');
    }
    public function dashboard(Request $request){
    	$user_email = $request->user_email;
    	$user_password = md5($request->user_password);
       
    	$result = DB::table('user')->where('user_email',$user_email)->where('user_password',$user_password)-> first();
    	if($result){
            Session::put('user_name',$result->user_name);
            Session::put('user_id',$result->user_id);
            Session::put('user_email',$result->user_email);          
            Session::put('user_img',$result->user_img);
            return Redirect::route('home');
        }else{
            Session::put('message','Mật khẩu hoặc tài khoản bị sai.Làm ơn nhập lại');
            return Redirect::route('login');
        }

    }
    public function adduser(){
        $this->AuthLogin();
    	return view('web.register'); 
    }
    public function postUser(Request $request){
        $this->AuthLogin();
       $data = array();
   
       $data['user_name'] = $request->user_name;
       $data['user_birthday'] = $request->user_birthday;
       $data['user_address'] = $request->user_address;
       $data['user_email'] = $request->user_email;
       $data['user_password'] = md5($request->user_password);
       $data['user_phone'] = $request->user_phone;
       $data['user_sex'] = $request->user_sex;
       $data['facebook'] = $request->facebook;
     
       $get_image = $request->file('user_img');
     
       if($get_image){
           $get_name_image = $get_image->getClientOriginalName();
           $name_image = current(explode('.',$get_name_image));
           $new_image =  $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
           $get_image->move('public/website/uploads/user',$new_image);
           $data['user_img'] = $new_image;
           DB::table('user')->insert($data);
           Session::put('message','Đăng ký thành công');
           return Redirect::route('register');
       }
       $data['user_img'] = '';
       DB::table('user')->insert($data);
       Session::put('message','Đăng ký thành công');
       return Redirect::route('register');
   }
    public function logout(){
        $this->AuthLogin();
        Session::put('user_name',null);
        Session::put('user_id',null);
        return Redirect::route('home');
    }
}