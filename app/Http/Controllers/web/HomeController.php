<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\blog;
use App\contact;
use App\information;
use App\information_image;
use App\like;
use App\messenger;
use App\partnerships;
use App\slider;
use App\type_blog;
use App\typeasset;
use App\vip;
use App\User;
use DB;

class HomeController extends Controller
{
    //home
    public function index()
    {
        $user = User::get();
        $blog = blog::where('blog_stt',1)
        ->join('type_blog', 'type_blog.type_blog_id', '=', 'blog.type_blog_id')
        ->join('User', 'blog.id_user_post', '=', 'User.user_id')
        ->orderBy('blog.blog_id', 'DESC')->take(3)->get();
        $information = information::where('inf_stt', 1)
        ->join('User', 'information.id_user', '=', 'User.user_id')
            ->orderBy('information.inf_id', 'DESC')->get();
        return view('web.index',compact('user','blog','information'));
    }
    //blog
    public function blog()
    {
        $blog = blog::where('blog_stt',1)
        ->join('type_blog', 'type_blog.type_blog_id', '=', 'blog.type_blog_id')
        ->join('User', 'blog.id_user_post', '=', 'User.user_id')->paginate(6);
        $typeasset = typeasset::where('type_stt',1)->get();
        $information = information::where('inf_stt', 1)
        ->leftJoin('User', 'information.id_user', '=', 'User.user_id')
            ->select('information.*', 'User.user_name','User.user_img')
            ->orderBy('information.inf_id', 'DESC')->get();
        return view('web.blog.blog',compact('blog','typeasset','information'));
    }
    //blog type
    public function getBlog($type)
    {
        $blog_type = blog::where('blog_stt', 1)->where('type_blog_id',$type)
        ->join('User', 'blog.id_user_post', '=', 'User.user_id')->paginate(6);
        // dd( $detail);
        return view('web.blog.blogtype',compact('blog_type'));
    }
    //blog detail
    public function detailBlog($id)
    {
        
        $detail = blog::where('blog.blog_id',$id)
        ->join('type_blog', 'type_blog.type_blog_id', '=', 'blog.type_blog_id')
        ->join('User', 'blog.id_user_post', '=', 'User.user_id')->get();
        return view('web.blog.detailblog',compact('detail'));
    }
    //all user
    public function allUser()
    {
        
        $user = User::paginate(6);
        $typeasset = typeasset::where('type_stt',1)->get();
        $information = information::where('inf_stt', 1)
        ->leftJoin('User', 'information.id_user', '=', 'User.user_id')
            ->select('information.*', 'User.user_name','User.user_img')
            ->orderBy('information.inf_id', 'DESC')->get();
        // $info_count = information::join('User', 'information.id_user', '=', 'User.user_id')
        // ->where('id_user')->count();
        return view('web.user.alluser',compact('user','typeasset','information'));
    }
    //user
    public function user()
    {
        return view('web.user.user');
    }
    //category
    public function typeasset()
    {
        $user = User::get();
        $blog = blog::where('blog_stt',1)
        ->join('type_blog', 'type_blog.type_blog_id', '=', 'blog.type_blog_id')
        ->join('User', 'blog.id_user_post', '=', 'User.user_id')
        ->orderBy('blog.blog_id', 'DESC')->take(3)->get();
        $typeasset = information::where('inf_stt',1)
        ->join('User', 'information.id_user', '=', 'User.user_id')->paginate(6);
        return view('web.category.allcategory',compact('user','blog','typeasset'));
    }
    //detail category
    public function getTyppeasset($type)
    {
        $user = User::get();
        $blog = blog::where('blog_stt',1)
        ->join('type_blog', 'type_blog.type_blog_id', '=', 'blog.type_blog_id')
        ->join('User', 'blog.id_user_post', '=', 'User.user_id')
        ->orderBy('blog.blog_id', 'DESC')->take(3)->get();
        $type_asset = information::where('inf_stt', 1)->where('id_type',$type)
        ->leftJoin('User', 'information.id_user', '=', 'User.user_id')
            ->select('information.*', 'User.user_name','User.user_img')
            ->orderBy('information.inf_id', 'DESC')->get();
        //dd( $typeasset);
        return view('web.category.category',compact('user','blog','type_asset'));
    }

    //detail information
    public function getChitiet($id)
    {
        $typeasset = typeasset::where('type_stt',1)->get();
        $information = information::where('inf_stt', 1)
        ->join('User', 'information.id_user', '=', 'User.user_id')->take(4)->get();
        $detail = information::join('information_image','information.inf_id','=','information_image.id_info')
        ->join('typeasset','information.id_type','=','typeasset.type_id')
        ->join('User', 'information.id_user', '=', 'User.user_id')
        ->where('information.inf_id',$id)->get();
        // dd($detail);
        return view('web.detail',compact('detail','typeasset','information'));
    }

    public function postComment(Request $request,$id)
    {

    }
}