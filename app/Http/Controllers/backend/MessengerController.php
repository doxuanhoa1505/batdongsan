<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\messenger; 
class MessengerController extends Controller
{
    public function AuthLogin(){
        $admin_id = Session::get('admin_id');
        if($admin_id){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }
    public function add_messenger(){
        $this->AuthLogin();
        $all_user = DB::table('user')->orderby('user_id', 'asc')->get();
        $manager_messenger  = view('admin.messenger.add')->with('all_user',$all_user);
        return view('admin')->with('admin.messenger.add',$manager_messenger);
    	

    }
    public function all_messenger(){
        $this->AuthLogin();
        $all_messenger = DB::table('messenger')
        ->join('user', 'messenger.email_user_receive', '=', 'user.user_email')
        ->join('admin', 'messenger.admin_email', '=', 'admin.admin_email')->orderby('mes_id','desc')
        ->orderby(DB::raw('RAND()'))->paginate(9);
    	$manager_messenger  = view('admin.messenger.list')->with('all_messenger',$all_messenger);
    	return view('admin')->with('admin.messenger.list', $manager_messenger );

    }
    public function save_messenger(Request $request){
         $this->AuthLogin();
         $data =new messenger;
        
         $data['mes_name'] = $request->mes_name;
         $data['mes_des'] = $request->mes_des;
         $data['admin_email'] = $request->admin_email;
         $data['email_user_receive'] = $request->email_user_receive;
       
         $data->save();
         Session::put('message','Đăng kí đề tài thành công');
         return Redirect::to('admin/all-messenger');   	
    }
    public function unactive_messenger($id){
         $this->AuthLogin();
        DB::table('messenger')->where('mes_id',$id)->update(['mes_stt'=>1]);
        Session::put('message','Không kích hoạt loại đề tài thành công');
        return Redirect::to('admin/all-messenger');

    }
    public function active_messenger($id){
         $this->AuthLogin();
        DB::table('messenger')->where('mes_id',$id)->update(['mes_stt'=>0]);
        Session::put('message','Kích hoạt loại đề tài thành công');
        return Redirect::to('admin/all-messenger');
    }
    public function edit_messenger($id){
         $this->AuthLogin();
        

        $edit_messenger = DB::table('messenger')->where('mes_id',$id)->get();

        $manager_messenger  = view('admin.messenger.edit')->with('edit_messenger',$edit_messenger);

        return view('admin')->with('admin.messenger.edit', $manager_messenger);
    }
    public function update_messenger(Request $request,$id){
         $this->AuthLogin();
        $data = array();
        $data['mes_name'] = $request->mes_name;
         $data['mes_des'] = $request->mes_des;
         $data['admin_email'] = $request->admin_email;
         $data['email_user_receive'] = $email_user_receive;
          
        DB::table('messenger')->where('mes_id',$id)->update($data);
        Session::put('message','Cập nhật sản phẩm không thành công');
        return Redirect::to('admin/all-messenger');
    }
    public function delete_messenger($id){
        $this->AuthLogin();
        DB::table('messenger')->where('mes_id',$id)->delete();
        Session::put('message','Xóa sản phẩm thành công');
        return Redirect::to('admin/all-messenger');
    }
    public function detailemail($id){
          $detailemail = DB::table('messenger')
          ->join('user', 'messenger.email_user_receive', '=', 'user.user_email')
          ->join('admin', 'messenger.admin_email', '=', 'admin.admin_email')
          ->where('mes_stt','1')->where('messenger.mes_id', $id)->orderby('mes_id', 'asc')->get();
 
        $manager_messenger  = view('admin.messenger.details')->with('detailemail', $detailemail);
       return view('admin')->with('admin.messenger.details', $manager_messenger );
    }
    public function detailuser($id){
        $all_user = DB::table('user')->orderby('user_id', 'asc')->get();
        $detailuser = DB::table('user')->where('user.user_id', $id)->orderby('user_id', 'asc')->get();

      $manager_messenger  = view('admin.messenger.detailuser')->with('detailuser', $detailuser)->with('all_user', $all_user);
     return view('admin')->with('admin.messenger.detailsuser', $manager_messenger );
  }
}