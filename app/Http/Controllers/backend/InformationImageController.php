<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();


class InformationImageController extends Controller
{
    public function AuthLogin(){
        $admin_id = Session::get('admin_id');
        if($admin_id){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }
    public function add_information_image(){
        $this->AuthLogin();
        
      return view('admin.information.details');
    	

    }
    public function all_information_image(){
       $this->AuthLogin();
        $all_information_image = DB::table('information_image')->get();
    	$manager_type  = view('admin.information_image.list')->with('all_information_image',$all_information_image);
    	return view('admin')->with('admin.information_image.list', $manager_type );

    }
    public function save_information_image(Request $request){
         $this->AuthLogin();
    	$data = array();
    	$data['id_info'] = $request->id_info;
        $data['info_image_stt'] = $request->info_image_stt;
        $get_image = $request->file('info_image');
      
        if($get_image){
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image =  $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/website/uploads/information',$new_image);
            $data['info_image'] = $new_image;
            DB::table('information_image')->insert($data);
            Session::put('message','Thêm sản phẩm thành công');
            return redirect()->back();
        }
        $data['info_image'] = '';
    	DB::table('information_image')->insert($data);
    	Session::put('message','Thêm sản phẩm thành công');
    	return redirect()->back();
    }
    
    public function unactive_information_image($id){
         $this->AuthLogin();
        DB::table('information_image')->where('info_id',$id)->update(['info_image_stt'=>1]);
        Session::put('message','Không kích hoạt loại đề tài thành công');
        return redirect()->back();

    }
    public function active_information_image($id){
         $this->AuthLogin();
        DB::table('information_image')->where('info_id',$id)->update(['info_image_stt'=>0]);
        Session::put('message','Kích hoạt loại đề tài thành công');
        return redirect()->back();
    }
    
    public function delete_information_image($id){
        $this->AuthLogin();
        DB::table('information_image')->where('info_id',$id)->delete();
        Session::put('message','Xóa sản phẩm thành công');
        return redirect()->back();
    }
}
