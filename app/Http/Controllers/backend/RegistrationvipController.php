<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use App\Registration; 
use Illuminate\Support\Facades\Redirect;
session_start();

class RegistrationvipController extends Controller
{
    public function AuthLogin(){
        $admin_id = Session::get('admin_id');
        if($admin_id){
             return Redirect::to('dashboard');
        }else{
             return Redirect::to('admin')->send();
        }
    }
    public function add_vip(){
        $this->AuthLogin();
        $type_vip = DB::table('type_vip')->get();
        $manager_type  = view('admin.registration_vip.add')->with('type_vip',$type_vip);
        return view('admin')->with('admin.registration_vip.add', $manager_type );
    	

    }
    public function all_vip(){
       $this->AuthLogin();
        $all_vip_on = DB::table('registration_vip')
        ->join('type_vip', 'registration_vip.vip_id', '=', 'type_vip.vip_id')
        ->join('user', 'registration_vip.user_id', '=', 'user.user_id')
        ->where ("regi_vip_time_off", ">", date('Y-m-d H:i:s', time()))
       ->orderby('regi_vip_id', 'asc')->get();
       $all_vip_off = DB::table('registration_vip')
       ->join('type_vip', 'registration_vip.vip_id', '=', 'type_vip.vip_id')
       ->join('user', 'registration_vip.user_id', '=', 'user.user_id')
       ->where ("regi_vip_time_off", "<", date('Y-m-d H:i:s', time()))
      ->orderby('regi_vip_id', 'asc')->get();
    	$manager_type  = view('admin.registration_vip.list')->with('all_vip_off',$all_vip_off)->with('all_vip_on',$all_vip_on);
    	return view('admin')->with('admin.registration_vip.list', $manager_type );

    }
    public function save_vip(Request $request){
         $this->AuthLogin();
    
          $data =new Registration;
        
          $data['user_id'] = $request->user_id;
          $data['vip_id'] = $request->vip_id;
          $data['regi_vip_time_on'] = $request->regi_vip_time_on;
          $data['regi_vip_time_off'] = $request->regi_vip_time_off;
        
          $data->save();
          Session::put('message','Đăng kí đề tài thành công');
          return Redirect::to('admin/all-registration_vip');  
    }
    public function unactive_vip($id){
         $this->AuthLogin();
        DB::table('registration_vip')->where('regi_vip_id',$id)->update(['regi_vip_stt'=>1]);
        Session::put('message','Không kích hoạt loại đề tài thành công');
         return Redirect::to('admin/all-registration_vip');

    }
    public function active_vip($id){
         $this->AuthLogin();
        DB::table('registration_vip')->where('regi_vip_id',$id)->update(['regi_vip_stt'=>0]);
        Session::put('message','Kích hoạt loại đề tài thành công');
         return Redirect::to('admin/all-registration_vip');
    }
    public function edit_vip($id){
         $this->AuthLogin();
        
        // $type_vip = DB::table('type_vip')->get();
        
       
        $edit_type = DB::table('registration_vip')->where('regi_vip_id',$id)
        ->join('type_vip', 'registration_vip.vip_id', '=', 'type_vip.vip_id')
        ->get();

        $manager_type  = view('admin.registration_vip.edit')->with('edit_type',$edit_type);

        return view('admin')->with('admin.registration_vip.edit', $manager_type);
    }
    public function update_vip(Request $request,$id){
         $this->AuthLogin();
         $data = array();
        
         $data['user_id'] = $request->user_id;
         $data['vip_id'] = $request->vip_id;
         $data['regi_vip_time_on'] = $request->regi_vip_time_on;
         $data['regi_vip_time_off'] = $request->regi_vip_time_off;
       
         DB::table('registration_vip')->where('regi_vip_id',$id)->update($data);
         Session::put('message','Cập nhật sản phẩm không thành công');
         return Redirect::to('admin/all-registration_vip');  
    }
    public function delete_vip($id){
        $this->AuthLogin();   
        DB::table('registration_vip')->where('regi_vip_id',$id)->delete();
        Session::put('message','Xóa sản phẩm thành công');
         return Redirect::to('admin/all-registration_vip');
    }
	 
}
     