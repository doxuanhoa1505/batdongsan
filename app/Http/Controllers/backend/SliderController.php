<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();

class SliderController extends Controller
{
    public function AuthLogin(){
        $admin_id = Session::get('admin_id');
        if($admin_id){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }
    public function add_slider(){
        $this->AuthLogin();
        
        return view('admin.slider.add');
    	

    }
    public function all_slider(){
        $this->AuthLogin();
        $all_slider = DB::table('slider')->get();
    	$manager_slider  = view('admin.slider.list')->with('all_slider',$all_slider);
    	return view('admin')->with('admin.slider.list', $manager_slider );

    }
    public function save_slider(Request $request){
         $this->AuthLogin();
    	$data = array();
    
        $data['sli_name'] = $request->sli_name;
        $data['sli_stt'] = $request->sli_stt;
        $get_image = $request->file('sli_img');
      
        if($get_image){
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image =  $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/website/uploads/slider',$new_image);
            $data['sli_img'] = $new_image;
            DB::table('slider')->insert($data);
            Session::put('message','Thêm sản phẩm thành công');
            return Redirect::to('add-slider');
        }
        $data['sli_img'] = '';
    	DB::table('slider')->insert($data);
    	Session::put('message','Thêm sản phẩm thành công');
    	return Redirect::to('admin/all-slide');
    }
    public function unactive_slider($id){
         $this->AuthLogin();
        DB::table('slider')->where('sli_id',$id)->update(['sli_stt'=>1]);
        Session::put('message','Không kích hoạt loại đề tài thành công');
        return Redirect::to('admin/all-slide');

    }
    public function active_slider($id){
         $this->AuthLogin();
        DB::table('slider')->where('sli_id',$id)->update(['sli_stt'=>0]);
        Session::put('message','Kích hoạt loại đề tài thành công');
        return Redirect::to('admin/all-slide');
    }
    public function edit_slider($id){
         $this->AuthLogin();
        

        $edit_slider = DB::table('slider')->where('sli_id',$id)->get();

        $manager_slider  = view('admin.slider.edit')->with('edit_slider',$edit_slider);

        return view('admin')->with('admin.slider.edit', $manager_slider);
    }
    public function update_slider(Request $request,$id){
         $this->AuthLogin();
        $data = array();
        $data['sli_name'] = $request->sli_name;
        $data['sli_img'] = $request->sli_img;
      
        $get_image = $request->file('sli_img');
        
        if($get_image){
                    $get_name_image = $get_image->getClientOriginalName();
                    $name_image = current(explode('.',$get_name_image));
                    $new_image =  $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
                    $get_image->move('public/website/uploads/slider',$new_image);
                    $data['sli_img'] = $new_image;
                    DB::table('slider')->where('sli_id',$id)->update($data);
                    Session::put('message','Cập nhật sản phẩm thành công');
                    return Redirect::to('admin/all-slide');
        }
            
        DB::table('slider')->where('sli_id',$id)->update($data);
        Session::put('message','Cập nhật sản phẩm không thành công');
        return Redirect::to('admin/all-slide');
    }
    public function delete_slider($id){
        $this->AuthLogin();
        DB::table('slider')->where('sli_id',$id)->delete();
        Session::put('message','Xóa sản phẩm thành công');
        return Redirect::to('admin/all-slide');
    }
}
