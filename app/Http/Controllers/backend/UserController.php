<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();

class UserController extends Controller
{
    public function AuthLogin(){
        $admin_id = Session::get('admin_id');
        if($admin_id){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }
    public function add_user(){
        $this->AuthLogin();
        
        return view('admin.user.add');
    	

    }
    public function all_user(){
        $this->AuthLogin();
        $all_user = DB::table('user')->orderby(DB::raw('RAND()'))->paginate(9);
    	$manager_user  = view('admin.user.list')->with('all_user',$all_user);
    	return view('admin')->with('admin.user.list', $manager_user );

    }
    public function save_user(Request $request){
         $this->AuthLogin();
    	$data = array();
    
        $data['user_name'] = $request->user_name;
        $data['user_birthday'] = $request->user_birthday;
        $data['user_address'] = $request->user_address;
        $data['user_email'] = $request->user_email;
        $data['user_password'] = md5($request->user_password);
        $data['user_phone'] = $request->user_phone;
        $data['user_sex'] = $request->user_sex;
        $data['facebook'] = $request->facebook;
      
        $get_image = $request->file('user_img');
      
        if($get_image){
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image =  $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/website/uploads/user',$new_image);
            $data['user_img'] = $new_image;
            DB::table('user')->insert($data);
            Session::put('message','Thêm sản phẩm thành công');
            return Redirect::to('admin/add-user');
        }
        $data['user_img'] = '';
    	DB::table('user')->insert($data);
    	Session::put('message','Thêm sản phẩm thành công');
    	return Redirect::to('admin/all-user');
    }
    public function unactive_user($id){
         $this->AuthLogin();
        DB::table('user')->where('user_id',$id)->update(['vip_stt'=>1]);
        Session::put('message','Không kích hoạt loại đề tài thành công');
        return Redirect::to('admin/all-user');

    }
    public function active_user($id){
         $this->AuthLogin();
        DB::table('user')->where('user_id',$id)->update(['vip_stt'=>0]);
        Session::put('message','Kích hoạt loại đề tài thành công');
        return Redirect::to('admin/all-user');
    }
    public function edit_user($id){
         $this->AuthLogin();
        

        $edit_user = DB::table('user')->where('user_id',$id)->get();

        $manager_user  = view('admin.user.edit')->with('edit_user',$edit_user);

        return view('admin')->with('admin.user.edit', $manager_user);
    }
    public function update_user(Request $request,$id){
         $this->AuthLogin();
        $data = array();
        $data['user_name'] = $request->user_name;
        $data['user_birthday'] = $request->user_birthday;
        $data['user_address'] = $request->user_address;
        $data['user_email'] = $request->user_email;
        $data['user_password'] = $request->user_password;
        $data['user_phone'] = $request->user_phone;
        $data['user_sexe'] = $request->user_sex;
        $data['facebook'] = $request->facebook;
      
        $get_image = $request->file('user_img');
        
        if($get_image){
                    $get_name_image = $get_image->getClientOriginalName();
                    $name_image = current(explode('.',$get_name_image));
                    $new_image =  $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
                    $get_image->move('public/website/uploads/user',$new_image);
                    $data['user_img'] = $new_image;
                    DB::table('user')->where('user_id',$id)->update($data);
                    Session::put('message','Cập nhật sản phẩm thành công');
                    return Redirect::to('admin/all-user');
        }
            
        DB::table('user')->where('user_id',$id)->update($data);
        Session::put('message','Cập nhật sản phẩm không thành công');
        return Redirect::to('admin/all-user');
    }
    public function delete_user($id){
        $this->AuthLogin();
        DB::table('user')->where('user_id',$id)->delete();
        Session::put('message','Xóa sản phẩm thành công');
        return Redirect::to('admin/all-user');
    }
    public function detailuser($id){
        $all_user = DB::table('user')->orderby('user_id', 'asc')->get();
        $detailuser = DB::table('user')->where('user.user_id', $id)->orderby('user_id', 'asc')->get();

      $manager_messenger  = view('admin.user.detailuser')->with('detailuser', $detailuser)->with('all_user', $all_user);
     return view('admin')->with('admin.user.detailsuser', $manager_messenger );
  }
}

