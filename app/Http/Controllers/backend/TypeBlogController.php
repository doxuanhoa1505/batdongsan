<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Http\Controllers\AdminController;
class TypeBlogController extends Controller
{
    public function AuthLogin(){
        $admin_id = Session::get('admin_id');
        if($admin_id){
             return Redirect::to('dashboard');
        }else{
             return Redirect::to('admin')->send();
        }
    }
    public function add_typeblog(){
        $this->AuthLogin();
        
        return view('admin.typeblog.add');
    	

    }
    public function all_typeblog(){
       $this->AuthLogin();
        $all_typeblog = DB::table('type_blog')->get();
    	$manager_type  = view('admin.typeblog.list')->with('all_typeblog',$all_typeblog);
    	return view('admin')->with('admin.typeblog.list', $manager_type );

    }
    public function save_typeblog(Request $request){
         $this->AuthLogin();
    	$data = array();
    	$data['type_blog_name'] = $request->type_blog_name;
        $data['type_blog_stt'] = $request->type_blog_stt;
    	DB::table('type_blog')->insert($data);
    	Session::put('message','Thêm thành công');
    	 return Redirect::to('admin/all-typeblog');
    }
    public function unactive_typeblog($id){
         $this->AuthLogin();
        DB::table('type_blog')->where('type_blog_id',$id)->update(['stt'=>1]);
        Session::put('message','Không kích hoạt loại đề tài thành công');
         return Redirect::to('admin/all-typeblog');

    }
    public function active_typeblog($id){
         $this->AuthLogin();
        DB::table('type_blog')->where('type_blog_id',$id)->update(['stt'=>0]);
        Session::put('message','Kích hoạt loại đề tài thành công');
         return Redirect::to('admin/all-typeblog');
    }
    public function edit_typeblog($id){
         $this->AuthLogin();
        

        $edit_type = DB::table('type_blog')->where('type_blog_id',$id)->get();

        $manager_type  = view('admin.typeblog.edit')->with('edit_type',$edit_type);

        return view('admin')->with('admin.typeblog.edit', $manager_type);
    }
    public function update_typeblog(Request $request,$id){
         $this->AuthLogin();
        $data = array();
        $data['type_blog_name'] = $request->type_blog_name;
        $data['type_blog_stt'] = $request->type_blog_stt;
            
        DB::table('type_blog')->where('type_blog_id',$id)->update($data);
        Session::put('message','Cập nhật sản phẩm không thành công');
         return Redirect::to('admin/all-typeblog');
    }
    public function delete_typeblog($id){
        $this->AuthLogin();
        DB::table('type_blog')->where('type_blog_id',$id)->delete();
        Session::put('message','Xóa sản phẩm thành công');
         return Redirect::to('admin/all-typeblog');
    }
}
