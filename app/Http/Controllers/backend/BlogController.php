<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Http\Controllers\AdminController;


class BlogController extends Controller
{
    public function AuthLogin(){
        $admin_id = Session::get('admin_id');
        if($admin_id){
             return Redirect::to('dashboard');
        }else{
             return Redirect::to('admin')->send();
        }
    }
    public function add_blog(){
        $this->AuthLogin();
        $type_blog = DB::table('type_blog')->orderby('type_blog_id', 'asc')->get();
        return view('admin.blog.add')->with('type_blog',$type_blog);
    	

    }
    public function all_blog(){
       $this->AuthLogin();
      
        $all_blog = DB::table('blog') 
        ->join('type_blog', 'type_blog.type_blog_id', '=', 'blog.type_blog_id')
        
        ->orderby('blog.blog_id', 'asc')->get();
        $manager_type  = view('admin.blog.list')
        ->with('all_blog',$all_blog)
        ;
    	return view('admin')->with('admin.blog.list', $manager_type );

    }
    public function save_blog(Request $request){
        $this->AuthLogin();
       $data = array();
       $data['blog_name'] = $request->blog_name;
       $data['type_blog_id'] = $request->type_blog_id;
       $data['blog_des'] = $request->blog_des;
       $data['id_admin'] = $request->id_admin;
       $data['id_user_post'] = $request->id_user_post;
       $data['blog_stt'] = $request->blog_stt;
       $get_image = $request->file('blog_img');
     
       if($get_image){
           $get_name_image = $get_image->getClientOriginalName();
           $name_image = current(explode('.',$get_name_image));
           $new_image =  $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
           $get_image->move('public/website/uploads/blog',$new_image);
           $data['blog_img'] = $new_image;
           DB::table('blog')->insert($data);
           Session::put('message','Thêm sản phẩm thành công');
            return Redirect::to('admin/all-blog');
       }
       $data['blog_img'] = '';
       DB::table('blog')->insert($data);
       Session::put('message','Thêm sản phẩm thành công');
        return Redirect::to('admin/all-blog');
   }
    
    public function unactive_blog($id){
         $this->AuthLogin();
        DB::table('blog')->where('blog_id',$id)->update(['blog_stt'=>1]);
        Session::put('message','Không kích hoạt loại đề tài thành công');
         return Redirect::to('admin/all-blog');

    }
    public function active_blog($id){
         $this->AuthLogin();
        DB::table('blog')->where('blog_id',$id)->update(['blog_stt'=>0]);
        Session::put('message','Kích hoạt loại đề tài thành công');
         return Redirect::to('admin/all-blog');
    }
    public function edit_blog($id){
         $this->AuthLogin();
        

        $edit_blog = DB::table('blog')->where('blog_id',$id)->get();
        $type_blog = DB::table('type_blog')->orderby('type_blog_id', 'asc')->get();
        $manager_type  = view('admin.blog.edit')->with('edit_blog',$edit_blog)->with('type_blog',$type_blog);

        return view('admin')->with('admin.blog.edit', $manager_type);
    }
    public function update_blog(Request $request,$id){
        $this->AuthLogin();
        $data = array();
        $data['blog_name'] = $request->blog_name;
        $data['type_blog_id'] = $request->type_blog_id;
        $data['blog_des'] = $request->blog_des;
        $data['id_admin'] = $request->id_admin;
        $data['id_user_post'] = $request->id_user_post;
        $data['blog_stt'] = $request->blog_stt;
        $get_image = $request->file('blog_img');
       
       if($get_image){
                   $get_name_image = $get_image->getClientOriginalName();
                   $name_image = current(explode('.',$get_name_image));
                   $new_image =  $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
                   $get_image->move('public/website/uploads/blog',$new_image);
                   $data['blog_img'] = $new_image;
                   DB::table('blog')->where('id',$id)->update($data);
                   Session::put('message','Cập nhật sản phẩm thành công');
                    return Redirect::to('admin/all-blog');
       }
           
       DB::table('blog')->where('blog_id',$id)->update($data);
       Session::put('message','Cập nhật sản phẩm không thành công');
        return Redirect::to('admin/all-blog');
   }
  
    public function delete_blog($id){
        $this->AuthLogin();
        DB::table('blog')->where('blog_id',$id)->delete();
        Session::put('message','Xóa sản phẩm thành công');
         return Redirect::to('admin/all-blog');
    }
}
    