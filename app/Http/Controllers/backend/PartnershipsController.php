<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();


class PartnershipsController extends Controller
{
    public function AuthLogin(){
        $admin_id = Session::get('admin_id');
        if($admin_id){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }
    public function add_partnerships(){
        $this->AuthLogin();
        
        return view('admin.partnerships.add');
    	

    }
    public function all_partnerships(){
        $this->AuthLogin();
        $all_partnerships = DB::table('partnerships')->get();
    	$manager_partnerships  = view('admin.partnerships.list')->with('all_partnerships',$all_partnerships);
    	return view('admin')->with('admin.partnerships.list', $manager_partnerships );

    }
    public function save_partnerships(Request $request){
         $this->AuthLogin();
    	$data = array();
    
        $data['par_name'] = $request->par_name;
        $data['par_stt'] = $request->par_stt;
        $get_image = $request->file('par_img');
      
        if($get_image){
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image =  $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/website/uploads/partnerships',$new_image);
            $data['par_img'] = $new_image;
            DB::table('partnerships')->insert($data);
            Session::put('message','Thêm sản phẩm thành công');
            return Redirect::to('add-partnerships');
        }
        $data['par_img'] = '';
    	DB::table('partnerships')->insert($data);
    	Session::put('message','Thêm sản phẩm thành công');
    	return Redirect::to('admin/all-partnerships');
    }
    public function unactive_partnerships($id){
         $this->AuthLogin();
        DB::table('partnerships')->where('par_id',$id)->update(['par_stt'=>1]);
        Session::put('message','Không kích hoạt loại đề tài thành công');
        return Redirect::to('admin/all-partnerships');

    }
    public function active_partnerships($id){
         $this->AuthLogin();
        DB::table('partnerships')->where('par_id',$id)->update(['par_stt'=>0]);
        Session::put('message','Kích hoạt loại đề tài thành công');
        return Redirect::to('admin/all-partnerships');
    }
    public function edit_partnerships($id){
         $this->AuthLogin();
        

        $edit_partnerships = DB::table('partnerships')->where('par_id',$id)->get();

        $manager_partnerships  = view('admin.partnerships.edit')->with('edit_partnerships',$edit_partnerships);

        return view('admin')->with('admin.partnerships.edit', $manager_partnerships);
    }
    public function update_partnerships(Request $request,$id){
         $this->AuthLogin();
        $data = array();
        $data['par_name'] = $request->par_name;
        $data['par_img'] = $request->par_img;
      
        $get_image = $request->file('par_img');
        
        if($get_image){
                    $get_name_image = $get_image->getClientOriginalName();
                    $name_image = current(explode('.',$get_name_image));
                    $new_image =  $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
                    $get_image->move('public/website/uploads/partnerships',$new_image);
                    $data['par_img'] = $new_image;
                    DB::table('partnerships')->where('par_id',$id)->update($data);
                    Session::put('message','Cập nhật sản phẩm thành công');
                    return Redirect::to('admin/all-partnerships');
        }
            
        DB::table('partnerships')->where('par_id',$id)->update($data);
        Session::put('message','Cập nhật sản phẩm không thành công');
        return Redirect::to('admin/all-partnerships');
    }
    public function delete_partnerships($id){
        $this->AuthLogin();
        DB::table('partnerships')->where('par_id',$id)->delete();
        Session::put('message','Xóa sản phẩm thành công');
        return Redirect::to('admin/all-partnerships');
    }
}
