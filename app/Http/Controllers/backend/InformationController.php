<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\information_image;  

class InformationController extends Controller
{
    public function AuthLogin(){
        $admin_id = Session::get('admin_id');
        if($admin_id){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }
    public function add_information(){
        $this->AuthLogin();
        $type = DB::table('typeasset')->orderby('type_id', 'asc')->get();
        return view('admin.information.add')->with('type',$type);
    	

    }
    public function all_information(){
        $this->AuthLogin();
        $all_information = DB::table('information')->get();
    	$manager_information  = view('admin.information.list')->with('all_information',$all_information);
    	return view('admin')->with('admin.information.list', $manager_information );

    }
    public function save_information(Request $request){
         $this->AuthLogin();
    	$data = array();
        $data['id_type'] = $request->id_type;
        $data['inf_name'] = $request->inf_name;
        $data['id_user'] = $request->id_user ;
        $data['inf_to_own'] = $request->inf_to_own;
        $data['inf_address'] = $request->inf_address ;
        $data['inf_des'] = $request->inf_des ;
        $data['inf_price'] = $request->inf_price;
        $data['inf_like'] = $request->inf_like ;
        $data['inf_air_conditioning'] = $request->inf_air_conditioning ;
        $data['inf_barbeque'] = $request->inf_barbeque ;
        $data['inf_dryer'] = $request->inf_dryer ;
        $data['inf_gym'] = $request->inf_gym ;
        $data['inf_laundry'] = $request->inf_laundry ;
        $data['inf_microwave'] = $request->inf_microwave;
        $data['inf_outdoor_shower'] = $request->inf_outdoor_shower ;
        $data['inf_refrigerator'] = $request->inf_refrigerator ;
        $data['inf_sauna'] = $request->inf_sauna ;
        $data['inf_swimming_pool'] = $request->inf_swimming_pool;
        $data['inf_tv'] = $request->inf_tv ;
        $data['inf_washer'] = $request->inf_washer ;
        $data['inf_wifi'] = $request->inf_wifi ;
        $data['inf_window'] = $request->inf_window ;
        $data['inf_bedrooms'] = $request->inf_bedrooms ;
        $data['inf_bathrooms'] = $request->inf_bathrooms ;
        $data['inf_garage'] = $request->inf_garage ;
        $data['inf_garage_size'] = $request->inf_garage_size ;
        $data['inf_property_size'] = $request->inf_property_size ;
        $data['inf_year_built'] = $request->inf_year_built ;
        $data['inf_last_remodel_year'] = $request->inf_last_remodel_year ;
        $data['inf_pool_size'] = $request->inf_pool_size ;
        $data['inf_stt'] = $request->inf_stt;
        $get_image = $request->file('inf_img');
      
        if($get_image){
            $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
            $new_image =  $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
            $get_image->move('public/website/uploads/information',$new_image);
            $data['inf_img'] = $new_image;
            DB::table('information')->insert($data);
            Session::put('message','Thêm sản phẩm thành công');
            return Redirect::to('add-information');
        }
        $data['inf_img'] = '';
    	DB::table('information')->insert($data);
    	Session::put('message','Thêm sản phẩm thành công');
    	return Redirect::to('admin/all-information');
    }
    public function unactive_information($id){
         $this->AuthLogin();
        DB::table('information')->where('inf_id',$id)->update(['inf_stt'=>1]);
        Session::put('message','Không kích hoạt loại đề tài thành công');
        return Redirect::to('admin/all-information');

    }
    public function active_information($id){
         $this->AuthLogin();
        DB::table('information')->where('inf_id',$id)->update(['inf_stt'=>0]);
        Session::put('message','Kích hoạt loại đề tài thành công');
        return Redirect::to('admin/all-information');
    }
    public function edit_information($id){
         $this->AuthLogin();
        
         $type = DB::table('typeasset')->orderby('type_id', 'asc')->get();
        $edit_information = DB::table('information')->where('inf_id',$id)->get();

        $manager_information  = view('admin.information.edit')->with('edit_information',$edit_information)->with('type',$type);

        return view('admin')->with('admin.information.edit', $manager_information);
    }
    public function update_information(Request $request,$id){
         $this->AuthLogin();
        $data = array();
        $data['id_type'] = $request->id_type;
        $data['inf_name'] = $request->inf_name;
        $data['id_user'] = $request->id_user ;
        $data['inf_to_own'] = $request->inf_to_own;
        $data['inf_address'] = $request->inf_address ;
        $data['inf_des'] = $request->inf_des ;
        $data['inf_price'] = $request->inf_price;
        $data['inf_like'] = $request->inf_like ;
        $data['inf_air_conditioning'] = $request->inf_air_conditioning ;
        $data['inf_barbeque'] = $request->inf_barbeque ;
        $data['inf_dryer'] = $request->inf_dryer ;
        $data['inf_gym'] = $request->inf_gym ;
        $data['inf_laundry'] = $request->inf_laundry ;
        $data['inf_microwave'] = $request->inf_microwave;
        $data['inf_outdoor_shower'] = $request->inf_outdoor_shower ;
        $data['inf_refrigerator'] = $request->inf_refrigerator ;
        $data['inf_sauna'] = $request->inf_sauna ;
        $data['inf_swimming_pool'] = $request->inf_swimming_pool;
        $data['inf_tv'] = $request->inf_tv ;
        $data['inf_washer'] = $request->inf_washer ;
        $data['inf_wifi'] = $request->inf_wifi ;
        $data['inf_window'] = $request->inf_window ;
        $data['inf_bedrooms'] = $request->inf_bedrooms ;
        $data['inf_bathrooms'] = $request->inf_bathrooms ;
        $data['inf_garage'] = $request->inf_garage ;
        $data['inf_garage_size'] = $request->inf_garage_size ;
        $data['inf_property_size'] = $request->inf_property_size ;
        $data['inf_year_built'] = $request->inf_year_built ;
        $data['inf_last_remodel_year'] = $request->inf_last_remodel_year ;
        $data['inf_pool_size'] = $request->inf_pool_size ;
        $data['inf_stt'] = $request->inf_stt;
        $get_image = $request->file('inf_img');
        
        if($get_image){
                    $get_name_image = $get_image->getClientOriginalName();
                    $name_image = current(explode('.',$get_name_image));
                    $new_image =  $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
                    $get_image->move('public/website/uploads/information',$new_image);
                    $data['inf_img'] = $new_image;
                    DB::table('information')->where('inf_id',$id)->update($data);
                    Session::put('message','Cập nhật sản phẩm thành công');
                    return Redirect::to('admin/all-information');
        }
            
        DB::table('information')->where('inf_id',$id)->update($data);
        Session::put('message','Cập nhật sản phẩm không thành công');
        return Redirect::to('admin/all-information');
    }
    public function delete_information($id){
        $this->AuthLogin();
        DB::table('information')->where('inf_id',$id)->delete();
        Session::put('message','Xóa sản phẩm thành công');
        return Redirect::to('admin/all-information');
    }


    public function detailinf($id){
        $this->AuthLogin();
    
        $info_details = DB::table('information')
        ->join('typeasset', 'information.id_type', '=', 'typeasset.type_id')
        ->where('information.inf_id', $id)->orderby('inf_id', 'asc')->get();
        $infoimg = information_image::where('information_image.id_info', $id)->orderby('info_id', 'asc')->get();
        $manager_information  = view('admin.information.details')->with('info_details', $info_details)->with('infoimg', $infoimg);
      
       return view('admin')->with('admin.information.details', $manager_information );
    }
}

