<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Http\Controllers\AdminController;

class TypevipController extends Controller
{
    public function AuthLogin(){
        $admin_id = Session::get('admin_id');
        if($admin_id){
             return Redirect::to('dashboard');
        }else{
             return Redirect::to('admin')->send();
        }
    }
    public function add_typevip(){
        $this->AuthLogin();
        
        return view('admin.typevip.add');
    	

    }
    public function all_typevip(){
       $this->AuthLogin();
        $all_typevip = DB::table('type_vip')->get();
    	$manager_type  = view('admin.typevip.list')->with('all_typevip',$all_typevip);
    	return view('admin')->with('admin.typevip.list', $manager_type );

    }
    public function save_typevip(Request $request){
         $this->AuthLogin();
    	$data = array();
         $data['vip_name'] = $request->vip_name;
         $data['vip_price'] = $request->vip_price;
         $data['vip_time'] = $request->vip_time;
         $data['vip_stt'] = $request->vip_stt;
  
    	DB::table('type_vip')->insert($data);
    	Session::put('message','Thêm thành công');
    	 return Redirect::to('admin/all-typevip');
    }
    public function unactive_typevip($id){
         $this->AuthLogin();
        DB::table('type_vip')->where('vip_id',$id)->update(['vip_stt'=>1]);
        Session::put('message','Không kích hoạt loại đề tài thành công');
         return Redirect::to('admin/all-typevip');

    }
    public function active_typevip($id){
         $this->AuthLogin();
        DB::table('type_vip')->where('vip_id',$id)->update(['vip_stt'=>0]);
        Session::put('message','Kích hoạt loại đề tài thành công');
         return Redirect::to('admin/all-typevip');
    }
    public function edit_typevip($id){
         $this->AuthLogin();
        

        $edit_type = DB::table('type_vip')->where('vip_id',$id)->get();

        $manager_type  = view('admin.typevip.edit')->with('edit_type',$edit_type);

        return view('admin')->with('admin.typevip.edit', $manager_type);
    }
    public function update_typevip(Request $request,$id){
         $this->AuthLogin();
        $data = array();
        $data['vip_name'] = $request->vip_name;
         $data['vip_price'] = $request->vip_price;
         $data['vip_time'] = $request->vip_time;
            
        DB::table('type_vip')->where('vip_id',$id)->update($data);
        Session::put('message','Cập nhật sản phẩm không thành công');
         return Redirect::to('admin/all-typevip');
    }
    public function delete_typevip($id){
        $this->AuthLogin();
        DB::table('type_vip')->where('vip_id',$id)->delete();
        Session::put('message','Xóa sản phẩm thành công');
         return Redirect::to('admin/all-typevip');
    }
}