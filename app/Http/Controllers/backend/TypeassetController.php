<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();
use App\Http\Controllers\AdminController;

class TypeassetController extends Controller
{
    public function AuthLogin(){
        $admin_id = Session::get('admin_id');
        if($admin_id){
            return Redirect::to('dashboard');
        }else{
            return Redirect::to('admin')->send();
        }
    }
    public function add_typeasset(){
        $this->AuthLogin();
        
        return view('admin.typeasset.add');
    	

    }
    public function all_typeasset(){
       $this->AuthLogin();
        $all_typeasset = DB::table('typeasset')->get();
    	$manager_type  = view('admin.typeasset.list')->with('all_typeasset',$all_typeasset);
    	return view('admin')->with('admin.typeasset.list', $manager_type );

    }
    public function save_typeasset(Request $request){
         $this->AuthLogin();
    	$data = array();
    	$data['type_name'] = $request->type_name;
        $data['type_stt'] = $request->type_stt;
    	DB::table('typeasset')->insert($data);
    	Session::put('message','Thêm thành công');
    	return Redirect::to('admin/all-typeasset');
    }
    public function unactive_typeasset($id){
         $this->AuthLogin();
        DB::table('typeasset')->where('type_id',$id)->update(['type_stt'=>1]);
        Session::put('message','Không kích hoạt loại đề tài thành công');
        return Redirect::to('admin/all-typeasset');

    }
    public function active_typeasset($id){
         $this->AuthLogin();
        DB::table('typeasset')->where('type_id',$id)->update(['type_stt'=>0]);
        Session::put('message','Kích hoạt loại đề tài thành công');
        return Redirect::to('admin/all-typeasset');
    }
    public function edit_typeasset($id){
         $this->AuthLogin();
        

        $edit_type = DB::table('typeasset')->where('type_id',$id)->get();

        $manager_type  = view('admin.typeasset.edit')->with('edit_type',$edit_type);

        return view('admin')->with('admin.typeasset.edit', $manager_type);
    }
    public function update_typeasset(Request $request,$id){
         $this->AuthLogin();
        $data = array();
        $data['type_name'] = $request->type_name;
        $data['typet_stt'] = $request->type_stt;
            
        DB::table('typeasset')->where('type_id',$id)->update($data);
        Session::put('message','Cập nhật sản phẩm không thành công');
        return Redirect::to('admin/all-typeasset');
    }
    public function delete_typeasset($id){
        $this->AuthLogin();
        DB::table('typeasset')->where('type_id',$id)->delete();
        Session::put('message','Xóa sản phẩm thành công');
        return Redirect::to('admin/all-typeasset');
    }
}
