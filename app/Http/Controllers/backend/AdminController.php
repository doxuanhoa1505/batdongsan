<?php

namespace App\Http\Controllers\backend;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
use Session;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
session_start();

class AdminController extends Controller
{
    public function AuthLogin(){
        $id = Session::get('admin_id');
        if($id){
            return Redirect::to('/admin/dashboard');
        }else{
            return Redirect::to('/admin/admin')->send();
        }
    }
    public function index(){
    	return view('admin_login');
    }
    public function show_dashboard(){
       $this->AuthLogin();
    	return view('admin.dashboard');
    }
    public function dashboard(Request $request){
    	$admin_email = $request->admin_email;
    	$admin_password = md5($request->admin_password);
       
    	$result = DB::table('admin')->where('admin_email',$admin_email)->where('admin_password',$admin_password)-> first();
    	if($result){
            Session::put('admin_name',$result->admin_name);
            Session::put('admin_id',$result->admin_id);
            Session::put('admin_email',$result->admin_email);          
            Session::put('admin_img',$result->admin_img);
            return Redirect::to('/admin/dashboard');
        }else{
            Session::put('message','Mật khẩu hoặc tài khoản bị sai.Làm ơn nhập lại');
            return Redirect::to('/admin/admin');
        }

    }
    public function addadmin(){
    	return view('add_admin');
    }
    public function save_admin(Request $request){
        $this->AuthLogin();
         $data = array();
         $data['admin_name'] = $request->admin_name;
         $data['admin_email'] = $request->admin_email;
         $data['admin_password'] = md5($request->admin_password);
         $data['admin_img'] = $request->admin_img;  
         $get_image = $request->file('admin_img');
      
     if($get_image){
             $get_name_image = $get_image->getClientOriginalName();
            $name_image = current(explode('.',$get_name_image));
             $new_image =  $name_image.rand(0,99).'.'.$get_image->getClientOriginalExtension();
             $get_image->move('public/admin_img/img',$new_image);
           $data['admin_img'] = $new_image;
             DB::table('admin')->insert($data);
             Session::put('message','Thêm admin thành công');
            return Redirect::to('/admin/addadmin');
        }
         $data['admin_img'] = '';
        DB::table('admin')->insert($data);
         Session::put('message','Thêm admin thành công');
        return Redirect::to('/admin/addadmin');
 
 }
    public function logout(){
        $this->AuthLogin();
        Session::put('admin_name',null);
        Session::put('admin_id',null);
        return Redirect::to('/admin/admin');
    }           
}
