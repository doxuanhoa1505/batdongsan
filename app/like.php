<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class like extends Model
{
    protected $table='like';  
    protected $primaryKey = 'like_id'; 
    protected $data=['like_id','id_user','like_stt','id_info'];  
    protected $guarded = []; 
}
