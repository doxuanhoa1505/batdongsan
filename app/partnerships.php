<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class partnerships extends Model
{
    protected $table='partnerships';  
    protected $primaryKey = 'par_id'; 
    protected $data=['par_id','par_name','par_stt','par_img'];  
    protected $guarded = []; 
}
