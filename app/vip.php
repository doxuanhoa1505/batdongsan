<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vip extends Model
{
    protected $table='vip';  
    protected $primaryKey = 'vip_sid'; 
    protected $data=['vip_sid','id_user','vip_stt','time_vip'];  
    protected $guarded = []; 
}
