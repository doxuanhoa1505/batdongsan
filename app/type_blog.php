<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class type_blog extends Model
{
    protected $table='type_blog';  
    protected $primaryKey = 'type_blog_id'; 
    protected $data=['type_blog_id','type_blog_name','type_blog_stt'];  
    protected $guarded = []; 
}
