<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class contact extends Model
{
    protected $table='contact';  
    protected $primaryKey = 'cont_id'; 
    protected $data=['cont_id','cont_name','id_info','name_info','user_post','user_buy','email_buy','contact','phone_buy','facebook'];  
    protected $guarded = []; 
}
