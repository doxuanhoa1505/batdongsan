<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\typeasset;
use App\type_blog;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('web.layout.header',function($view){
            $typeasset = typeasset::get()->where('type_stt',1);
            
            $view->with('typeasset',$typeasset);
        });

        view()->composer('web.layout.header',function($view){
            $type_blog = type_blog::get()->where('type_blog_stt',1);
            
            $view->with('type_blog',$type_blog);
        });
    }
}
