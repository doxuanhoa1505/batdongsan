<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class information extends Model
{
    protected $table='information';  
    protected $primaryKey = 'inf_id'; 
    protected $data=['inf_id','id_type','inf_name','id_user','inf_address','inf_to_own','inf_des','inf_price','inf_like','inf_img','inf_air_conditioning','inf_barbeque','inf_dryer','inf_gym','inf_laundry','inf_microwave','inf_outdoor_shower','inf_refrigerator','inf_sauna','inf_swimming_pool','inf_tv','inf_washer','inf_wifi','inf_window','inf_stt','inf_bedrooms','inf_bathrooms','inf_garage','inf_garage_size','inf_property_size','inf_year_built','inf_last_remodel_year','inf_pool_size'];  
    protected $guarded = []; 
}