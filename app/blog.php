<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class blog extends Model
{
    protected $table='blog';  
    protected $primaryKey = 'blog_id'; 
    protected $data=['blog_id','blog_name','type_blog_id','blog_des','id_admin','id_user_post','blog_img','blog_stt'];  
    protected $guarded = []; 
}
