<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class messenger extends Model
{
    protected $table='messenger';  
    protected $primaryKey = 'mes_id'; 
    protected $data=['mes_id','mes_name','mes_des','mes_stt','admin_email','email_user_receive','email_user_post'];  
    protected $guarded = []; 
}
