<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class information_image extends Model
{
    protected $table='information_image';  
    protected $primaryKey = 'info_id'; 
    protected $data=['info_id','info_image','info_image_stt','id_info'];  
    protected $guarded = []; 
}
