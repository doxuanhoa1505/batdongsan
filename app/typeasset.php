<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class typeasset extends Model
{
    protected $table='typeasset';  
    protected $primaryKey = 'type_id'; 
    protected $data=['type_id','type_name','type_stt'];  
    protected $guarded = []; 
}
