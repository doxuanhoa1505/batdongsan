<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class slider extends Model
{
    protected $table='slider';  
    protected $primaryKey = 'sli_id'; 
    protected $data=['sli_id','sli_name','sli_stt','sli_img'];  
    protected $guarded = []; 
}
