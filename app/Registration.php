<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Registration extends Model
{
    protected $table='registration_vip';  
    protected $primaryKey = 'regi_vip_id'; 
    protected $data=['regi_vip_id','user_id','vip_id','regi_vip_time_on','regi_vip_time_off','regi_vip_stt'];  
    protected $guarded = []; 
}
