@extends('admin')
@section('contensen')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>DataTables</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">DataTables</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
      

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">DataTable with default features</h3>
            </div>
            <!-- /.card-header -->
           
            <div class="container">
                <div class="register-form">
                     <form action="{{URL::to('/save-admin')}}" method="post" enctype="multipart/form-data">	<?php
                            $message = Session::get('message');
                            if($message){
                                echo '<span class="text-alert">'.$message.'</span>';
                                Session::put('message',null);
                            }
                            ?>{{ csrf_field() }}
                        <h1>Đăng ký tài khoản mới</h1>
                        <div class="input-box">
						
                            <input type="text" placeholder="Nhập adminname" required="required" name="admin_name">
                        </div>
						<div class="input-box">
                             
                            <input type="text" placeholder="Nhập adminemail" required="required" name="admin_email">
                        </div>
                        <div class="input-box">

                            <input type="password" placeholder="Nhập mật khẩu" required="required" name="admin_password">
                        </div>
						<div class="input-box">

                            <input type="text" placeholder="Nhập quê quán" required="required" name="admin_quequan">
                        </div>
                        <div class="input-box">
                            <div class="col-6">
                                <label for="admin_sinhnhat">Sinh Nhật</label>
                                <br>
                                <input type="date" name="admin_sinhnhat"  required="required">
                            </div>
                            <div class="clear"></div>
                        </div>
                            <div class="input-box">
                            <div class="col-6">
                                <label for="gioitinh">Giới tính</label>
                                <br>
                                <select id="gioitinh" name="admin_gioitinh">
                                    <option value="nam">Nam</option>
                                    <option value="nu">Nữ</option>
									<option value="khongxacdinh">khác</option>
                                </select>
                            </div>
							<div class="clear"></div>
                        </div>
						  <div class="input-box">
							<div class="col-6">
                                <label for="chucvu">Chức vụ</label>
                                <br>
                                <select id="chucvu" name="admin_chucvu">
                                <option value="quantrivien">Quản tri viên</option>
                                    <option value="giangvien">Giảng viên</option>
                                    <option value="sinh viên">Sinh viên</option>
									<option value="nhomnguoikhac">Nhóm người khác</option>
                                </select>
                            </div>
                            <div class="clear"></div>
                        </div>
                        <div class="input-box">
							<div class="col-6">
							<label for="phone">SĐT</label>
                                <br>
                                <input type="text" name="admin_phone"  required="required">
                            
                            </div>
                            <div class="clear"></div>
                        </div>
                     
                        <div class="input-box">
                            <label >Avata</label>
                            <br>
							<input type="file" name="admin_img" >
                        </div>
					
                        <div class="btn-box">
                            <button type="submit">
                                Đăng ký
                            </button>
                        </div>
                    </form>
                    </div>

            </div>   

            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


    <style type="text/css">   
            *{
    padding: 0px;
    margin: 0px;
    font-family: sans-serif;
    box-sizing: border-box;
}
.container{
    width: 100%;
    max-width: 1200px;
    margin-left: auto;
    margin-right: auto;
    height:800px;
}
.col-6{
    float: left;
    width: 50%;
}
.margin_b{
    margin-bottom: 7.5px;
}
.clear{
    clear: both;
}

h1{
    color: #009999;
    font-size: 20px;
    margin-bottom: 30px;
}

.register-form{
    width: 100%;
    max-width: 1200px;
    margin: auto;
    background-color: #ecf0f5;
    padding: 15px;
    border: 2px dotted #cccccc;
    border-radius: 10px;
    margin-top: 50px;
    
  
}

.input-box{
    margin-bottom: 10px;
}
.input-box input[type='text'],
.input-box input[type='password'],
.input-box input[type='date']{
    padding: 7.5px 12px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    display: inline-block;
    height: 40px;
    color: #666666;
}
.input-box select{
    padding: 7.5px 15px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    display: inline-block;
    height: 40px;
    color: #666666;
}
.input-box option{
    font-size: 16px;
}
.input-box input[type='checkbox']{
    height: 1.5em;
    width: 1.5em;
    vertical-align: middle;
    line-height: 2em;
}
.input-box textarea{
    padding: 7.5px 15px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    min-height: 120px;
    color: #666666;
}
.btn-box{
    text-align: right;
    margin-top: 30px;
}
.btn-box button{
    padding: 7.5px 15px;
    border-radius: 2px;
    background-color: #009999;
    color: #ffffff;
    border: none;
    outline: none;
}</style>  

@endsection 