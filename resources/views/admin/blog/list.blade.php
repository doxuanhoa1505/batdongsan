@extends('admin')
@section('contensen')


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
      

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">DataTable with default features</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>STT</th>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Type</th>
                  <th>Mô Tả</th>
                  <th>Admin</th>
                  <th>User</th>
                  <th>Img</th>
                  <th>Trạng Thái</th>
                  <th>Tác Vụ</th>
                </tr>
                </thead>
                <tbody>
               @foreach($all_blog as $key=>$data)
                <tr>
                
                <td>{{++$key}}</td>
                <td>{{$data->blog_id}}</td>
                <td>{{$data->blog_name}}</td>
                <td>{{$data->type_blog_name}}</td>
                <td>{{$data->blog_des}}</td>
                <td>{{$data->id_admin}}</td>
                <td>{{$data->id_user_post}}</td>
                <td>{{$data->blog_img}}</td>
               
                  <td>
                   <?php
                   if($data->blog_stt==0){
                   ?>
                  <a href="{{URL::to('/admin/unactive-blog/'.$data->blog_id)}}"><span class="fa-thum-styling fa fa-thumbs-down"></span><br>Không duyệt bài</a>
                  <?php }else{ ?>
                  <a href="{{URL::to('/admin/active-blog/'.$data->blog_id)}}"><span class="fa-thum-styling fa fa-thumbs-up"> </span><br>Duyệt bài</a>
                  <?php  }
                   ?>
                  </td>
                  <td>  <a href="{{URL::to('/admin/edit-blog/'.$data->blog_id)}}"> <span  class="nut-bam" style="font-family: arial;">Xem thử</span></a>
                  <style>
                  .nut-bam a:hover {
background:  !important;
color: white !important;
border-radius:5px;
}

.nut-bam a:link, .nut-bam a:visited {
width: fit-content;
padding: 0.5em 1em;
text-align: center;
float: inherit;
margin: 0em auto;
color: #ffffff;
background: #00000026;
border-radius:5px;
}
                  </style>
                        <a href="#"> <input type="image" src="{{('public/backend//buton/email.png')}}" alt="Submit" width="40" height="40"></a>
                        <a href="{{URL::to('/admin/delete-blog/'.$data->blog_id)}}">  <input type="image" src="{{('public/backend/buton/xoa.jpg')}}" alt="Submit" width="40" height="40"></a>
                                            </td>
                 
                </tr> 
                @endforeach
              
                </tbody>
                <tfoot>
                <tr>
                <th>STT</th>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Type</th>
                  <th>Mô Tả</th>
                  <th>Admin</th>
                  <th>User</th>
                  <th>Img</th>
                  <th>Trạng Thái</th>
                  <th>Tác Vụ</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>



@endsection 