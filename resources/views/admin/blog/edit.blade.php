@extends('admin')
@section('contensen')



    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
      

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">DataTable with default features</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @foreach($edit_blog as $key => $data)
                     <form action="{{URL::to('/admin/update-blog/'.$data->blog_id)}}" method="post" enctype="multipart/form-data">
                     <?php
                            $message = Session::get('message');
                            if($message){
                                echo '<span class="text-alert">'.$message.'</span>';
                                Session::put('message',null);
                            }
                            ?>
                        <h1>Cập nhật đề tài</h1> {{ csrf_field() }}	
                        
                        
                        <div class="input-box">
                        <h5> Name </h5>
                            <input type="text" name="blog_name" value="{{$data->blog_name}}">
                        </div>
						<div class="input-box">
                        <h5> Name </h5>
                            <input type="text" name="id_admin" value="{{$data->id_admin}}">
                        </div>
						<div class="input-box">
                        <h5> Name </h5>
                            <input type="text" name="id_user_post" value="{{$data->id_user_post}}">
                        </div>
						<div class="input-box">
                        <h5> Name </h5>
                            <input type="text" name="blog_des" value="{{$data->blog_des}}">
                        </div>
                        <div class="input-box">
                            <label >img</label>
                            <br>
                            <img src="{{URL::to('/adminpublic/uploads/blog/'.$data->blog_img)}}" height="100" width="100"> <br>
							<input type="file" name="blog_img" >
                        </div>
                            
                        <div class="input-box">
                            <div class="col-6">
                                <label for="gioitinh">Loại blog</label>
                                <br>
                                <select id="gioitinh" name="type_blog_id">
                                @foreach($type_blog as $key=>$data)
                                    <option value="{{$data->type_blog_id}}">{{$data->type_blog_name}}</option>
                                @endforeach
                                </select>
                            </div>
							<div class="clear"></div>
                        </div>
                      
						
                        
					
                        <div class="btn-box">
                            <button type="submit">
                                Đăng ký
                            </button>
                        </div>
                    </form>@endforeach
                    

            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


    <style type="text/css">   
            *{
    padding: 0px;
    margin: 0px;
    font-family: sans-serif;
    box-sizing: border-box;
}
.container{
    width: 100%;
    max-width: 1200px;
    margin-left: auto;
    margin-right: auto;
    height:800px;
}
.col-6{
    float: left;
    width: 50%;
}
.margin_b{
    margin-bottom: 7.5px;
}
.clear{
    clear: both;
}

h1{
    color: #009999;
    font-size: 20px;
    margin-bottom: 30px;
}

.register-form{
    width: 100%;
    max-width: 1200px;
    margin: auto;
    background-color: #ecf0f5;
    padding: 15px;
    border: 2px dotted #cccccc;
    border-radius: 10px;
    margin-top: 50px;
    
  
}

.input-box{
    margin-bottom: 10px;
}
.input-box input[type='text'],
.input-box input[type='password'],
.input-box input[type='date']{
    padding: 7.5px 12px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    display: inline-block;
    height: 40px;
    color: #666666;
}
.input-box select{
    padding: 7.5px 15px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    display: inline-block;
    height: 40px;
    color: #666666;
}
.input-box option{
    font-size: 16px;
}
.input-box input[type='checkbox']{
    height: 1.5em;
    width: 1.5em;
    vertical-align: middle;
    line-height: 2em;
}
.input-box textarea{
    padding: 7.5px 15px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    min-height: 120px;
    color: #666666;
}
.btn-box{
    text-align: right;
    margin-top: 30px;
}
.btn-box button{
    padding: 7.5px 15px;
    border-radius: 2px;
    background-color: #009999;
    color: #ffffff;
    border: none;
    outline: none;
}</style>  

@endsection 