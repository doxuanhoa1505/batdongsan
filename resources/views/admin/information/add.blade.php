@extends('admin')
@section('contensen')



    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
      

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">DataTable with default features</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                     <form action="{{URL::to('/admin/save-information')}}" method="post" enctype="multipart/form-data">	<?php
                            $message = Session::get('message');
                            if($message){
                                echo '<span class="text-alert">'.$message.'</span>';
                                Session::put('message',null);
                            }
                            ?>{{ csrf_field() }}
                   
                        <div class="input-box">
                        <div class="col-6">
                        <label for="gioitinh"> Tên tài sản</label>
                                <br>
                            <input type="text" placeholder="Nhập name" required="required" name="inf_name">
                        </div>
                        <div class="col-6">
                        <label for="gioitinh"> Địa chỉ</label>
                                <br>
                            <input type="text" placeholder="Nhập địa chỉ" required="required" name="inf_address">
                        </div> </div>
						<div class="input-box">
                        <label for="gioitinh"> Mô tả</label>
                                <br>
                            <input type="text" placeholder="Mô tả" required="required" name="inf_des">
                        </div>


                        <div class="input-box">
                        <div class="col-3">
                                <label for="gioitinh"> ID Chủ</label>
                                <br>
                                <input type="text" placeholder="ID" required="required" name="id_user">
                            </div> 
                            <div class="col-3">
                                <label for="gioitinh"> Loại tài sản</label>
                                <br>
                                <select id="gioitinh" name="id_type">
                                @foreach($type as $key=>$data)
                                    <option value="{{$data->type_id}}">{{$data->type_name}}</option>
                                @endforeach
								
                                </select>
                            </div>
                            <div class="col-3">
                                <label for="gioitinh">Dạng sở hữu </label>
                                <br>
                                <select id="gioitinh" name="inf_to_own">
                                    <option value="Mua">Mua bán</option>
                                    <option value="Thuê">Thuê</option>
								
                                </select>
                            </div>
                            <div class="col-3">
                                <label for="gioitinh"> Giá</label>
                                <br>
                                <input type="text" placeholder="Giá" required="required" name="inf_price">
                            </div> 
                            <div class="clear"></div>
                        </div>
                    
            <div class="card-header">
              <h3 class="card-title">Chi tiết nhà</h3>
            </div> 
                            <div class="input-box">
                            <div class="col-2">
                                <label for="gioitinh"> Điều hòa</label>
                                <br>
                                <select id="gioitinh" name="inf_air_conditioning">
                                    <option value="Có">Có</option>
                                    <option value="Không">Không</option>
								
                                </select>
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Bếp</label>
                                <br>
                                <select id="gioitinh" name="inf_barbeque">
                                    <option value="Có">Có</option>
                                    <option value="Không">Không</option>
								
                                </select>
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Máy sấy</label>
                                <br>
                                <select id="gioitinh" name="inf_dryer">
                                    <option value="Có">Có</option>
                                    <option value="Không">Không</option>
								
                                </select>
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Phòng Gym</label>
                                <br>
                                <select id="gioitinh" name="inf_gym">
                                    <option value="Có">Có</option>
                                    <option value="Không">Không</option>
								
                                </select>
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Khu vực giặt</label>
                                <br>
                                <select id="gioitinh" name="inf_laundry">
                                    <option value="Có">Có</option>
                                    <option value="Không">Không</option>
								
                                </select>
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Lò vi sóng</label>
                                <br>
                                <select id="gioitinh" name="inf_microwave">
                                    <option value="Có">Có</option>
                                    <option value="Không">Không</option>
								
                                </select>
                            </div>
                            <div class="clear"></div>
                            <div class="col-2">
                                <label for="gioitinh"> Vòi tắm ngoài trời</label>
                                <br>
                                <select id="gioitinh" name="inf_outdoor_shower">
                                    <option value="Có">Có</option>
                                    <option value="Không">Không</option>
								
                                </select>
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Tủ lạnh</label>
                                <br>
                                <select id="gioitinh" name="inf_refrigerator">
                                    <option value="Có">Có</option>
                                    <option value="Không">Không</option>
								
                                </select>
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Phòng tắm hơi</label>
                                <br>
                                <select id="gioitinh" name="inf_sauna">
                                    <option value="Có">Có</option>
                                    <option value="Không">Không</option>
								
                                </select>
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Hồ bơi</label>
                                <br>
                                <select id="gioitinh" name="inf_swimming_pool">
                                    <option value="Có">Có</option>
                                    <option value="Không">Không</option>
								
                                </select>
                            </div>
                            <div class="col-2">
                                <label for="gioitinh">Kích thước hồ bơi(m2)</label>
                                <br>
                                <input type="text"  required="required"  name="inf_pool_size">
                                    
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> TiVi</label>
                                <br>
                                <select id="gioitinh" name="inf_tv">
                                    <option value="Có">Có</option>
                                    <option value="Không">Không</option>
								
                                </select>
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Máy giặt</label>
                                <br>
                                <select id="gioitinh" name="inf_washer">
                                    <option value="Có">Có</option>
                                    <option value="Không">Không</option>
								
                                </select>
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Wifi</label>
                                <br>
                                <select id="gioitinh" name="inf_wifi">
                                    <option value="Có">Có</option>
                                    <option value="Không">Không</option>
								
                                </select>
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Số cửa sổ </label>
                                <br><input type="text"  required="required" name="inf_window">
                                
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Số phòng ngủ</label>
                                <br>
                                <input type="text"  required="required"name="inf_bedrooms">
                                    
                            </div>
                            <div class="col-2">
                                <label for="gioitinh">Số phòng tắm </label>
                                <br>
                                <input type="text"  required="required" name="inf_bathrooms">
                                   
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Nhà để xe</label>
                                <br>
                                <select id="gioitinh" name="inf_garage">
                                    <option value="Có">Có</option>
                                    <option value="Không">Không</option>
								
                                </select>
                            </div>
                            <div class="col-2">
                                <label for="gioitinh">Diện tích garage(m2)</label>
                                <br>
                                <input type="text"  required="required" name="inf_garage_size">
                                   
                            </div>
                            <div class="col-2">
                                <label for="gioitinh">Diện tích nhà (m2)</label>
                                <br>
                                <input type="text"  required="required" name="inf_property_size">
                                   
                            </div>
						
                            <div class="col-4">
                            <label for=""> Năm xây đựng</label>
                            <input type="datetime-local"  required="required" name="inf_year_built">
                            </div>
                            <div class="col-4">
                            <label for=""> Thời gian sửa gần nhất</label>
                            <input type="datetime-local"  required="required" name="inf_last_remodel_year">
                            </div>
							<div class="clear"></div>
                        </div>
						
                        <div class="input-box">
                            <div class="col-6">
                                <label for="gioitinh">Tình Trạng</label>
                                <br>
                                <select id="gioitinh" name="inf_stt">
                                    <option value="0">Ẩn</option>
                                    <option value="1">Hiện</option>
									
                                </select>
                            </div>
							<div class="clear"></div>
                        </div>
						 
                       
                        <div class="input-box">
                            <label >Hình ảnh</label>
                            <br>
							<input type="file" name="inf_img" >
                        </div>
					
                        <div class="btn-box">
                            <button type="submit">
                                Đăng ký
                            </button>
                        </div>
					
                    </form>
                    

            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


    <style type="text/css">   
            *{
    padding: 0px;
    margin: 0px;
    font-family: sans-serif;
    box-sizing: border-box;
}

.col-6{
    float: left;
    width: 50%;
}
.col-4{
    float: left;
    width: 35%;
}
.col-3 {
    float: left;
    width: 25%;
}
.col-2{
    float: left;
    width: 20%;
}

.margin_b{
    margin-bottom: 7.5px;
}
.clear{
    clear: both;
}

h1{
    color: #009999;
    font-size: 20px;
    margin-bottom: 30px;
}

.register-form{
    width: 100%;
    max-width: 1200px;
    margin: auto;
    background-color: #ecf0f5;
    padding: 15px;
    border: 2px dotted #cccccc;
    border-radius: 10px;
    margin-top: 50px;
    
  
}

.input-box{
    margin-bottom: 10px;
}
.input-box input[type='text'],
.input-box input[type='password'],
.input-box input[type='datetime-local'],
.input-box input[type='date']
{
    padding: 7.5px 12px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    display: inline-block;
    height: 40px;
    color: #666666;
}
.input-box select{
    padding: 7.5px 15px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    display: inline-block;
    height: 40px;
    color: #666666;
}
.input-box option{
    font-size: 16px;
}
.input-box input[type='checkbox']{
    height: 1.5em;
    width: 1.5em;
    vertical-align: middle;
    line-height: 2em;
}
.input-box textarea{
    padding: 7.5px 15px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    min-height: 120px;
    color: #666666;
}
.btn-box{
    text-align: right;
    margin-top: 30px;
}
.btn-box button{
    padding: 7.5px 15px;
    border-radius: 2px;
    background-color: #009999;
    color: #ffffff;
    border: none;
    outline: none;
}</style>  

@endsection 