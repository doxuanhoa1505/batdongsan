@extends('admin')
@section('contensen')


    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
      

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">DataTable with default features</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>STT</th>
                  <th>Id_info</th>
                  <th>Name  </th>
                  <th>Trạng Thái</th>
                  <th>Chi tiết</th>
                  <th>Tác Vụ</th>
                </tr>
                </thead>
                <tbody>
               @foreach($all_information as $key=>$data)
                <tr>
                
                <td>{{++$key}}</td>
                <td>{{$data->inf_id}}</td>
                <td>{{$data->inf_name}}</td>
               
                  <td>
                   <?php
                   if($data->inf_stt==0){
                   ?>
                  <a href="{{URL::to('/admin/unactive-information/'.$data->inf_id)}}"><span class="fa-thum-styling fa fa-thumbs-down"></span><br>Không duyệt bài</a>
                  <?php }else{ ?>
                  <a href="{{URL::to('/admin/active-information/'.$data->inf_id)}}"><span class="fa-thum-styling fa fa-thumbs-up"> </span><br>Duyệt bài</a>
                  <?php  }
                   ?>
                  </td>
                  <td>  <a href="{{URL::to('/admin/xem-chi-tiet/'.$data->inf_id)}}"> <input type="image" src="{{asset('public/backend/buton/xem.png')}}" alt="Submit" width="70" height="40"></a>
                        
                                            </td>
                  <td>  <a href="{{URL::to('/admin/edit-information/'.$data->inf_id)}}"> <input type="image" src="{{asset('public/backend/buton/sua.png')}}" alt="Submit" width="40" height="40"></a>
                        <a href="#"> <input type="image" src="{{asset('public/backend//buton/email.png')}}" alt="Submit" width="40" height="40"></a>
                        <a href="{{URL::to('/admin/delete-information/'.$data->inf_id)}}">  <input type="image" src="{{asset('public/backend//buton/xoa.png')}}" alt="Submit" width="40" height="40"></a>
                                            </td>
                 
                </tr> 
                @endforeach
              
                </tbody>
                <tfoot>
                <tr>
                <th>STT</th>
                  <th>Id_info</th>
                  <th>Name  </th>
                  <th>Trạng Thái</th>
                  <th>Chi tiết</th>
                  <th>Tác Vụ</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>



@endsection 