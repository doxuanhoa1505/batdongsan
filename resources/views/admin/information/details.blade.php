@extends('admin')
@section('contensen')



     <!-- Main content -->
     <section class="content">
      <div class="row">
    
        <div class="col-12">
      

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">DataTable with default features</h3>
            </div>
            
            <!-- /.card-header -->
            <div class="card-body">  @foreach($info_details as $key => $data)
                        <div class="input-box">
                        <div class="col-6">
                        <label for="gioitinh"> Tên tài sản:{{$data->inf_name}}</label>
                                
                        </div>
                        <div class="col-6">
                        <label for="gioitinh"> Địa chỉ:{{$data->inf_address}}</label>
                              
                        </div> </div>
						<div class="input-box">
                        <label for="gioitinh"> Mô tả:{{$data->inf_des}}</label>
                              
                            
                        </div>


                        <div class="input-box">
                        <div class="col-3">
                                <label for="gioitinh"> ID Chủ:{{$data->id_user}}</label>
                                
                            </div> 
                            <div class="col-3">
                                <label for="gioitinh"> Loại tài sản:{{$data->type_name}}</label>
                                
                            </div>
                            <div class="col-3">
                                <label for="gioitinh">Dạng sở hữu:{{$data->inf_to_own}} </label>
                                
                            </div>
                            <div class="col-3">
                                <label for="gioitinh"> Giá:{{$data->inf_price}}</label>
                                 </div> 
                            <div class="clear"></div>
                        </div>
                    
                                <div class="card-header">
                                <h3 class="card-title">Chi tiết nhà</h3>
                                </div> 
                            <div class="input-box">
                            <div class="col-2">
                                <label for="gioitinh"> Điều hòa:{{$data->inf_air_conditioning}}</label>
                                
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Bếp:{{$data->inf_barbeque}}</label>
                                
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Máy sấy:{{$data->inf_dryer}}</label>
                               
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Phòng Gym:{{$data->inf_gym}}</label>
                                
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Khu vực giặt:{{$data->inf_laundry}}</label>
                                <br>
                                
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Lò vi sóng:{{$data->inf_microwave}}</label>
                                
                            </div>
                            <div class="clear"></div>
                            <div class="col-2">
                                <label for="gioitinh"> Vòi tắm ngoài trời:{{$data->inf_outdoor_shower}}</label>
                                
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Tủ lạnh:{{$data->inf_refrigerator}}</label>
                                
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Phòng tắm hơi:{{$data->inf_sauna}}</label>
                                <br>
                                
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Hồ bơi:{{$data->inf_swimming_pool}}</label>
                                
                            </div>
                            <div class="col-2">
                                <label for="gioitinh">Kích thước hồ bơi:{{$data->inf_pool_size}}(m2)</label>
                                 
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> TiVi:{{$data->inf_tv}}</label>
                               
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Máy giặt:{{$data->inf_washer}}</label>
                                
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Wifi:{{$data->inf_wifi}}</label>
                                
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Số cửa sổ:{{$data->inf_window}} </label>
                                
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Số phòng ngủ:{{$data->inf_bedrooms}} phòng</label>
                               
                            </div>
                            <div class="col-2">
                                <label for="gioitinh">Số phòng tắm :{{$data->inf_bathrooms}} phòng</label>
                                 
                            </div>
                            <div class="col-2">
                                <label for="gioitinh"> Nhà để xe:{{$data->inf_garage}}</label>
                                
                            </div>
                            <div class="col-2">
                                <label for="gioitinh">Diện tích garage:{{$data->inf_garage_size}}(m2)</label>
                               
                            </div>
                            <div class="col-2">
                                <label for="gioitinh">Diện tích nhà:{{$data->inf_property_size}}(m2)</label>
                           
                                   
                            </div>
						
                            <div class="col-2">
                            <label for=""> Năm xây đựng:{{$data->inf_year_built}}</label>
                            
                            </div>
                            <div class="col-2">
                            <label for=""> Thời gian sửa gần nhất:{{$data->inf_last_remodel_year}}</label>
                            
                            </div>
							<div class="clear"></div>
                        </div>
						
                      
                       
                        <div class="input-box">
                            <label >Hình ảnh</label>
                            <br>
							<img src="{{URL::to('/public/uploads/information/'.$data->inf_img)}}"width="200" height="70">  
                            </div> 
                           
                        <form action="{{URL::to('/admin/save-information_image')}}" method="post" enctype="multipart/form-data">	<?php
                            $message = Session::get('message');
                            if($message){
                                echo '<span class="text-alert">'.$message.'</span>';
                                Session::put('message',null);
                            }
                            ?>{{ csrf_field() }}
                            <div class="input-boxx">
                        <div class="col-0">
                        <label for="gioitinh"> </label>
                                <br>
                            <input type="text"  required="required" name="id_info" value="{{$data->inf_id}}">
                        </div></div>
                            <div class="input-box">
                           
                            <label >Thêm hình ảnh cho tài sản</label>
                            <br>
							<input type="file" name="info_image" >
                        </div>
                        <div class="btn-boxx">
                            <button type="submit">
                                Thêm ảnh
                            </button>
                        </div>
                        </form>







                        <label >Hình ảnh đi kèm</label>

                        <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>STT</th>
                  <th>Id</th>
                 <th>img</th>
                <th>Trạng Thái</th>
                  <th>Tác Vụ</th>
                </tr>
                </thead>
                <tbody>
                @foreach($infoimg as $key => $data)
                <tr>
                
                <td>{{++$key}}</td>
                <td>{{$data->info_id}}</td>
                <td>  <img src="{{URL::to('/public/uploads/information/'.$data->info_image)}}"width="200" height="70"> </td>
               
                  <td>
                   <?php
                   if($data->info_image_stt==0){
                   ?>
                  <a href="{{URL::to('/admin/unactive-information_image/'.$data->info_id)}}"><span class="fa-thum-styling fa fa-thumbs-down"></span><br>Không duyệt bài</a>
                  <?php }else{ ?>
                  <a href="{{URL::to('/admin/active-information_image/'.$data->info_id)}}"><span class="fa-thum-styling fa fa-thumbs-up"> </span><br>Duyệt bài</a>
                  <?php  }
                   ?>
                  </td>
                  
                  <td>      <a href="{{URL::to('/admin/delete-information_image/'.$data->info_id)}}">  Delete</a>
                                            </td>
                 
                </tr> 
                @endforeach
              
                </tbody> 
                <tfoot>
                <tr>
         
                <th>STT</th>
                  <th>Id</th>
                 <th>img</th>
                <th>Trạng Thái</th>
                  <th>Tác Vụ</th>
                </tr>
                </tfoot>
              </table>
              
        <!-- /.col -->
                         
            <!-- /.card-body -->
          </div> @endforeach
          <!-- /.card -->
        </div>
      
                          
      </div>
      <!-- /.row -->
    </section>


    <style type="text/css">   
            *{
    padding: 0px;
    margin: 0px;
    font-family: sans-serif;
    box-sizing: border-box;
}

.col-6{
    float: left;
    width: 50%;
}
.col-4{
    float: left;
    width: 35%;
}
.col-3 {
    float: left;
    width: 25%;
}
.col-2{
    float: left;
    width: 20%;
}
.col-0{
    float: left;
    width: 0%;
}

.margin_b{
    margin-bottom: 7.5px;
}
.clear{
    clear: both;
}

h1{
    color: #009999;
    font-size: 20px;
    margin-bottom: 30px;
}

.register-form{
    width: 100%;
    max-width: 1200px;
    margin: auto;
    background-color: #ecf0f5;
    padding: 15px;
    border: 2px dotted #cccccc;
    border-radius: 10px;
    margin-top: 50px;
    
  
}

.input-box{
    margin-bottom: 10px;
}
.input-box input[type='text'],
.input-box input[type='password'],
.input-box input[type='date']{
    padding: 7.5px 12px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    display: inline-block;
    height: 40px;
    color: #666666;
}.col-0{
    float: left;
    width: 0%;
}
.input-boxx{
    margin-bottom: 0px;}
.input-boxx input[type='text'],
.input-boxx input[type='password'],
.input-boxx input[type='date']
{
    padding: 7.5px 12px;
    width: 100%;
    border: 0px solid #cccccc;
    outline: none;
    font-size: 0px;
    display: inline-block;
    height: 0px;
    color: #666666;
}
.input-box select{
    padding: 7.5px 15px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    display: inline-block;
    height: 40px;
    color: #666666;
}
.input-box option{
    font-size: 16px;
}
.input-box input[type='checkbox']{
    height: 1.5em;
    width: 1.5em;
    vertical-align: middle;
    line-height: 2em;
}
.input-box textarea{
    padding: 7.5px 15px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    min-height: 120px;
    color: #666666;
}
.btn-box{
    text-align: right;
    margin-top: 30px;}
.btn-box button{
    padding: 7.5px 15px;
    border-radius: 2px;
    background-color: #009999;
    color: #ffffff;
    border: none;
    outline: none;}

.input-boxx{
    margin-bottom: 0px;}
.input-boxx input[type='text'],
.input-boxx input[type='password'],
.input-boxx input[type='date']
{
    padding: 7.5px 12px;
    width: 100%;
    border: 0px solid #cccccc;
    outline: none;
    font-size: 0px;
    display: inline-block;
    height: 0px;
    color: #666666;
}
.btn-boxx{
    text-align: left;
    margin-top: 30px;
}
.btn-boxx button{
    padding: 7.5px 15px;
    border-radius: 2px;
    background-color: #009999;
    color: #ffffff;
    border: none;
    outline: none;
}</style>  


@endsection 