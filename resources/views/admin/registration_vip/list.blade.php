@extends('admin')
@section('contensen')



    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
      

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">DataTable with default features</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
        
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>STT</th>
                  <th>Người đăng ký</th>
                  <th>Loại Vip </th>
                  <th>Thời gian bắt đầu </th>
                  <th>Thời gian kết thúc</th>
                  <th>Trạng thái</th>
                  <th>Tác Vụ</th>
                </tr>
                </thead>
                <tbody>
               @foreach($all_vip_on as $key=>$data)
                <tr>
                
                <td>{{++$key}}</td>
                <td>{{$data->user_name}}</td>
                <td>{{$data->vip_name}}:{{$data->vip_time}} </td>
                <td>{{$data->regi_vip_time_on}}</td>
                <td>{{$data->regi_vip_time_off}}</td>
               
                  <td>
                   <?php
                   if($data->regi_vip_stt==0){
                   ?>
                  <a href="{{URL::to('/admin/unactive-registration_vip/'.$data->regi_vip_id)}}"><span class="fa-thum-styling fa fa-thumbs-down"></span><br>Không duyệt bài</a>
                  <?php }else{ ?>
                  <a href="{{URL::to('/admin/active-registration_vip/'.$data->regi_vip_id)}}"><span class="fa-thum-styling fa fa-thumbs-up"> </span><br>Duyệt bài</a>
                  <?php  }
                   ?>
                  </td>
                  <td>  <a href="{{URL::to('/admin/edit-registration_vip/'.$data->regi_vip_id)}}"> <input type="image" src="{{asset('public/backend/buton/sua.png')}}" alt="Submit" width="40" height="40"></a>
                        <a href="#"> <input type="image" src="{{asset('public/backend/buton/email.png')}}" alt="Submit" width="40" height="40"></a>
                        <a href="{{URL::to('/admin/delete-registration_vip/'.$data->regi_vip_id)}}">  <input type="image" src="{{asset('public/backend/buton/xoa.png')}}" alt="Submit" width="40" height="40"></a>
                                            </td>
                 
                </tr> 
                @endforeach
              
                </tbody>
                <tfoot>
                <tr>
                  <th>STT</th>
                  <th>Người đăng ký</th>
                  <th>Loại Vip </th>
                  <th>Thời gian bắt đầu </th>
                  <th>Thời gian kết thúc</th>
                  <th>Trạng thái</th>
                  <th>Tác Vụ</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <div class="card-body">
            <h2>Hết Vip</h2>
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>STT</th>
                  <th>Người đăng ký</th>
                  <th>Loại Vip </th>
                  <th>Thời gian bắt đầu </th>
                  <th>Thời gian kết thúc</th>
                  <th>Trạng thái</th>
                  <th>Tác Vụ</th>
                </tr>
                </thead>
                <tbody>
               @foreach($all_vip_off as $key=>$data)
                <tr>
                
                <td>{{++$key}}</td>
                <td>{{$data->user_name}}</td>
                <td>{{$data->vip_name}}:{{$data->vip_time}} ngày</td>
                <td>{{$data->regi_vip_time_on}}</td>
                <td>{{$data->regi_vip_time_off}}</td>
               
                  <td>
                   <?php
                   if($data->regi_vip_stt==0){
                   ?>
                  <a href="{{URL::to('/admin/unactive-registration_vip/'.$data->regi_vip_id)}}"><span class="fa-thum-styling fa fa-thumbs-down"></span><br>Không duyệt bài</a>
                  <?php }else{ ?>
                  <a href="{{URL::to('/admin/active-registration_vip/'.$data->regi_vip_id)}}"><span class="fa-thum-styling fa fa-thumbs-up"> </span><br>Duyệt bài</a>
                  <?php  }
                   ?>
                  </td>
                  <td>  <a href="{{URL::to('/admin/edit-registration_vip/'.$data->regi_vip_id)}}"> <input type="image" src="{{asset('public/backend/buton/sua.png')}}" alt="Submit" width="40" height="40"></a>
                        <a href="#"> <input type="image" src="{{asset('public/backend/buton/email.png')}}" alt="Submit" width="40" height="40"></a>
                        <a href="#"> <input type="image" src="{{asset('public/backend/buton/vip.png')}}" alt="Submit" width="40" height="40"></a>
                        <a href="{{URL::to('/admin/delete-registration_vip/'.$data->regi_vip_id)}}">  <input type="image" src="{{asset('public/backend/buton/xoa.png')}}" alt="Submit" width="40" height="40"></a>
                                            </td>
                 
                </tr> 
                @endforeach
              
                </tbody>
                <tfoot>
                <tr>
                  <th>STT</th>
                  <th>Người đăng ký</th>
                  <th>Loại Vip </th>
                  <th>Thời gian bắt đầu </th>
                  <th>Thời gian kết thúc</th>
                  <th>Trạng thái</th>
                  <th>Tác Vụ</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>



@endsection 