@extends('admin')
@section('contensen')



    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
      

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">DataTable with default features</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                @foreach($edit_type  as $key => $data)
                     <form action="{{URL::to('/admin/update-registration_vip/'.$data->regi_vip_id)}}" method="post" enctype="multipart/form-data">
                     <?php
                            $message = Session::get('message');
                            if($message){
                                echo '<span class="text-alert">'.$message.'</span>';
                                Session::put('message',null);
                            }
                            ?>
                        <h1>Cập nhật đề tài</h1> {{ csrf_field() }}	
                        
                        
                        <div class="input-boxx">
                      <div class="col-0">
                      <label for="gioitinh"> </label>
             <br>
                         <input type="text" name="vip_id" value="{{$data->vip_id}}">
                         
                     </div></div>
                     <div class="input-box">
                     <div class="col-6">
                         <label for="gioitinh"> User</label>
                         <br>
                     <input type="text"  name="user_id" value="{{$data->user_id}}">

                     </div>
                    
                     <div class="clear"></div>
                 </div>
                 
                 <div class="input-box">
                     <div class="col-6">
                     <label for="gioitinh">Nhập thời gian bắt đầu </label>
                         <br>
                     <input type="date" required="required" name="regi_vip_time_on" value="<?php  $today = date('Y-m-d');
                            echo $today;?>">
                     </div>
                     <div class="col-6">
                     <label for="gioitinh"> Nhập thời gian kết thúc</label>
                         <br>
                     <input type="date" required="required" name="regi_vip_time_off" value="<?php  $today = date('Y-m-d');
 
  
  $week = strtotime(date("Y-m-d", strtotime($today)) . " + {$data->vip_time}{$data->vip_name}");
  $week = strftime("%Y-%m-%d", $week);
  echo  $week;
?>">
                     </div>
                     <div class="clear"></div>
                 </div>
                     
                 
                 
             
                 <div class="btn-box">
                     <button type="submit">
                         Đăng ký
                     </button>
                 </div>@endforeach
                 

            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


    <style type="text/css">   
            *{
    padding: 0px;
    margin: 0px;
    font-family: sans-serif;
    box-sizing: border-box;
}
.container{
    width: 100%;
    max-width: 1200px;
    margin-left: auto;
    margin-right: auto;
    height:800px;
}
.col-6{
    float: left;
    width: 50%;
    
}
.col-0{
    float: left;
    width: 0%;
}
.margin_b{
    margin-bottom: 7.5px;
}
.clear{
    clear: both;
}

h1{
    color: #009999;
    font-size: 20px;
    margin-bottom: 30px;
}

.register-form{
    width: 100%;
    max-width: 1200px;
    margin: auto;
    background-color: #ecf0f5;
    padding: 15px;
    border: 2px dotted #cccccc;
    border-radius: 10px;
    margin-top: 50px;
    
  
}

.input-box{
    margin-bottom: 10px;
}
.input-box input[type='text'],
.input-box input[type='password'],
.input-box input[type='date']{
    padding: 7.5px 12px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    display: inline-block;
    height: 40px;
    color: #666666;
}
.input-box select{
    padding: 7.5px 15px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    display: inline-block;
    height: 40px;
    color: #666666;
}
.input-box option{
    font-size: 16px;
}
.input-box input[type='checkbox']{
    height: 1.5em;
    width: 1.5em;
    vertical-align: middle;
    line-height: 2em;
}
.input-box textarea{
    padding: 7.5px 15px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    min-height: 120px;
    color: #666666;
}
.btn-box{
    text-align: right;
    margin-top: 30px;
}
.btn-box button{
    padding: 7.5px 15px;
    border-radius: 2px;
    background-color: #009999;
    color: #ffffff;
    border: none;
    outline: none;
    
}.input-boxx{
    margin-bottom: 0px;}
.input-boxx input[type='text'],
.input-boxx input[type='password'],
.input-boxx input[type='date']
{
    padding: 7.5px 12px;
    width: 100%;
    border: 0px solid #cccccc;
    outline: none;
    font-size: 0px;
    display: inline-block;
    height: 0px;
    color: #666666;
}
.btn-boxx{
    text-align: left;
    margin-top: 30px;
}
.btn-boxx button{
    padding: 7.5px 15px;
    border-radius: 2px;
    background-color: #009999;
    color: #ffffff;
    border: none;
    outline: none;
}</style>  

@endsection 