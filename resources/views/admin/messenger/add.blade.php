@extends('admin')
@section('contensen')

   <!-- Content Header (Page header) -->
   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Compose</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Compose</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-3">
            <a href="{{URL::to('/admin/all-messenger/')}}" class="btn btn-primary btn-block mb-3">Back to Inbox</a>

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Folders</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                </div>
              </div>
              <div class="card-body p-0">
                <ul class="nav nav-pills flex-column">
                @foreach($all_user as $key => $data)
                  <li class="nav-item">
                    <a href="{{URL::to('/admin/sent-email-user/'.$data->user_id)}}" class="nav-link">{{$data->user_name}}<br>
                      <i class="far fa-envelope"></i> {{$data->user_email}}
                    </a> 
                     
                  </li>
                  @endforeach
                </ul>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
       
            <!-- /.card -->
          </div>
          <!-- /.col -->
          <div class="col-md-9">
            <div class="card card-primary card-outline">
              <div class="card-header">
                <h3 class="card-title">Compose New Message</h3>
              </div>
              <!-- /.card-header --> 
               <form action="{{URL::to('/admin/save-messenger')}}" method="post" enctype="multipart/form-data">	<?php
                            $message = Session::get('message');
                            if($message){
                                echo '<span class="text-alert">'.$message.'</span>';
                                Session::put('message',null);
                            }
                            ?>{{ csrf_field() }}
              <div class="card-body">
              <div class="form-group">
                  <input class="form-control" placeholder="Theme:"name="mes_name">
                </div>
                <div class="form-group">
                  <input class="form-control" placeholder="To:"name="email_user_receive">
                </div>
                <div class="form-group">
                  <input class="form-control" placeholder="Subject:" name="admin_email" value="<?php
$name = Session::get('admin_email');
if($name){
    echo $name;
}
    ?>">
                </div>
                <div class="form-group">
                    <textarea id="compose-textarea" class="form-control" style="height: 300px" name="mes_des">
                
                    </textarea>
                </div>
              
              </div>
           
              <!-- /.card-body -->
              <div class="card-footer">
                <div class="float-right">
     
                  <button type="submit" class="btn btn-primary"><i class="far fa-envelope"></i> Send</button>
                </div>
                
              </div>
              </form>
              <!-- /.card-footer -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
@endsection 