@extends('admin')
@section('contensen')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>List User</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">List User</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card card-solid">
        <div class="card-body pb-0">
          <div class="row d-flex align-items-stretch">
          @foreach($all_user as $key=>$data)
            <div class="col-12 col-sm-6 col-md-4 d-flex align-items-stretch">
              <div class="card bg-light">
                <div class="card-header text-muted border-bottom-0">
               
                </div>
                <div class="card-body pt-0">
                  <div class="row">
                    <div class="col-7">
                      <h2 class="lead"><b>{{$data->user_name}}</b></h2>
                      <p class="text-muted text-sm"><b>Thông tin: </b> {{$data->user_birthday}}/{{$data->user_sex}} </p>
                      <p class="text-muted text-sm"><b>Email: </b> {{$data->user_email}}</p>
                     
                      <ul class="ml-4 mb-0 fa-ul text-muted">
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-building"></i></span>Địa chỉ:  {{$data->user_address}}</li>
                        <li class="small"><span class="fa-li"><i class="fas fa-lg fa-phone"></i></span> Phone #: {{$data->user_phone}}</li>
                       
                      </ul>
                    </div>
                    <div class="col-5 text-center">
                    <img src="{{URL::to('/public/uploads/user/'.$data->user_img)}}" class="img-circle img-fluid">
                    </div>
                  </div>
                </div>
                <div class="card-footer">
                  <div class="text-right">
                  
                    
                    <a href="#" class="btn btn-sm btn-primary">
                    <ion-icon name="person-circle-outline"></ion-icon> Profile
                    </a>
                    <a href="{{URL::to('/admin/sent-user-email/'.$data->user_id)}}" class="btn btn-sm bg-teal">
                    <ion-icon name="mail-outline">Email</ion-icon>
                     </a>
                    <a href="{{$data->facebook}}" class="btn btn-sm bg-teal">
                      <ion-icon name="logo-facebook" >Facebook</ion-icon>
                    </a>
                    &ensp;&ensp;  &ensp;&ensp;&ensp;
                    
                      <a href="{{URL::to('/admin/edit-user/'.$data->user_id)}}" class="btn btn-sm bg-teal">
                      <ion-icon name="create-outline">Edit</ion-icon>
                     </a>
                     <a href="{{URL::to('/admin/delete-user/'.$data->user_id)}}" class="btn btn-sm bg-teal" style="background:red">
                  <ion-icon name="trash-outline" >Delete</ion-icon>
                     </a>
                   
                  </div>
                </div>
              </div>
            </div>
            @endforeach
          </div>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <nav aria-label="Contacts Page Navigation">
          {!!$all_user->links()!!}
          </nav>
        </div>
        <!-- /.card-footer -->
      </div>
      <!-- /.card -->

    </section>

@endsection 