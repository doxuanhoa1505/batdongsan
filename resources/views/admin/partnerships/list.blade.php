@extends('admin')
@section('contensen')



    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
      

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">DataTable with default features</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>STT</th>
                  <th>Id</th>
                  <th>Name </th>
                  <th>Img </th>
                  <th>Trạng Thái</th>
                  <th>Tác Vụ</th>
                </tr>
                </thead>
                <tbody>
               @foreach($all_partnerships as $key=>$data)
                <tr>
                
                <td>{{++$key}}</td>
                <td>{{$data->par_id}}</td>
                <td>{{$data->par_name}}</td>
                <td>{{$data->par_img}}</td>
               
                  <td>
                   <?php
                   if($data->par_stt==0){
                   ?>
                  <a href="{{URL::to('/admin/unactive-partnerships/'.$data->par_id)}}"><span class="fa-thum-styling fa fa-thumbs-down"></span><br>Không duyệt bài</a>
                  <?php }else{ ?>
                  <a href="{{URL::to('/admin/active-partnerships/'.$data->par_id)}}"><span class="fa-thum-styling fa fa-thumbs-up"> </span><br>Duyệt bài</a>
                  <?php  }
                   ?>
                  </td>
                  <td>  <a href="{{URL::to('/admin/edit-partnerships/'.$data->par_id)}}"> <input type="image" src="{{('public/backend/buton/sua.png')}}" alt="Submit" width="40" height="40"></a>
                        <a href="#"> <input type="image" src="{{('public/backend//buton/email.png')}}" alt="Submit" width="40" height="40"></a>
                        <a href="{{URL::to('/admin/delete-partnerships/'.$data->par_id)}}">  <input type="image" src="{{('public/backend/buton/xoa.jpg')}}" alt="Submit" width="40" height="40"></a>
                                            </td>
                 
                </tr> 
                @endforeach
              
                </tbody>
                <tfoot>
                <tr>
                <th>STT</th>
                  <th>Id</th>
                  <th>Name </th>
                  <th>Img </th>
                  <th>Trạng Thái</th>
                  <th>Tác Vụ</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>



@endsection 