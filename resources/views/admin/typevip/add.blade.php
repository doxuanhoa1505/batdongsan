@extends('admin')
@section('contensen')



    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-12">
      

          <div class="card">
            <div class="card-header">
              <h3 class="card-title">DataTable with default features</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                     <form action="{{URL::to('/admin/save-typevip')}}" method="post" enctype="multipart/form-data" name="myForm" onsubmit="return validateForm()">	<?php
                            $messvip_time = Session::get('messvip_time');
                            if($messvip_time){
                                echo '<span class="text-alert">'.$messvip_time.'</span>';
                                Session::put('messvip_time',null);
                            }
                            ?>{{ csrf_field() }}
                        <h1> Thêm type Vip</h1>
                        <div class="input-box">
                        <div class="col-6">
                        <label for="gioitinh"> Đăng ký theo</label>
                      
                                <br>
                                <select id="gioitinh" name="vip_name">
                                    <option value="week">Theo tuần</option>
                                    <option value="month">Theo tháng</option>
                                    <option value="year">Theo năm</option>
								
                                </select>
                          
                        </div>
                      
                            <div class="col-6">
                            <label for="gioitinh"> Nhập giá</label>
                                <br>
                            <input type="text" placeholder="Nhập giá" required="required" name="vip_price">
                            </div> 
                            </div>
                             <div class="input-box">
                            <div class="col-6">
                            <label for="gioitinh"> Nhập thời gian sử dụng (Nhập số từ 1 ->12)</label>
                                <br>
                            <input type="text" placeholder="Nhập name" required="required" name="vip_time">
                            </div>
			
                            <div class="col-6">
                                <label for="gioitinh"> Trạng Thái</label>
                                <br>
                                <select id="gioitinh" name="vip_stt">
                                    <option value="0">Ẩn</option>
                                    <option value="1">Hiển thị</option>
								
                                </select>
                            </div>
							<div class="clear"></div>
                        </div>
						
                        
					
                        <div class="btn-box">
                            <button type="submit">
                                Đăng ký
                            </button>
                        </div>
                    </form>
                    

            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>


    <style type="text/css">   
            *{
    padding: 0px;
    margin: 0px;
    font-family: sans-serif;
    box-sizing: border-box;
}
.container{
    width: 100%;
    max-width: 1200px;
    margin-left: auto;
    margin-right: auto;
    height:800px;

}
.col-3{
    float: left;
    width: 30%;
}
.col-6{
    float: left;
    width: 50%;
}
.margin_b{
    margin-bottom: 7.5px;
}
.clear{
    clear: both;
}

h1{
    color: #009999;
    font-size: 20px;
    margin-bottom: 30px;
}

.register-form{
    width: 100%;
    max-width: 1200px;
    margin: auto;
    background-color: #ecf0f5;
    padding: 15px;
    border: 2px dotted #cccccc;
    border-radius: 10px;
    margin-top: 50px;
    
  
}

.input-box{
    margin-bottom: 10px;
}
.input-box input[type='text'],
.input-box input[type='password'],
.input-box input[type='date']{
    padding: 7.5px 12px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    display: inline-block;
    height: 40px;
    color: #666666;
}
.input-box select{
    padding: 7.5px 15px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    display: inline-block;
    height: 40px;
    color: #666666;
}
.input-box option{
    font-size: 16px;
}
.input-box input[type='checkbox']{
    height: 1.5em;
    width: 1.5em;
    vertical-align: middle;
    line-height: 2em;
}
.input-box textarea{
    padding: 7.5px 15px;
    width: 100%;
    border: 1px solid #cccccc;
    outline: none;
    font-size: 16px;
    min-height: 120px;
    color: #666666;
}
.btn-box{
    text-align: right;
    margin-top: 30px;
}
.btn-box button{
    padding: 7.5px 15px;
    border-radius: 2px;
    background-color: #009999;
    color: #ffffff;
    border: none;
    outline: none;
}</style> 
<script type="text/javascript">
// JavaScript Document
function validateForm() {
    //Nhập thời gian muốn mua
var vip_time = document.forms["myForm"]["vip_time"].value;
var checkvip_time = isNaN(vip_time);
if ((vip_time == "") || (vip_time <= 0)) {
alert("Thời gian không được để trống và phải lớn hơn  0");
return false;
}
if (vip_time > 12) {
alert("Thời gian mua không được lớn hơn 12");
return false;
}
if (checkvip_time == true) {
alert("Thời gian phải để ở định dạng số");
return false;
}
//Nhập giá tiền muốn mua
var vip_price= document.forms["myForm"]["vip_price"].value;
var checkvip_price= isNaN(vip_price);
if ((vip_price== "") || (vip_price<= 50000 )) {
alert("Số tiền không được để trống và phải lớn hơn 50000 VNĐ");
return false;
}
if (vip_price> 999999999999999) {
alert("Số tiền không được lớn hơn 999999999999999 VNĐ");
return false;
}
if (checkvip_price== true) {
alert("Số tiền  phải để ở định dạng số");
return false;
}
//Email phải được điền chính xác
var email=document.forms["myForm"]["email"].value;
var aCong=email.indexOf("@");
var dauCham = email.lastIndexOf(".com");
if (email == "") {
alert("Email không được để trống");
return false;
}
else if ((aCong<1) || (dauCham<aCong+2) || (dauCham+2>email.length)) {
alert("Email bạn điền không chính xác");
return false;
}

}
</script> 

@endsection 