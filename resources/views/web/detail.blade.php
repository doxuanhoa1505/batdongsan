@extends('web.layout.master')
@section('content')
<div>

    <div class="home10-mainslider">
        <div class="container-fluid p0">
            <div class="row">
                <div class="col-lg-12">
                    <div class="main-banner-wrapper home10">
                        <div class="banner-style-one owl-theme owl-carousel">
                            @foreach($detail as $key=>$item)
                            <div class="slide slide-one"
                                style="background-image: url(images/home/{{$item->info_image}});height: 600px;">
                            </div>
                            @endforeach

                        </div>
                        <div class="carousel-btn-block banner-carousel-btn">
                            <span class="carousel-btn left-btn"><i class="flaticon-left-arrow-1 left"></i></span>
                            <span class="carousel-btn right-btn"><i class="flaticon-right-arrow right"></i></span>
                        </div><!-- /.carousel-btn-block banner-carousel-btn -->
                    </div><!-- /.main-banner-wrapper -->
                </div>
            </div>
        </div>
    </div>
    @foreach($detail as $key=>$item)
    <section class="p0">
        <div class="container">
            <div class="row listing_single_row style2">
                <div class="col-lg-7 col-xl-8">
                    <div class="single_property_title multicolor_style mt30-767">
                        <h2>{{$item->inf_name}}</h2>
                        <p>{{$item->inf_address}}</p>
                    </div>
                    <div class="dn db-991">
                        <div id="main2">
                            <span id="open2" class="flaticon-filter-results-button filter_open_btn style3"> Show
                                Filter</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-5 col-xl-4">
                    <div class="single_property_social_share multicolor_style text-right tal-767">
                        <div class="price">
                            <h2>{{ $item->inf_price }}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endforeach


    <section class="sticky_heading h60 p0">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">
                    <div class="sticky-nav-tabs">
                        <ul class="sticky-nav-tabs-container">
                            <li class="list-inline-item nav-item active"><a class="sticky-nav-tab" href="#tab-1">Chi
                                    tiết</a></li>
                            <li class="list-inline-item nav-item"><a class="sticky-nav-tab" href="#tab-2">Đặc trưng</a>
                            </li>
                            <li class="list-inline-item nav-item"><a class="sticky-nav-tab" href="#tab-3">Địa điểm</a>
                            </li>
                            <li class="list-inline-item nav-item"><a class="sticky-nav-tab" href="#tab-6">Nhận xét</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="our-listing-single bgc-f7 pb30-991">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="listing_sidebar dn db-991">
                        <div class="sidebar_content_details style3">
                            <!-- <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a> -->
                            <div class="sidebar_listing_list style2 mobile_sytle_sidebar mb0">
                                <div class="sidebar_advanced_search_widget">
                                    <h4 class="mb25">Tìm kiếm <a class="filter_closed_btn float-right"
                                            href="#"><small>Hide Filter</small> <span class="flaticon-close"></span></a>
                                    </h4>
                                    <ul class="sasw_list style2 mb0">
                                        <li class="search_area">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="exampleInputName1"
                                                    placeholder="keyword">
                                                <label for="exampleInputEmail"><span
                                                        class="flaticon-magnifying-glass"></span></label>
                                            </div>
                                        </li>
                                        <li class="search_area">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="exampleInputEmail"
                                                    placeholder="Location">
                                                <label for="exampleInputEmail"><span
                                                        class="flaticon-maps-and-flags"></span></label>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Status</option>
                                                        <option>Apartment</option>
                                                        <option>Bungalow</option>
                                                        <option>Condo</option>
                                                        <option>House</option>
                                                        <option>Land</option>
                                                        <option>Single Family</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Property Type</option>
                                                        <option>Apartment</option>
                                                        <option>Bungalow</option>
                                                        <option>Condo</option>
                                                        <option>House</option>
                                                        <option>Land</option>
                                                        <option>Single Family</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="small_dropdown2">
                                                <div id="prncgs" class="btn dd_btn">
                                                    <span>Price</span>
                                                    <label for="exampleInputEmail2"><span
                                                            class="fa fa-angle-down"></span></label>
                                                </div>
                                                <div class="dd_content2">
                                                    <div class="pricing_acontent">
                                                        <input type="text" class="amount" placeholder="$52,239">
                                                        <input type="text" class="amount2" placeholder="$985,14">
                                                        <div class="slider-range"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Bathrooms</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                        <option>6</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Bedrooms</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                        <option>3</option>
                                                        <option>4</option>
                                                        <option>5</option>
                                                        <option>6</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Garages</option>
                                                        <option>Yes</option>
                                                        <option>No</option>
                                                        <option>Others</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_two">
                                                <div class="candidate_revew_select">
                                                    <select class="selectpicker w100 show-tick">
                                                        <option>Year built</option>
                                                        <option>2013</option>
                                                        <option>2014</option>
                                                        <option>2015</option>
                                                        <option>2016</option>
                                                        <option>2017</option>
                                                        <option>2018</option>
                                                        <option>2019</option>
                                                        <option>2020</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </li>
                                        <li class="min_area style2 list-inline-item">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="exampleInputName2"
                                                    placeholder="Min Area">
                                            </div>
                                        </li>
                                        <li class="max_area list-inline-item">
                                            <div class="form-group">
                                                <input type="text" class="form-control" id="exampleInputName3"
                                                    placeholder="Max Area">
                                            </div>
                                        </li>
                                        <li>
                                            <div id="accordion" class="panel-group">
                                                <div class="panel">
                                                    <div class="panel-heading">
                                                        <h4 class="panel-title">
                                                            <a href="#panelBodyRating" class="accordion-toggle link"
                                                                data-toggle="collapse" data-parent="#accordion"><i
                                                                    class="flaticon-more"></i> Advanced features</a>
                                                        </h4>
                                                    </div>
                                                    <div id="panelBodyRating" class="panel-collapse collapse">
                                                        <div class="panel-body row">
                                                            <div class="col-lg-12">
                                                                <ul
                                                                    class="ui_kit_checkbox selectable-list float-left fn-400">
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                class="custom-control-input"
                                                                                id="customCheck1">
                                                                            <label class="custom-control-label"
                                                                                for="customCheck1">Air
                                                                                Conditioning</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                class="custom-control-input"
                                                                                id="customCheck4">
                                                                            <label class="custom-control-label"
                                                                                for="customCheck4">Barbeque</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                class="custom-control-input"
                                                                                id="customCheck10">
                                                                            <label class="custom-control-label"
                                                                                for="customCheck10">Gym</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                class="custom-control-input"
                                                                                id="customCheck5">
                                                                            <label class="custom-control-label"
                                                                                for="customCheck5">Microwave</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                class="custom-control-input"
                                                                                id="customCheck6">
                                                                            <label class="custom-control-label"
                                                                                for="customCheck6">TV Cable</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                class="custom-control-input"
                                                                                id="customCheck2">
                                                                            <label class="custom-control-label"
                                                                                for="customCheck2">Lawn</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                class="custom-control-input"
                                                                                id="customCheck11">
                                                                            <label class="custom-control-label"
                                                                                for="customCheck11">Refrigerator</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                class="custom-control-input"
                                                                                id="customCheck3">
                                                                            <label class="custom-control-label"
                                                                                for="customCheck3">Swimming Pool</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                                <ul
                                                                    class="ui_kit_checkbox selectable-list float-right fn-400">
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                class="custom-control-input"
                                                                                id="customCheck12">
                                                                            <label class="custom-control-label"
                                                                                for="customCheck12">WiFi</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                class="custom-control-input"
                                                                                id="customCheck14">
                                                                            <label class="custom-control-label"
                                                                                for="customCheck14">Sauna</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                class="custom-control-input"
                                                                                id="customCheck7">
                                                                            <label class="custom-control-label"
                                                                                for="customCheck7">Dryer</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                class="custom-control-input"
                                                                                id="customCheck9">
                                                                            <label class="custom-control-label"
                                                                                for="customCheck9">Washer</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                class="custom-control-input"
                                                                                id="customCheck13">
                                                                            <label class="custom-control-label"
                                                                                for="customCheck13">Laundry</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                class="custom-control-input"
                                                                                id="customCheck8">
                                                                            <label class="custom-control-label"
                                                                                for="customCheck8">Outdoor
                                                                                Shower</label>
                                                                        </div>
                                                                    </li>
                                                                    <li>
                                                                        <div class="custom-control custom-checkbox">
                                                                            <input type="checkbox"
                                                                                class="custom-control-input"
                                                                                id="customCheck15">
                                                                            <label class="custom-control-label"
                                                                                for="customCheck15">Window
                                                                                Coverings</label>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="search_option_button">
                                                <button type="submit" class="btn btn-block btn-thm">Search</button>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="listing_single_property_compare">
                        <ul class="mb0">
                            <li class="list-inline-item">
                                <div class="icon"><span class="flaticon-transfer-1"></span></div>
                                <p>Add to compare</p>
                            </li>
                            <li class="list-inline-item">
                                <div class="icon"><span class="flaticon-envelope"></span></div>
                                <p>Send an email</p>
                            </li>
                            <li class="list-inline-item">
                                <div class="icon"><span class="flaticon-heart"></span></div>
                                <p>Save</p>
                            </li>
                            <li class="list-inline-item">
                                <div class="icon"><span class="flaticon-share"></span></div>
                                <p>Share</p>
                            </li>
                            <li class="list-inline-item">
                                <div class="icon"><span class="flaticon-printer"></span></div>
                                <p>Print</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 col-lg-8 mt35">
                    <div class="row">
                        <div id="tab-1" class="col-lg-12">
                            <div class="listing_single_description">
                                <div class="lsd_list">
                                    <ul class="mb0">
                                        <li class="list-inline-item"><a href="#">{{$item->type_name}}</a></li>
                                        <li class="list-inline-item"><a href="#">Phòng: {{$item->inf_bedrooms}}</a></li>
                                        <li class="list-inline-item"><a href="#">Phòng tắm: {{$item->inf_bathrooms}}</a>
                                        </li>
                                        <li class="list-inline-item"><a href="#">Kích thước:
                                                {{$item->inf_garage_size}}</a></li>
                                    </ul>
                                </div>
                                <h4 class="mb30">Thông tin</h4>
                                <p class="mb25">{{ $item->inf_des }}</p>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="additional_details">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4 class="mb15">Chi tiết</h4>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xl-4">
                                        <ul class="list-inline-item">
                                            <li>
                                                <p>ID :</p></a>
                                            </li>
                                            <li>
                                                <p>Giá :</p></a>
                                            </li>
                                            <li>
                                                <p>Diện tích :</p></a>
                                            </li>
                                            <li>
                                                <p>Năm xây dựng :</p></a>
                                            </li>
                                        </ul>
                                        <ul class="list-inline-item">
                                            <li>
                                                <p><span>{{$item->inf_id}}</span></p></a>
                                            </li>
                                            <li>
                                                <p><span>{{$item->inf_price}}</span></p></a>
                                            </li>
                                            <li>
                                                <p><span>1560 Sq Ft</span></p></a>
                                            </li>
                                            <li>
                                                <p><span>{{$item->inf_year_built}}</span></p></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xl-4">
                                        <ul class="list-inline-item">
                                            <li>
                                                <p>Phòng :</p>
                                            </li>
                                            <li>
                                                <p>Phòng tắm :</p>
                                            </li>
                                            <li>
                                                <p>nhà để xe :</p>
                                            </li>
                                            <li>
                                                <p>Kích thước nhà để xe :</p>
                                            </li>
                                        </ul>
                                        <ul class="list-inline-item">
                                            <li>
                                                <p><span>{{$item->inf_bedrooms}}</span></p>
                                            </li>
                                            <li>
                                                <p><span>{{$item->inf_bathrooms}}</span></p>
                                            </li>
                                            <li>
                                                <p><span>{{$item->inf_garage}}</span></p>
                                            </li>
                                            <li>
                                                <p><span>{{$item->inf_garage_size}}</span></p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xl-4">
                                        <ul class="list-inline-item">
                                            <li>
                                                <p>Loại tài sản :</p>
                                            </li>
                                            <li>
                                                <p>Trạng thái :</p>
                                            </li>
                                        </ul>
                                        <ul class="list-inline-item">
                                            <li>
                                                <p><span>{{$item->type_name}}</span></p>
                                            </li>
                                            <li>
                                                <p><span>{{$item->inf_to_own}}</span></p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-2" class="col-lg-12">
                            <div class="application_statics mt30">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4 class="mb10">Thiết bị</h4>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xl-4">
                                        <ul class="list-inline-item">
                                            <li>
                                                <p>Máy lạnh:</p>
                                            </li>
                                            <li>
                                                <p>Đồ nướng:</p>
                                            </li>
                                            <li>
                                                <p>Máy sấy khô:</p>
                                            </li>
                                            <li>
                                                <p>Phòng thể dục:</p>
                                            </li>
                                            <li>
                                                <p>Giặt ủi:</p>
                                            </li>
                                        </ul>
                                        <ul class="list-inline-item">
                                            <li>
                                                <p>{{ $item->inf_air_conditioning }}</p>
                                            </li>
                                            <li>
                                                <p>{{ $item->inf_barbeque }}</p>
                                            </li>
                                            <li>
                                                <p>{{ $item->inf_dryer }}</p>
                                            </li>
                                            <li>
                                                <p>{{ $item->inf_gym}}</p>
                                            </li>
                                            <li>
                                                <p>{{ $item->inf_laundry }}</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xl-4">
                                        <ul class="list-inline-item">
                                            <li>
                                                <p>Lò vi sóng:</p>
                                            </li>
                                            <li>
                                                <p>Vòi tắm ngoài trời:</p>
                                            </li>
                                            <li>
                                                <p>Tủ lạnh:</p>
                                            </li>
                                            <li>
                                                <p>Phòng tăm hơi:</p>
                                            </li>
                                            <li>
                                                <p>Hồ bơi:</p>
                                            </li>
                                        </ul>
                                        <ul class="list-inline-item">
                                            <li>
                                                <p>{{ $item->inf_microwave }}</p>
                                            </li>
                                            <li>
                                                <p>{{ $item->inf_outdoor_shower }}</p>
                                            </li>
                                            <li>
                                                <p>{{ $item->inf_refrigerator }}</p>
                                            </li>
                                            <li>
                                                <p>{{ $item->inf_sauna}}</p>
                                            </li>
                                            <li>
                                                <p>{{ $item->inf_swimming_pool }}</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-xl-4">
                                        <ul class="list-inline-item">
                                            <li>
                                                <p>Cáp TV:</p>
                                            </li>
                                            <li>
                                                <p>Máy giặt:</p>
                                            </li>
                                            <li>
                                                <p>Wifi:</p>
                                            </li>
                                            <li>
                                                <p>Tấm phủ cửa sổ:</p>
                                            </li>
                                        </ul>
                                        <ul class="list-inline-item">
                                            <li>
                                                <p>{{ $item->inf_tv }}</p>
                                            </li>
                                            <li>
                                                <p>{{ $item->inf_washer }}</p>
                                            </li>
                                            <li>
                                                <p>{{ $item->inf_wifi }}</p>
                                            </li>
                                            <li>
                                                <p>{{ $item->inf_window}}</p>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div id="tab-3" class="application_statics mt30">
                                <h4 class="mb30">Địa chỉ <small class="float-right">{{ $item->inf_address }}</small>
                                </h4>
                                <div class="property_video p0">
                                    <div class="thumb">
                                        <div class="h400" id="map-canvas"></div>
                                        <div class="overlay_icon">
                                            <a href="#"><img class="map_img_icon" src="images/header-logo.png"
                                                    alt="header-logo.png"></a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12">
                            <div class="product_single_content">
                                <div class="mbp_pagination_comments mt30">
                                    <div class="mbp_comment_form style2">
                                        <div class="fb-comments"
                                            data-href="{{route('chitiet',[$item->inf_id,$item->inf_name])}}"
                                            data-numposts="5" data-width="100%"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <h4 class="mt30 mb30">Tương tự</h4>
                        </div>
                        @foreach($information as $key=>$item)
                        <div class="col-lg-6">
                            <div class="feat_property">
                                <div class="thumb">
                                    <img class="img-whp" src="uploads/information/{{ $item->inf_img }}"
                                        alt="{{ $item->inf_img }}">
                                    <div class="thmb_cntnt">
                                        <ul class="tag mb0">
                                            <li class="list-inline-item"><span>{{ $item->inf_to_own }}</span></li>
                                        </ul>
                                        <ul class="icon mb0">
                                            <li class="list-inline-item"><a href="#"><span
                                                        class="flaticon-heart"></span></a></li>
                                        </ul>
                                        <a class="fp_price" href="#">{{ $item->inf_price }}</a>
                                    </div>
                                </div>
                                <a href="{{route('chitiet',[$item->inf_id,$item->inf_name])}}">
                                    <div class="details">
                                        <div class="tc_content">
                                            <p class="text-thm">{{ $item->type_name }}</p>
                                            <h4>{{ $item->inf_name }}</h4>
                                            <p><span class="flaticon-placeholder"></span> {{ $item->inf_address }}</p>
                                            <ul class="prop_details mb0">
                                                <li class="list-inline-item"><a href="#">Phòng:
                                                        {{ $item->inf_bedrooms }}</a></li>
                                                <li class="list-inline-item"><a href="#">Nhà tắm:
                                                        {{ $item->inf_bathrooms}}</a></li>
                                                <li class="list-inline-item"><a href="#">Diện tích:
                                                        {{ $item->inf_property_size }}</a></li>
                                            </ul>
                                        </div>
                                        <div class="fp_footer">
                                            <ul class="fp_meta float-left mb0">
                                                <li class="list-inline-item"><a href=""><img
                                                            src="uploads/user/{{ $item->user_img }}"
                                                            alt="{{ $item->user_img }}"
                                                            style="width:25px; border-radius:10px;"></a></li>
                                                <li class="list-inline-item"><a
                                                        href="{{ route('user',[$item->user_id,$item->user_name])}}">{{ $item->user_name}}</a>
                                                </li>
                                            </ul>
                                            <div class="fp_pdate float-right">{{ $item->inf_year_built}}</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
                <div class="col-lg-4 col-xl-4 mt35">
                    <div class="sidebar_listing_list">
                        <div class="sidebar_advanced_search_widget">
                            <div class="sl_creator">
                                <h4 class="mb25">Người đăng</h4>
                                <div class="media">
                                    <a href="{{ route('user',[$item->user_id,$item->user_name])}}"><img class="mr-3"
                                            src="uploads/user/{{ $item->user_img }}" alt="{{ $item->user_img }}"
                                            style="width:75px; border-radius:10px;"></a>
                                    <a href="{{ route('user',[$item->user_id,$item->user_name])}}">
                                        <div class="media-body">
                                            <h5 class="mt-0 mb0">{{ $item->user_name }}</h5>
                                            <p class="mb0">{{ $item->user_phone }}</p>
                                            <p class="mb0"><a href="#" class="__cf_email__"
                                                    data-cfemail="7910171f16391f10171d11160c0a1c571a1614">{{ $item->user_email }}</a>
                                            </p>
                                            <a class="text-thm"
                                                href="{{ route('user',[$item->user_id,$item->user_name])}}">Danh
                                                sách</a>
                                        </div>
                                    </a>
                                </div>
                            </div>
                            <ul class="sasw_list mb0">
                                <li class="search_area">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="exampleInputName1"
                                            placeholder="Your Name">
                                    </div>
                                </li>
                                <li class="search_area">
                                    <div class="form-group">
                                        <input type="number" class="form-control" id="exampleInputName2"
                                            placeholder="Phone">
                                    </div>
                                </li>
                                <li class="search_area">
                                    <div class="form-group">
                                        <input type="email" class="form-control" id="exampleInputEmail"
                                            placeholder="Email">
                                    </div>
                                </li>
                                <li class="search_area">
                                    <div class="form-group">
                                        <textarea id="form_message" name="form_message" class="form-control required"
                                            rows="5" required="required"
                                            placeholder="I'm interest in [ Listing Single ]"></textarea>
                                    </div>
                                </li>
                                <li>
                                    <div class="search_option_button">
                                        <button type="submit" class="btn btn-block btn-thm">Search</button>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="sidebar_listing_list">
                        <div class="sidebar_advanced_search_widget">
                            <ul class="sasw_list mb0">
                                <li class="search_area">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="exampleInputName1"
                                            placeholder="keyword">
                                        <label for="exampleInputEmail"><span
                                                class="flaticon-magnifying-glass"></span></label>
                                    </div>
                                </li>
                                <li class="search_area">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="exampleInputEmail"
                                            placeholder="Location">
                                        <label for="exampleInputEmail"><span
                                                class="flaticon-maps-and-flags"></span></label>
                                    </div>
                                </li>
                                <li>
                                    <div class="search_option_two">
                                        <div class="candidate_revew_select">
                                            <select class="selectpicker w100 show-tick">
                                                <option>Status</option>
                                                <option>Apartment</option>
                                                <option>Bungalow</option>
                                                <option>Condo</option>
                                                <option>House</option>
                                                <option>Land</option>
                                                <option>Single Family</option>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="search_option_two">
                                        <div class="candidate_revew_select">
                                            <select class="selectpicker w100 show-tick">
                                                <option>Property Type</option>
                                                <option>Apartment</option>
                                                <option>Bungalow</option>
                                                <option>Condo</option>
                                                <option>House</option>
                                                <option>Land</option>
                                                <option>Single Family</option>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="small_dropdown2">
                                        <div id="prncgs2" class="btn dd_btn">
                                            <span>Price</span>
                                            <label for="exampleInputEmail2"><span
                                                    class="fa fa-angle-down"></span></label>
                                        </div>
                                        <div class="dd_content2">
                                            <div class="pricing_acontent">
                                                <input type="text" class="amount" placeholder="$52,239">
                                                <input type="text" class="amount2" placeholder="$985,14">
                                                <div class="slider-range"></div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="search_option_two">
                                        <div class="candidate_revew_select">
                                            <select class="selectpicker w100 show-tick">
                                                <option>Bathrooms</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                <option>6</option>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="search_option_two">
                                        <div class="candidate_revew_select">
                                            <select class="selectpicker w100 show-tick">
                                                <option>Bedrooms</option>
                                                <option>1</option>
                                                <option>2</option>
                                                <option>3</option>
                                                <option>4</option>
                                                <option>5</option>
                                                <option>6</option>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="search_option_two">
                                        <div class="candidate_revew_select">
                                            <select class="selectpicker w100 show-tick">
                                                <option>Garages</option>
                                                <option>Yes</option>
                                                <option>No</option>
                                                <option>Others</option>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="search_option_two">
                                        <div class="candidate_revew_select">
                                            <select class="selectpicker w100 show-tick">
                                                <option>Year built</option>
                                                <option>2013</option>
                                                <option>2014</option>
                                                <option>2015</option>
                                                <option>2016</option>
                                                <option>2017</option>
                                                <option>2018</option>
                                                <option>2019</option>
                                                <option>2020</option>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li class="min_area list-inline-item">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="exampleInputName2"
                                            placeholder="Min Area">
                                    </div>
                                </li>
                                <li class="max_area list-inline-item">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="exampleInputName3"
                                            placeholder="Max Area">
                                    </div>
                                </li>
                                <li>
                                    <div id="accordion" class="panel-group">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a href="#panelBodyRating" class="accordion-toggle link"
                                                        data-toggle="collapse" data-parent="#accordion"><i
                                                            class="flaticon-more"></i> Advanced features</a>
                                                </h4>
                                            </div>
                                            <div id="panelBodyRating" class="panel-collapse collapse">
                                                <div class="panel-body row">
                                                    <div class="col-lg-12">
                                                        <ul class="ui_kit_checkbox selectable-list float-left fn-400">
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="customCheck16">
                                                                    <label class="custom-control-label"
                                                                        for="customCheck16">Air Conditioning</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="customCheck17">
                                                                    <label class="custom-control-label"
                                                                        for="customCheck17">Barbeque</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="customCheck18">
                                                                    <label class="custom-control-label"
                                                                        for="customCheck18">Gym</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="customCheck19">
                                                                    <label class="custom-control-label"
                                                                        for="customCheck19">Microwave</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="customCheck20">
                                                                    <label class="custom-control-label"
                                                                        for="customCheck20">TV Cable</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="customCheck21">
                                                                    <label class="custom-control-label"
                                                                        for="customCheck21">Lawn</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="customCheck22">
                                                                    <label class="custom-control-label"
                                                                        for="customCheck22">Refrigerator</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="customCheck23">
                                                                    <label class="custom-control-label"
                                                                        for="customCheck23">Swimming Pool</label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                        <ul class="ui_kit_checkbox selectable-list float-right fn-400">
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="customCheck24">
                                                                    <label class="custom-control-label"
                                                                        for="customCheck24">WiFi</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="customCheck25">
                                                                    <label class="custom-control-label"
                                                                        for="customCheck25">Sauna</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="customCheck26">
                                                                    <label class="custom-control-label"
                                                                        for="customCheck26">Dryer</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="customCheck27">
                                                                    <label class="custom-control-label"
                                                                        for="customCheck27">Washer</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="customCheck28">
                                                                    <label class="custom-control-label"
                                                                        for="customCheck28">Laundry</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="customCheck29">
                                                                    <label class="custom-control-label"
                                                                        for="customCheck29">Outdoor Shower</label>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="custom-control custom-checkbox">
                                                                    <input type="checkbox" class="custom-control-input"
                                                                        id="customCheck30">
                                                                    <label class="custom-control-label"
                                                                        for="customCheck30">Window Coverings</label>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="search_option_button">
                                        <button type="submit" class="btn btn-block btn-thm">Search</button>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <!-- <div class="terms_condition_widget">
                        <h4 class="title">Featured Properties</h4>
                        <div class="sidebar_feature_property_slider">
                            <div class="item">
                                <div class="feat_property home7 agent">
                                    <div class="thumb">
                                        <img class="img-whp" src="images/property/fp1.jpg" alt="fp1.jpg">
                                        <div class="thmb_cntnt">
                                            <ul class="tag mb0">
                                                <li class="list-inline-item"><a href="#">For Rent</a></li>
                                                <li class="list-inline-item"><a href="#">Featured</a></li>
                                            </ul>
                                            <a class="fp_price" href="#">$13,000<small>/mo</small></a>
                                            <h4 class="posr color-white">Renovated Apartment</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="feat_property home7 agent">
                                    <div class="thumb">
                                        <img class="img-whp" src="images/property/fp2.jpg" alt="fp2.jpg">
                                        <div class="thmb_cntnt">
                                            <ul class="tag mb0">
                                                <li class="list-inline-item"><a href="#">For Rent</a></li>
                                                <li class="list-inline-item"><a href="#">Featured</a></li>
                                            </ul>
                                            <a class="fp_price" href="#">$13,000<small>/mo</small></a>
                                            <h4 class="posr color-white">Renovated Apartment</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="feat_property home7 agent">
                                    <div class="thumb">
                                        <img class="img-whp" src="images/property/fp3.jpg" alt="fp3.jpg">
                                        <div class="thmb_cntnt">
                                            <ul class="tag mb0">
                                                <li class="list-inline-item"><a href="#">For Rent</a></li>
                                                <li class="list-inline-item"><a href="#">Featured</a></li>
                                            </ul>
                                            <a class="fp_price" href="#">$13,000<small>/mo</small></a>
                                            <h4 class="posr color-white">Renovated Apartment</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="feat_property home7 agent">
                                    <div class="thumb">
                                        <img class="img-whp" src="images/property/fp4.jpg" alt="fp4.jpg">
                                        <div class="thmb_cntnt">
                                            <ul class="tag mb0">
                                                <li class="list-inline-item"><a href="#">For Rent</a></li>
                                                <li class="list-inline-item"><a href="#">Featured</a></li>
                                            </ul>
                                            <a class="fp_price" href="#">$13,000<small>/mo</small></a>
                                            <h4 class="posr color-white">Renovated Apartment</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="item">
                                <div class="feat_property home7 agent">
                                    <div class="thumb">
                                        <img class="img-whp" src="images/property/fp5.jpg" alt="fp5.jpg">
                                        <div class="thmb_cntnt">
                                            <ul class="tag mb0">
                                                <li class="list-inline-item"><a href="#">For Rent</a></li>
                                                <li class="list-inline-item"><a href="#">Featured</a></li>
                                            </ul>
                                            <a class="fp_price" href="#">$13,000<small>/mo</small></a>
                                            <h4 class="posr color-white">Renovated Apartment</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div class="terms_condition_widget">
                        <h4 class="title">Categories Property</h4>
                        <div class="widget_list">
                            <ul class="list_details">
                                <li><a href="#"><i class="fa fa-caret-right mr10"></i>Apartment <span
                                            class="float-right">6 properties</span></a></li>
                                <li><a href="#"><i class="fa fa-caret-right mr10"></i>Condo <span class="float-right">12
                                            properties</span></a></li>
                                <li><a href="#"><i class="fa fa-caret-right mr10"></i>Family House <span
                                            class="float-right">8 properties</span></a></li>
                                <li><a href="#"><i class="fa fa-caret-right mr10"></i>Modern Villa <span
                                            class="float-right">26 properties</span></a></li>
                                <li><a href="#"><i class="fa fa-caret-right mr10"></i>Town House <span
                                            class="float-right">89 properties</span></a></li>
                            </ul>
                        </div>
                    </div>
                    <!-- <div class="sidebar_feature_listing">
                        <h4 class="title">Recently Viewed</h4>
                        <div class="media">
                            <img class="align-self-start mr-3" src="images/blog/fls1.jpg" alt="fls1.jpg">
                            <div class="media-body">
                                <h5 class="mt-0 post_title">Nice Room With View</h5>
                                <a href="#">$13,000/<small>/mo</small></a>
                                <ul class="mb0">
                                    <li class="list-inline-item">Beds: 4</li>
                                    <li class="list-inline-item">Baths: 2</li>
                                    <li class="list-inline-item">Sq Ft: 5280</li>
                                </ul>
                            </div>
                        </div>
                        <div class="media">
                            <img class="align-self-start mr-3" src="images/blog/fls2.jpg" alt="fls2.jpg">
                            <div class="media-body">
                                <h5 class="mt-0 post_title">Villa called Archangel</h5>
                                <a href="#">$13,000<small>/mo</small></a>
                                <ul class="mb0">
                                    <li class="list-inline-item">Beds: 4</li>
                                    <li class="list-inline-item">Baths: 2</li>
                                    <li class="list-inline-item">Sq Ft: 5280</li>
                                </ul>
                            </div>
                        </div>
                        <div class="media">
                            <img class="align-self-start mr-3" src="images/blog/fls3.jpg" alt="fls3.jpg">
                            <div class="media-body">
                                <h5 class="mt-0 post_title">Sunset Studio</h5>
                                <a href="#">$13,000<small>/mo</small></a>
                                <ul class="mb0">
                                    <li class="list-inline-item">Beds: 4</li>
                                    <li class="list-inline-item">Baths: 2</li>
                                    <li class="list-inline-item">Sq Ft: 5280</li>
                                </ul>
                            </div>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
    </section>

</div>
@endsection