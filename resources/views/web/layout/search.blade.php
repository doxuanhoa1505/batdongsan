<!-- 10th Home Slider -->
<div class="home10-mainslider">
    <div class="container-fluid p0">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-banner-wrapper home10">
                    <div class="banner-style-one owl-theme owl-carousel">
                        <div class="slide slide-one" style="background-image: url(images/home/7.jpg);height: 620px;">
                        </div>
                        <div class="slide slide-one" style="background-image: url(images/home/2.jpg);height: 620px;">
                        </div>
                        <div class="slide slide-one" style="background-image: url(images/home/1.jpg);height: 620px;">
                        </div>
                    </div>
                    <div class="carousel-btn-block banner-carousel-btn">
                        <span class="carousel-btn left-btn"><i class="flaticon-left-arrow-1 left"></i></span>
                        <span class="carousel-btn right-btn"><i class="flaticon-right-arrow right"></i></span>
                    </div><!-- /.carousel-btn-block banner-carousel-btn -->
                </div><!-- /.main-banner-wrapper -->
            </div>
        </div>
    </div>
</div>