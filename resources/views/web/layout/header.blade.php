	<!-- Main Header Nav -->
	<header class="header-nav menu_style_home_one style2 style3 navbar-scrolltofixed stricky main-menu">
	    <div class="container-fluid p0">
	        <!-- Ace Responsive Menu -->
	        <nav>
	            <!-- Menu Toggle btn-->
	            <div class="menu-toggle">
	                <img class="nav_logo_img img-fluid" src="images/header-logo.png" alt="header-logo.png">
	                <button type="button" id="menu-btn">
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                    <span class="icon-bar"></span>
	                </button>
	            </div>
	            <a href="{{route('home')}}" class="navbar_brand float-left dn-smd">
	                <img class="logo1 img-fluid" src="images/header-logo2.png" alt="header-logo.png">
	                <img class="logo2 img-fluid" src="images/header-logo2.png" alt="header-logo2.png">
	                <span>Bất động sản</span>
	            </a>
	            <!-- Responsive Menu Structure-->
	            <!--Note: declare the Menu style in the data-menu-style="horizontal" (options: horizontal, vertical, accordion) -->
	        
	            <ul id="respMenu" class="ace-responsive-menu text-right" data-menu-style="horizontal">
	                <li>
	                    <a href="{{route('home')}}"><span class="title">Trang chủ</span></a>

	                </li>
	                <li>
	                    <a href="{{route('allcategory')}}"><span class="title">Nhà đắt</span></a>
	                    <ul>
	                        @foreach($typeasset as $key=>$item)
	                        <li>
	                            <a href="{{route('category',[$item->type_id])}}">{{ $item->type_name }}</a>
	                        </li>
	                        @endforeach
	                    </ul>
	                </li>
	                <li>
	                    <a href="{{route('blog')}}"><span class="title">Tin tức</span></a>
	                    <ul>
	                        @foreach($type_blog as $key=>$item)
	                        <li>
	                            <a href="{{route('blogtype',[$item->type_blog_id])}}">{{ $item->type_blog_name }}</a>
	                        </li>
	                        @endforeach
	                    </ul>
	                </li>
	                <li>
	                    <a href="{{route('alluser')}}"><span class="title">Đại lý</span></a>

	                </li>
	                @if(Session::get('user_id'))
	                <li><a href=""><?php $name = Session::get('user_name');
					echo $name;?></a>
	                    <ul>
	                        <li><a href="{{route('logout')}}" class="btn">Đăng xuất</a></li>
	                    </ul>
	                </li>

	                @else
	                <li class="list-inline-item "><a href="{{route('login')}}" class="btn">Đăng nhập</a></li>
	                @endif


	            </ul>

	        </nav>
	    </div>
	</header>



	<!-- Main Header Nav For Mobile -->
	<div id="page" class="stylehome1 h0">
	    <div class="mobile-menu">
	        <div class="header stylehome1">
	            <div class="main_logo_home2 text-center">
	                <img class="nav_logo_img img-fluid mt20" src="images/header-logo2.png" alt="header-logo2.png">
	                <span class="mt20">FindHouse</span>
	            </div>
	            <ul class="menu_bar_home2">
	                <li class="list-inline-item list_s"><a href="page-register.html"><span
	                            class="flaticon-user"></span></a></li>
	                <li class="list-inline-item">
	                    <div class="search_overlay">
	                        <div id="search-button-listener2" class="mk-search-trigger style2 mk-fullscreen-trigger">
	                            <div id="search-button2"><i class="flaticon-magnifying-glass"></i></div>
	                        </div>
	                        <div class="mk-fullscreen-search-overlay" id="mk-search-overlay2">
	                            <button class="mk-fullscreen-close" id="mk-fullscreen-close-button2"><i
	                                    class="fa fa-times"></i></button>
	                            <div id="mk-fullscreen-search-wrapper2">
	                                <form method="get" id="mk-fullscreen-searchform2">
	                                    <input type="text" value="" placeholder="Search courses..."
	                                        id="mk-fullscreen-search-input2">
	                                    <i class="flaticon-magnifying-glass fullscreen-search-icon"><input value=""
	                                            type="submit"></i>
	                                </form>
	                            </div>
	                        </div>
	                    </div>
	                </li>
	                <li class="list-inline-item"><a href="#menu"><span></span></a></li>
	            </ul>
	        </div>
	    </div><!-- /.mobile-menu -->
	    <nav id="menu" class="stylehome1">
	        <ul>
	            <li>
	                <a href="{{route('home')}}">Trang chủ</a>
	            </li>
	            <li><a href="#"><span>Nhà đất</span></a>
	                <ul>
	                    @foreach($typeasset as $key=>$item)
	                    <li>
	                        <a href="{{route('category',$item->type_id)}}">{{ $item->type_name }}</a>
	                    </li>
	                    @endforeach
	                </ul>
	            </li>
	            <li><a href="{{route('blog')}}"><span>Tin tức</span></a>
	                <ul>
	                    @foreach($type_blog as $key=>$item)
	                    <li>
	                        <a href="">{{ $item->type_blog_name }}</a>
	                    </li>
	                    @endforeach
	                </ul>
	            </li>
	            <li>
	                <a href="{{route('alluser')}}">
	                    <?php
							$name = Session::get('user_name');
							if($name){
								echo $name;
							}
						?>
	                </a>
	            </li>
	            <li><a href="page-login.html"><span class="flaticon-user"></span> Đăng nhập | Đăng ký</a></li>
	        </ul>
	    </nav>
	</div>