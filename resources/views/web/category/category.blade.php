@extends('web.layout.master')
@section('content')
@include('web.layout.search')


<section id="feature-property" class="feature-property bgc-f7 pb30">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-title mb40">
                    <h2>Real estate highlights</h2>
                </div>
            </div>
            @foreach($type_asset as $key=>$item)
            <div class="col-md-6 col-lg-4">
                <div class="feat_property home7">
                    <div class="thumb">
                        <img class="img-whp" src="uploads/information/{{ $item->inf_img }}" alt="{{ $item->inf_img }}"
                            style="height:400px">
                        <div class="thmb_cntnt">
                            <ul class="tag mb0">
                                <li class="list-inline-item"><a href="#">For Rent</a></li>
                                <li class="list-inline-item"><a href="#">Featured</a></li>
                            </ul>
                            <ul class="icon mb0">
                                <!-- <li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a>
                                </li> -->
                                <li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a>
                                </li>
                            </ul>
                            <a class="fp_price" href="chitiet">{{ $item->inf_price }} <small>/m<sup>2</sup></small></a>
                        </div>
                    </div></a>
                    <a href="{{route('chitiet',[$item->inf_id,$item->inf_name])}}">
                        <div class="details">
                            <div class="tc_content">
                                <p class="text-thm">{{ $item->inf_name }}</p>
                                <h4></h4>
                                <p><span class="flaticon-placeholder"></span>{{ $item->inf_address }}</p>
                                <ul class="prop_details mb0">
                                    <li class="list-inline-item">Số giường: {{ $item->inf_bedrooms }}</li>
                                    <li class="list-inline-item">Phòng tắm: {{ $item->inf_bathrooms }}</li>
                                    <li class="list-inline-item">Diện tích: {{ $item->inf_property_size }}</li>
                                </ul>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            @endforeach


        </div>
    </div>
</section>
<!-- Our Blog -->
<section class="our-blog bgc-f7 pb30">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="main-title text-center">
                    <h2>Tin tức nổi bật</h2>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($blog as $item)
            <div class="col-md-6 col-lg-4 col-xl-4">
                <div class="for_blog feat_property">
                    <div class="thumb">
                        <img class="img-whp" src="images/blog/bh1.jpg" alt="bh1.jpg">
                    </div>
                    <div class="details">
                        <div class="tc_content">
                            <p class="text-thm">{{$item->type_blog_name}}</p>
                            <h4>{{$item->blog_name}}</h4>
                        </div>
                        <div class="fp_footer">
                            <ul class="fp_meta float-left mb0">
                                <li class="list-inline-item"><a href="#"><img src="images/property/pposter1.png"
                                            alt="pposter1.png"></a></li>
                                <li class="list-inline-item"><a href="#">{{$item->user_name}}</a></li>
                            </ul>
                            <a class="fp_pdate float-right" href="#">{{$item->create_at}}</a>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>

<!-- Our Agents -->
<section id="our-agents" class="our-agents pt40 pb15">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-title mb30">
                    <h2>Đại lý của chúng tôi</h2>
                    <a class="float-right" href="{{ route('alluser')}}">Xem thêm <span class="flaticon-next"></span></a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="our_agents_home6_slider">
                    @foreach($user as $key=>$item)
                    <div class="item">
                        <div class="our_agent">
                            <div class="thumb">
                                <a href="{{ route('user',[$item->user_id,$item->user_name])}}">
                                    <img class="img-fluid w100" src="uploads/user/{{ $item->user_img}}"
                                        alt="{{ $item->user_img}}">
                                </a>
                                <!-- <div class="overylay">
										<ul class="social_icon">
											<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-pinterest"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-dribbble"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-google"></i></a></li>
										</ul>
									</div> -->
                            </div>
                            <a href="{{ route('user',[$item->user_id,$item->user_name])}}">
                                <div class="details">
                                    <h4>{{ $item->user_name}}</h4>
                                    <!-- <p>Broker <a class="float-right" href="#">4.5 <i
                                            class="fa fa-star color-golden"></i></a></p> -->
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Our Partners -->
<section id="our-partners" class="our-partners">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-lg-3">
                <div class="main-title text-center">
                    <h2>Những cộng sự</h2>
                    <p>Chúng tôi chỉ làm việc với những công ty tốt nhất trên toàn cầu</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-md-4 col-lg">
                <div class="our_partner">
                    <img class="img-fluid" src="images/partners/1.png" alt="1.png">
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg">
                <div class="our_partner">
                    <img class="img-fluid" src="images/partners/2.png" alt="2.png">
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg">
                <div class="our_partner">
                    <img class="img-fluid" src="images/partners/3.png" alt="3.png">
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg">
                <div class="our_partner">
                    <img class="img-fluid" src="images/partners/4.png" alt="4.png">
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg">
                <div class="our_partner">
                    <img class="img-fluid" src="images/partners/5.png" alt="5.png">
                </div>
            </div>
        </div>
    </div>
</section>
@endsection