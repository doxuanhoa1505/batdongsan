@extends('web.layout.master')
@section('content')
<section class="blog_post_container bgc-f7">
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <div class="breadcrumb_content style2">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home')}}">Trang chủ</a></li>
                        <li class="breadcrumb-item active text-thm" aria-current="page">Tin tức</li>
                    </ol>
                    <h2 class="breadcrumb_title">Tin tức mới nhất</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-8">
                <div class="row">
                    @foreach($blog as $key=>$item)
                    <div class="col-lg-6">
                        <div class="for_blog feat_property">
                            <div class="thumb">
                                <img class="img-whp" src="uploads/blog/{{ $item->blog_img }}" alt="{{ $item->blog_img }}">
                                <div class="blog_tag">{{$item->type_blog_name}}</div>
                            </div>
                            <div class="details">
                                <div class="tc_content">
                                    <h4>{{$item->blog_name}}</h4>
                                    <ul class="bpg_meta">
                                        <li class="list-inline-item"><a href="#"><i class="flaticon-calendar"></i></a>
                                        </li>
                                        <li class="list-inline-item"><a href="#">{{ date($item->created) }}</a></li>
                                    </ul>
                                </div>
                                <div class="fp_footer">
                                    <ul class="fp_meta float-left mb0">
                                        <li class="list-inline-item"><a href="#"><img src="uploads/user/{{ $item->user_img }}"
                                                    alt="{{ $item->user_img }}" style="width:30px;border-radius:30%"></a></li>
                                        <li class="list-inline-item"><a href="#">{{ $item->user_name}}</a></li>
                                    </ul>
                                    <a class="fp_pdate float-right text-thm" href="{{ route('detailBlog',[$item->blog_name,$item->blog_id]) }}">Xem thêm <span
                                            class="flaticon-next"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
                {{ $blog->links() }}
                <!-- <div class="row">
						<div class="col-lg-12">
							<div class="mbp_pagination mt20">
								<ul class="page_navigation">
								    <li class="page-item disabled">
								    	<a class="page-link" href="#" tabindex="-1" aria-disabled="true"> <span class="flaticon-left-arrow"></span> Prev</a>
								    </li>
								    <li class="page-item"><a class="page-link" href="#">1</a></li>
								    <li class="page-item active" aria-current="page">
								    	<a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
								    </li>
								    <li class="page-item"><a class="page-link" href="#">3</a></li>
								    <li class="page-item"><a class="page-link" href="#">...</a></li>
								    <li class="page-item"><a class="page-link" href="#">29</a></li>
								    <li class="page-item">
								    	<a class="page-link" href="#"><span class="flaticon-right-arrow"></span></a>
								    </li>
								</ul>
							</div>
						</div>
					</div> -->
            </div>
            <div class="col-lg-4">
                <div class="sidebar_search_widget">
                    <div class="blog_search_widget">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Tìm kiếm"
                                aria-label="Recipient's username" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon2"><span
                                        class="flaticon-magnifying-glass"></span></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="terms_condition_widget">
                    <h4 class="title">Nhà đất</h4>
                    <div class="widget_list">
                        <ul class="list_details">
                            @foreach($typeasset as $key=>$item)
                            <li>
                                <a href="{{route('category',$item->type_id)}}"><i class="fa fa-caret-right mr10"></i>{{ $item->type_name }}<span class="float-right">6
                                        properties</span></a>
                            </li>
                            @endforeach

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Feature Properties -->
<section id="feature-property" class="feature-property mt80 pb50">
    <div class="container-fluid ovh">
        <div class="row">
            <div class="col-lg-12">
                <div class="main-title mb40">
                    <h2>Bất động sản dành cho bạn</h2>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="feature_property_home3_slider">
                    @foreach($information as $key=>$item)
                    <div class="item">
                        <div class="feat_property home3">
                            <div class="thumb">
                                <img class="img-whp" src="uploads/information/{{ $item->inf_img }}"
                                    alt="{{ $item->inf_img }}" style="height:220px">
                                <div class="thmb_cntnt">
                                    <ul class="tag mb0">
                                        <li class="list-inline-item"><a href="#">{{ $item->inf_to_own }}</a></li>
                                    </ul>
                                    <ul class="icon mb0">
                                        <li class="list-inline-item"><a href="#"><span
                                                    class="flaticon-heart"></span></a></li>
                                    </ul>
                                    <a class="fp_price"
                                        href="#">${{ $item->inf_price }}<small>/m<sup>2</sup></small></a>
                                </div>
                            </div>
                            <a href="{{route('chitiet',[$item->inf_id,$item->inf_name])}}">
                                <div class="details">
                                    <div class="tc_content">
                                        <p class="text-thm">{{ $item->inf_name }}</p>
                                        <h4></h4>
                                        <p><span class="flaticon-placeholder"></span>{{ $item->inf_address }}</p>
                                        <ul class="prop_details mb0">
                                            <li class="list-inline-item">Giường: {{ $item->inf_bedrooms }}</li>
                                            <li class="list-inline-item">Diện tích: {{ $item->inf_property_size }}</li>
                                        </ul>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach


                </div>
            </div>
        </div>
    </div>
</section>
@endsection