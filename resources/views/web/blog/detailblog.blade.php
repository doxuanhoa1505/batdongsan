@extends('web.layout.master')
@section('content')
<!-- Blog Single Post -->
<section class="blog_post_container bgc-f7 pb30">
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="main_blog_post_content">
                    <div class="mbp_thumb_post">

                        <div class="blog_sp_tag">
                            @foreach($detail as $key=>$item)
                            <a href="{{route('blogtype',[$item->type_blog_id])}}">{{ $item->type_blog_name }}</a>
                            @endforeach
                        </div>
                        @foreach($detail as $key=>$item)
                        <h3 class="blog_sp_title">{{ $item->blog_name }}</h3>
                        @endforeach

                        <ul class="blog_sp_post_meta">
                            @foreach($detail as $key=>$item)
                            <li class="list-inline-item"><a href="#"><img src="uploads/user/{{ $item->user_img }}"
                                        alt="{{ $item->user_img }}" style="height:25px;border-radius:10px"></a></li>
                            <li class="list-inline-item"><a href="#">{{ $item->user_name }}</a></li>
                            <li class="list-inline-item"><span class="flaticon-calendar"></span></li>
                            <li class="list-inline-item"><a href="#">{{ $item->created_at}}</a></li>
                            <li class="list-inline-item"><span class="flaticon-view"></span></li>
                            <li class="list-inline-item"><a href="#"> 341 views</a></li>
                            <li class="list-inline-item"><span class="flaticon-chat"></span></li>
                            <li class="list-inline-item"><a href="#">15</a></li>
                            @endforeach
                        </ul>
                        <div class="thumb">
                            @foreach($detail as $key=>$item)
                            <img class="img-fluid" src="uploads/blog/{{ $item->blog_img }}" alt="{{ $item->blog_img }}">
                            @endforeach
                        </div>
                        <div class="details">
                            @foreach($detail as $key=>$item)
                            <p class="mb30">{{ $item->blog_des }}</p>
                            @endforeach
                        </div>
                        <ul class="blog_post_share">
                            <li>
                                <p>Share</p>
                            </li>
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            <li><a href="#"><i class="fa fa-google"></i></a></li>
                            <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                    <div class="product_single_content mb30">
                        <div class="fb-comments"
                            data-href="{{ route('detailBlog',[$item->blog_name,$item->blog_id]) }}"
                            data-numposts="10" data-width="100%"></div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-lg-12 mb20">
                        <h4>Related Posts</h4>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="for_blog feat_property">
                            <div class="thumb">
                                <img class="img-whp" src="images/blog/1.jpg" alt="1.jpg">
                                <div class="tag">Construction</div>
                            </div>
                            <div class="details">
                                <div class="tc_content">
                                    <h4>Redfin Ranks the Most Competitive Neighborhoods of 2020</h4>
                                    <ul class="bpg_meta">
                                        <li class="list-inline-item"><a href="#"><i class="flaticon-calendar"></i></a>
                                        </li>
                                        <li class="list-inline-item"><a href="#">January 16, 2020</a></li>
                                    </ul>
                                    <p>Lorem ipsum dolor sit amet, consectetur text link libero tempus congue.</p>
                                </div>
                                <div class="fp_footer">
                                    <ul class="fp_meta float-left mb0">
                                        <li class="list-inline-item"><a href="#"><img src="images/property/pposter1.png"
                                                    alt="pposter1.png"></a></li>
                                        <li class="list-inline-item"><a href="#">Ali Tufan</a></li>
                                    </ul>
                                    <a class="fp_pdate float-right text-thm" href="#">Read More <span
                                            class="flaticon-next"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-6">
                        <div class="for_blog feat_property">
                            <div class="thumb">
                                <img class="img-whp" src="images/blog/2.jpg" alt="2.jpg">
                                <div class="tag">Construction</div>
                            </div>
                            <div class="details">
                                <div class="tc_content">
                                    <h4>Housing Markets That Changed the Most This Decade</h4>
                                    <ul class="bpg_meta">
                                        <li class="list-inline-item"><a href="#"><i class="flaticon-calendar"></i></a>
                                        </li>
                                        <li class="list-inline-item"><a href="#">January 16, 2020</a></li>
                                    </ul>
                                    <p>Lorem ipsum dolor sit amet, consectetur text link libero tempus congue.</p>
                                </div>
                                <div class="fp_footer">
                                    <ul class="fp_meta float-left mb0">
                                        <li class="list-inline-item"><a href="#"><img src="images/property/pposter1.png"
                                                    alt="pposter1.png"></a></li>
                                        <li class="list-inline-item"><a href="#">Ali Tufan</a></li>
                                    </ul>
                                    <a class="fp_pdate float-right text-thm" href="#">Read More <span
                                            class="flaticon-next"></span></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="sidebar_search_widget">
                    <div class="blog_search_widget">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search Here"
                                aria-label="Recipient's username" aria-describedby="button-addon2">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="button" id="button-addon2"><span
                                        class="flaticon-magnifying-glass"></span></button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="terms_condition_widget">
                    <h4 class="title">Categories Property</h4>
                    <div class="widget_list">
                        <ul class="list_details">
                            <li><a href="#"><i class="fa fa-caret-right mr10"></i>Apartment <span class="float-right">6
                                        properties</span></a></li>
                            <li><a href="#"><i class="fa fa-caret-right mr10"></i>Condo <span class="float-right">12
                                        properties</span></a></li>
                            <li><a href="#"><i class="fa fa-caret-right mr10"></i>Family House <span
                                        class="float-right">8 properties</span></a></li>
                            <li><a href="#"><i class="fa fa-caret-right mr10"></i>Modern Villa <span
                                        class="float-right">26 properties</span></a></li>
                            <li><a href="#"><i class="fa fa-caret-right mr10"></i>Town House <span
                                        class="float-right">89 properties</span></a></li>
                        </ul>
                    </div>
                </div>
                <div class="sidebar_feature_listing">
                    <h4 class="title">Featured Listings</h4>
                    <div class="media">
                        <img class="align-self-start mr-3" src="images/blog/fls1.jpg" alt="fls1.jpg">
                        <div class="media-body">
                            <h5 class="mt-0 post_title">Nice Room With View</h5>
                            <a href="#">$13,000/<small>/mo</small></a>
                            <ul class="mb0">
                                <li class="list-inline-item">Beds: 4</li>
                                <li class="list-inline-item">Baths: 2</li>
                                <li class="list-inline-item">Sq Ft: 5280</li>
                            </ul>
                        </div>
                    </div>
                    <div class="media">
                        <img class="align-self-start mr-3" src="images/blog/fls2.jpg" alt="fls2.jpg">
                        <div class="media-body">
                            <h5 class="mt-0 post_title">Villa called Archangel</h5>
                            <a href="#">$13,000<small>/mo</small></a>
                            <ul class="mb0">
                                <li class="list-inline-item">Beds: 4</li>
                                <li class="list-inline-item">Baths: 2</li>
                                <li class="list-inline-item">Sq Ft: 5280</li>
                            </ul>
                        </div>
                    </div>
                    <div class="media">
                        <img class="align-self-start mr-3" src="images/blog/fls3.jpg" alt="fls3.jpg">
                        <div class="media-body">
                            <h5 class="mt-0 post_title">Sunset Studio</h5>
                            <a href="#">$13,000<small>/mo</small></a>
                            <ul class="mb0">
                                <li class="list-inline-item">Beds: 4</li>
                                <li class="list-inline-item">Baths: 2</li>
                                <li class="list-inline-item">Sq Ft: 5280</li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="blog_tag_widget">
                    <h4 class="title">Tags</h4>
                    <ul class="tag_list">
                        <li class="list-inline-item"><a href="#">Apartment</a></li>
                        <li class="list-inline-item"><a href="#">Real Estate</a></li>
                        <li class="list-inline-item"><a href="#">Estate</a></li>
                        <li class="list-inline-item"><a href="#">Luxury</a></li>
                        <li class="list-inline-item"><a href="#">Real</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection