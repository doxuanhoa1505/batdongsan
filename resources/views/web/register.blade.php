@extends('web.layout.master')
@section('content')
<!-- Our LogIn Register -->
<section class="our-log-reg bgc-fa">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-lg-6 offset-lg-3">
                <div class="sign_up_form inner_page">
                    <div class="heading">
                        <h3 class="text-center">Đăng ký</h3>
                        <p class="text-center">Có tài khoản? <a class="text-thm" href="{{ route('login')}}">Đăng
                                nhập</a></p>
                    </div>
                    <div class="details">
                    <form action="{{ route('postregister')}}" method="post" enctype="multipart/form-data">
                        <?php
                            $message = Session::get('message');
                            if($message){
                                echo '<span class="text-alert">'.$message.'</span>';
                                Session::put('message',null);
                            }
                        ?>{{ csrf_field() }}
                            <div class="form-group">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Nhập Tên" required="required"
                                        name="user_name">
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Nhập email"
                                        required="required" name="user_email">
                                </div>
                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Nhập password"
                                        required="required" name="user_password">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Nhập địa chỉ của bạn"
                                        required="required" name="user_address">
                                </div>

                                <div class="form-group">
                                    <input type="date" class="form-control" name="user_birthday" required="required">
                                </div>
                                <div class="form-group">
                                    <select id="gioitinh" name="user_sex" class="form-control">
                                        <option value="Nam">Nam</option>
                                        <option value="Nữ">Nữ</option>
                                        <option value="Khác">Khác</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Nhập số điện thoại"
                                        name="user_phone" required="required">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Địa chỉ Facebook"
                                        required="required" name="facebook">
                                </div>
                                <div class="form-group">
                                    <input type="file" class="form-control" name="user_img">
                                </div>

                                <button type="submit" class="btn btn-log btn-block btn-thm2">Register</button>
                                <div class="divide">
                                    <span class="lf_divider">Or</span>
                                    <hr>
                                </div>
                                <div class="row mt40">
                                    <div class="col-lg">
                                        <button type="submit" class="btn btn-block color-white bgc-fb mb0"><i
                                                class="fa fa-facebook float-left mt5"></i> Facebook</button>
                                    </div>
                                    <div class="col-lg">
                                        <button type="submit" class="btn btn2 btn-block color-white bgc-gogle mb0"><i
                                                class="fa fa-google float-left mt5"></i> Google</button>
                                    </div>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection