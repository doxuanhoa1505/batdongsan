@extends('web.layout.master')
@section('content')
<!-- Agent Listing Grid View -->
<section class="our-agent-listing bgc-f7 pb30-991">
    <div class="container">

        <div class="row">
            <div class="col-lg-6">
                <div class="breadcrumb_content style2 mb0-991">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{ route('home') }}">Trang chủ</a></li>
                        <li class="breadcrumb-item active text-thm" aria-current="page">Đại lý</li>
                    </ol>
                    <h2 class="breadcrumb_title">Đại lý</h2>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-4 col-xl-4">
                <div class="sidebar_listing_grid1 dn-991">
                    <div class="sidebar_listing_list">
                        <div class="sidebar_advanced_search_widget">
                            <h4 class="mb25">Find Agent</h4>
                            <ul class="sasw_list mb0">
                                <li class="search_area">
                                    <div class="form-group">
                                        <input type="text" class="form-control" id="exampleInputName1"
                                            placeholder="Enter Agent Name">
                                    </div>
                                </li>
                                <li>
                                    <div class="search_option_two">
                                        <div class="candidate_revew_select">
                                            <select class="selectpicker w100 show-tick">
                                                <option>All Categories</option>
                                                <option>Apartment</option>
                                                <option>Bungalow</option>
                                                <option>Condo</option>
                                                <option>House</option>
                                                <option>Land</option>
                                                <option>Single Family</option>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="search_option_two">
                                        <div class="candidate_revew_select">
                                            <select class="selectpicker w100 show-tick">
                                                <option>All Cities</option>
                                                <option>Atlanta</option>
                                                <option>Florida</option>
                                                <option>Los Angeles</option>
                                                <option>Miami</option>
                                                <option>New York</option>
                                                <option>Orlando</option>
                                            </select>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="search_option_button">
                                        <button type="submit" class="btn btn-block btn-thm">Search</button>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="terms_condition_widget">
                        <h4 class="title">Bất động sản mới nhất</h4>
                        <div class="sidebar_feature_property_slider">
                            @foreach($information as $key=>$item)
                            <div class="item">
                                <div class="feat_property home7 agent">
                                    <div class="thumb">
                                        <img class="img-whp" src="uploads/information/{{ $item->inf_img }}"
                                            alt="{{ $item->inf_img }}">
                                        <div class="thmb_cntnt">
                                            <ul class="tag mb0">
                                                <li class="list-inline-item"><a href="#">{{ $item->inf_to_own }}</a>
                                                </li>
                                            </ul>
                                            <a class="fp_price" href="#">${{ $item->inf_price }}</a>
                                            <h4 class="posr color-white">{{ $item->inf_name }}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="terms_condition_widget">
                        <h4 class="title">Bất động sản</h4>
                        <div class="widget_list">
                            <ul class="list_details">
                                @foreach($typeasset as $item)
                                <li><a href="{{route('category',$item->type_id)}}">
                                        <i class="fa fa-caret-right mr10"></i>{{ $item->type_name }}</a></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-8">
                <div class="row">
                    @foreach($user as $key=>$item)
                    <a href="{{ route('user',[$item->user_id,$item->user_name])}}">
                        <div class="col-lg-12">
                            <div class="feat_property list style2 agent">
                                <div class="thumb">
                                    <img class="img-whp" src="uploads/user/{{ $item->user_img }}"
                                        alt="{{ $item->user_img }}" style="height:165px">
                                    <!-- <div class="thmb_cntnt">
										<ul class="tag mb0">
											<li class="list-inline-item dn"></li>
											<li class="list-inline-item"><a href="#">2 Listings</a></li>
										</ul>
									</div> -->
                                </div>
                                <div class="details">
                                    <div class="tc_content">
                                        <h4>{{ $item->user_name }}</h4>
                                        <!-- <p class="text-thm">Agent</p> -->
                                        <ul class="prop_details mb0">
                                            <li><a href="#">Điện thoại:{{ $item->user_phone }}</a></li>
                                            <li><a href="#">Địa chỉ: {{ $item->user_address }}</a></li>
                                            <li><a href="#">Email: {{ $item->user_email }}</a></li>
                                        </ul>
                                    </div>
                                    <div class="fp_footer">
                                        <ul class="fp_meta float-left mb0">
                                            <li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a>
                                            </li>
                                            <li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a>
                                            </li>
                                            <li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a>
                                            </li>
                                            <li class="list-inline-item"><a href="#"><i class="fa fa-pinterest"></i></a>
                                            </li>
                                            <li class="list-inline-item"><a href="#"><i class="fa fa-google"></i></a>
                                            </li>
                                        </ul>
                                        <a href="{{ route('user',[$item->user_id,$item->user_name])}}" class="fp_pdate float-right text-thm">Xem danh sách của tôi <i
                                                class="fa fa-angle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endsection