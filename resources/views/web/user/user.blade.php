@extends('web.layout.master')
@section('content')
<section class="our-agent-single bgc-f7 pb30-991">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="listing_sidebar dn db-991">
						<div class="sidebar_content_details style3">
							<!-- <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a> -->
							<div class="sidebar_listing_list style2 mobile_sytle_sidebar mb0">
								<div class="sidebar_advanced_search_widget">
									<h4 class="mb25"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">tìm kiếm nâng cao </font></font><a class="filter_closed_btn float-right" href="#"><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ẩn bộ lọc</font></font></small> <span class="flaticon-close"></span></a></h4>
									<ul class="sasw_list style2 mb0">
										<li class="search_area">
										    <div class="form-group">
										    	<input type="text" class="form-control" id="exampleInputName1" placeholder="từ khóa">
										    	<label for="exampleInputEmail"><span class="flaticon-magnifying-glass"></span></label>
										    </div>
										</li>
										<li class="search_area">
										    <div class="form-group">
										    	<input type="text" class="form-control" id="exampleInputEmail" placeholder="Vị trí">
										    	<label for="exampleInputEmail"><span class="flaticon-maps-and-flags"></span></label>
										    </div>
										</li>
										<li>
											<div class="search_option_two">
												<div class="candidate_revew_select">
													<div class="dropdown bootstrap-select w100 show-tick"><select class="selectpicker w100 show-tick" tabindex="-98">
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Trạng thái</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ, chung cư</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Bungalow</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">nhà ở</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đất đai</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Một gia đình</font></font></option>
													</select><button type="button" class="btn dropdown-toggle btn-light" data-toggle="dropdown" role="button" title="Trạng thái"><div class="filter-option"><div class="filter-option-inner"><div class="filter-option-inner-inner"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Trạng thái</font></font></div></div> </div></button><div class="dropdown-menu " role="combobox"><div class="inner show" role="listbox" aria-expanded="false" tabindex="-1"><ul class="dropdown-menu inner show"></ul></div></div></div>
												</div>
											</div>
										</li>
										<li>
											<div class="search_option_two">
												<div class="candidate_revew_select">
													<div class="dropdown bootstrap-select w100 show-tick"><select class="selectpicker w100 show-tick" tabindex="-98">
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Loại tài sản</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ, chung cư</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Bungalow</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">nhà ở</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đất đai</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Một gia đình</font></font></option>
													</select><button type="button" class="btn dropdown-toggle btn-light" data-toggle="dropdown" role="button" title="Loại tài sản"><div class="filter-option"><div class="filter-option-inner"><div class="filter-option-inner-inner"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Loại tài sản</font></font></div></div> </div></button><div class="dropdown-menu " role="combobox"><div class="inner show" role="listbox" aria-expanded="false" tabindex="-1"><ul class="dropdown-menu inner show"></ul></div></div></div>
												</div>
											</div>
										</li>
										<li>
											<div class="small_dropdown2">
											    <div id="prncgs" class="btn dd_btn">
											    	<span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Giá bán</font></font></span>
											    	<label for="exampleInputEmail2"><span class="fa fa-angle-down"></span></label>
											    </div>
											  	<div class="dd_content2">
												    <div class="pricing_acontent">
														<input type="text" class="amount" placeholder="$ 52.239"> 
														<input type="text" class="amount2" placeholder="$ 985,14">
														<div class="slider-range ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"><div class="ui-slider-range ui-corner-all ui-widget-header" style="left: 2.79875%; width: 57.8438%;"></div><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 2.79875%;"></span><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 60.6425%;"></span></div>
												    </div>
											  	</div>
											</div>
										</li>
										<li>
											<div class="search_option_two">
												<div class="candidate_revew_select">
													<div class="dropdown bootstrap-select w100 show-tick"><select class="selectpicker w100 show-tick" tabindex="-98">
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phòng tắm</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">5</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">6</font></font></option>
													</select><button type="button" class="btn dropdown-toggle btn-light" data-toggle="dropdown" role="button" title="Phòng tắm"><div class="filter-option"><div class="filter-option-inner"><div class="filter-option-inner-inner"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phòng tắm</font></font></div></div> </div></button><div class="dropdown-menu " role="combobox"><div class="inner show" role="listbox" aria-expanded="false" tabindex="-1"><ul class="dropdown-menu inner show"></ul></div></div></div>
												</div>
											</div>
										</li>
										<li>
											<div class="search_option_two">
												<div class="candidate_revew_select">
													<div class="dropdown bootstrap-select w100 show-tick"><select class="selectpicker w100 show-tick" tabindex="-98">
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phòng ngủ</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">1</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">3</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">5</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">6</font></font></option>
													</select><button type="button" class="btn dropdown-toggle btn-light" data-toggle="dropdown" role="button" title="Phòng ngủ"><div class="filter-option"><div class="filter-option-inner"><div class="filter-option-inner-inner"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phòng ngủ</font></font></div></div> </div></button><div class="dropdown-menu " role="combobox"><div class="inner show" role="listbox" aria-expanded="false" tabindex="-1"><ul class="dropdown-menu inner show"></ul></div></div></div>
												</div>
											</div>
										</li>
										<li>
											<div class="search_option_two">
												<div class="candidate_revew_select">
													<div class="dropdown bootstrap-select w100 show-tick"><select class="selectpicker w100 show-tick" tabindex="-98">
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nhà để xe</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đúng</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Không</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Khác</font></font></option>
													</select><button type="button" class="btn dropdown-toggle btn-light" data-toggle="dropdown" role="button" title="Nhà để xe"><div class="filter-option"><div class="filter-option-inner"><div class="filter-option-inner-inner"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nhà để xe</font></font></div></div> </div></button><div class="dropdown-menu " role="combobox"><div class="inner show" role="listbox" aria-expanded="false" tabindex="-1"><ul class="dropdown-menu inner show"></ul></div></div></div>
												</div>
											</div>
										</li>
										<li>
											<div class="search_option_two">
												<div class="candidate_revew_select">
													<div class="dropdown bootstrap-select w100 show-tick"><select class="selectpicker w100 show-tick" tabindex="-98">
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Năm xây dựng</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2013</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2014</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2015</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2016</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2017</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2018</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2019</font></font></option>
														<option><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2020</font></font></option>
													</select><button type="button" class="btn dropdown-toggle btn-light" data-toggle="dropdown" role="button" title="Năm xây dựng"><div class="filter-option"><div class="filter-option-inner"><div class="filter-option-inner-inner"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Năm xây dựng</font></font></div></div> </div></button><div class="dropdown-menu " role="combobox"><div class="inner show" role="listbox" aria-expanded="false" tabindex="-1"><ul class="dropdown-menu inner show"></ul></div></div></div>
												</div>
											</div>
										</li>
										<li class="min_area style2 list-inline-item">
										    <div class="form-group">
										    	<input type="text" class="form-control" id="exampleInputName2" placeholder="Khu vực tối thiểu">
										    </div>
										</li>
										<li class="max_area list-inline-item">
										    <div class="form-group">
										    	<input type="text" class="form-control" id="exampleInputName3" placeholder="Diện tích tối đa">
										    </div>
										</li>
										<li>
										  	<div id="accordion" class="panel-group">
											    <div class="panel">
											      	<div class="panel-heading">
												      	<h4 class="panel-title">
												        	<a href="#panelBodyRating" class="accordion-toggle link" data-toggle="collapse" data-parent="#accordion"><i class="flaticon-more"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Các tính năng tiên tiến</font></font></a>
												        </h4>
											      	</div>
												    <div id="panelBodyRating" class="panel-collapse collapse">
												        <div class="panel-body row">
												      		<div class="col-lg-12">
												                <ul class="ui_kit_checkbox selectable-list float-left fn-400">
												                	<li>
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox" class="custom-control-input" id="customCheck1">
																			<label class="custom-control-label" for="customCheck1"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Điều hòa nhiệt độ</font></font></label>
																		</div>
												                	</li>
												                	<li>
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox" class="custom-control-input" id="customCheck4">
																			<label class="custom-control-label" for="customCheck4"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đồ nướng</font></font></label>
																		</div>
												                	</li>
												                	<li>
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox" class="custom-control-input" id="customCheck10">
																			<label class="custom-control-label" for="customCheck10"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phòng thể dục</font></font></label>
																		</div>
												                	</li>
												                	<li>
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox" class="custom-control-input" id="customCheck5">
																			<label class="custom-control-label" for="customCheck5"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Lò vi sóng</font></font></label>
																		</div>
												                	</li>
												                	<li>
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox" class="custom-control-input" id="customCheck6">
																			<label class="custom-control-label" for="customCheck6"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cáp TV</font></font></label>
																		</div>
												                	</li>
												                	<li>
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox" class="custom-control-input" id="customCheck2">
																			<label class="custom-control-label" for="customCheck2"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cừu con</font></font></label>
																		</div>
												                	</li>
												                	<li>
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox" class="custom-control-input" id="customCheck11">
																			<label class="custom-control-label" for="customCheck11"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tủ lạnh</font></font></label>
																		</div>
												                	</li>
												                	<li>
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox" class="custom-control-input" id="customCheck3">
																			<label class="custom-control-label" for="customCheck3"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Hồ bơi</font></font></label>
																		</div>
												                	</li>
												                </ul>
												                <ul class="ui_kit_checkbox selectable-list float-right fn-400">
												                	<li>
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox" class="custom-control-input" id="customCheck12">
																			<label class="custom-control-label" for="customCheck12"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Wifi</font></font></label>
																		</div>
												                	</li>
												                	<li>
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox" class="custom-control-input" id="customCheck14">
																			<label class="custom-control-label" for="customCheck14"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tắm hơi</font></font></label>
																		</div>
												                	</li>
												                	<li>
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox" class="custom-control-input" id="customCheck7">
																			<label class="custom-control-label" for="customCheck7"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Máy sấy khô</font></font></label>
																		</div>
												                	</li>
												                	<li>
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox" class="custom-control-input" id="customCheck9">
																			<label class="custom-control-label" for="customCheck9"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Máy giặt</font></font></label>
																		</div>
												                	</li>
												                	<li>
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox" class="custom-control-input" id="customCheck13">
																			<label class="custom-control-label" for="customCheck13"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Giặt ủi</font></font></label>
																		</div>
												                	</li>
												                	<li>
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox" class="custom-control-input" id="customCheck8">
																			<label class="custom-control-label" for="customCheck8"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Vòi tắm ngoài trời</font></font></label>
																		</div>
												                	</li>
												                	<li>
																		<div class="custom-control custom-checkbox">
																			<input type="checkbox" class="custom-control-input" id="customCheck15">
																			<label class="custom-control-label" for="customCheck15"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Che cửa sổ</font></font></label>
																		</div>
												                	</li>
												                </ul>
													        </div>
												        </div>
												    </div>
											    </div>
											</div>
										</li>
										<li>
											<div class="search_option_button">
											    <button type="submit" class="btn btn-block btn-thm"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tìm kiếm</font></font></button>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 col-lg-8">
					<div class="row">
						<div class="col-lg-12">
							<div class="breadcrumb_content style2 mt30-767 mb30-767">
								<ol class="breadcrumb">
								    <li class="breadcrumb-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Trang Chủ</font></font></a></li>
								    <li class="breadcrumb-item active text-thm" aria-current="page"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đại lý đơn</font></font></li>
								</ol>
								<h2 class="breadcrumb_title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Christopher Pakulla</font></font></h2>
							</div>
							<div class="dn db-991 mt30 mb0">
								<div id="main2">
									<span id="open2" class="flaticon-filter-results-button filter_open_btn style3"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> Hiển thị bộ lọc</font></font></span>
								</div>
							</div>
						</div>
						<div class="col-lg-12">
							<div class="feat_property list style2 agent">
								<div class="thumb">
									<img class="img-whp" src="images/team/11.jpg" alt="Anh hai 11.jpg">
									<div class="thmb_cntnt">
										<ul class="tag mb0">
											<li class="list-inline-item dn"></li>
											<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">2 danh sách</font></font></a></li>
										</ul>
									</div>
								</div>
								<div class="details">
									<div class="tc_content">
										<h4><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Christopher Pakulla</font></font></h4>
										<p class="text-thm"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đặc vụ</font></font></p>
										<ul class="prop_details mb0">
											<li><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Văn phòng: 134 456 3210</font></font></a></li>
											<li><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Di động: 891 456 9874</font></font></a></li>
											<li><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Fax: 342 654 1258</font></font></a></li>
											<li><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Email: </font></font><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">pakulla@findhouse.com</font></font></a></li>
										</ul>
									</div>
									<div class="fp_footer">
										<ul class="fp_meta float-left mb0">
											<li class="list-inline-item"><a href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-twitter"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-instagram"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-pinterest"></i></a></li>
											<li class="list-inline-item"><a href="#"><i class="fa fa-google"></i></a></li>
										</ul>
										<div class="fp_pdate float-right text-thm"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Xem danh sách của tôi </font></font><i class="fa fa-angle-right"></i></div>
									</div>
								</div>
							</div>
							<div class="shop_single_tab_content style2 mt30">
								<ul class="nav nav-tabs" id="myTab" role="tablist">
									<li class="nav-item">
									    <a class="nav-link active" id="description-tab" data-toggle="tab" href="#description" role="tab" aria-controls="description" aria-selected="true"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sự miêu tả</font></font></a>
									</li>
									<li class="nav-item">
									    <a class="nav-link" id="listing-tab" data-toggle="tab" href="#listing" role="tab" aria-controls="listing" aria-selected="false"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Danh sách</font></font></a>
									</li>
									<li class="nav-item">
									    <a class="nav-link" id="review-tab" data-toggle="tab" href="#review" role="tab" aria-controls="review" aria-selected="false"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nhận xét</font></font></a>
									</li>
								</ul>
								<div class="tab-content" id="myTabContent2">
									<div class="tab-pane fade show active" id="description" role="tabpanel" aria-labelledby="description-tab">
										<div class="product_single_content">
											<div class="mbp_pagination_comments">
												<div class="mbp_first media">
													<div class="media-body">
												    	<p class="mb25"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Evans Tower có nhu cầu rất cao ở góc nhỏ một phòng ngủ cộng với ban công lớn với tầm nhìn toàn cảnh NYC rộng mở. </font><font style="vertical-align: inherit;">Bạn cần phải xem các quan điểm để tin họ. </font><font style="vertical-align: inherit;">Tình trạng bạc hà với sàn gỗ cứng mới. </font><font style="vertical-align: inherit;">Rất nhiều tủ quần áo cộng với máy giặt và máy sấy.</font></font></p>
												    	<p class="mt10 mb0"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đầy đủ nội thất. </font><font style="vertical-align: inherit;">Đơn vị chung cư được bổ nhiệm trang nhã nằm trên vị trí hàng đầu. </font><font style="vertical-align: inherit;">PS6. </font><font style="vertical-align: inherit;">Sảnh vào rộng dẫn đến phòng khách lớn với khu vực ăn uống. </font><font style="vertical-align: inherit;">Căn hộ 2 phòng ngủ rộng rãi và 2 phòng tắm lát đá cẩm thạch đã được tân trang lại này có cửa sổ lớn. </font><font style="vertical-align: inherit;">Mặc dù có tầm nhìn bên trong, các căn hộ tiếp xúc phía Nam và phía Đông cho phép ánh sáng tự nhiên đáng yêu tràn ngập mọi phòng. </font><font style="vertical-align: inherit;">Bộ tổng thể được bao quanh bởi các tác phẩm thủ công và có tủ quần áo không cửa ngăn và không gian lưu trữ đáng kinh ngạc.</font></font></p>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane fade row pl15 pl0-1199 pr15 pr0-1199" id="listing" role="tabpanel" aria-labelledby="listing-tab">
										<div class="col-lg-12">
											<div class="feat_property list style2 hvr-bxshd bdrrn mb10 mt20">
												<div class="thumb">
													<img class="img-whp" src="images/property/fp1.jpg" alt="fp1.jpg">
													<div class="thmb_cntnt">
														<ul class="icon mb0">
															<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
															<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
														</ul>
													</div>
												</div>
												<div class="details">
													<div class="tc_content">
														<div class="dtls_headr">
															<ul class="tag">
																<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cho thuê</font></font></a></li>
																<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đặc sắc</font></font></a></li>
															</ul>
															<a class="fp_price" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 13,000 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">/ tháng</font></font></small></a>
														</div>
														<p class="text-thm"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ, chung cư</font></font></p>
														<h4><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ đã được Cải tạo</font></font></h4>
														<p><span class="flaticon-placeholder"></span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> 1421 San Pedro St, Los Angeles, CA 90015</font></font></p>
														<ul class="prop_details mb0">
															<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Số giường: 4</font></font></a></li>
															<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phòng tắm: 2</font></font></a></li>
															<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sq Ft: 5280</font></font></a></li>
														</ul>
													</div>
													<div class="fp_footer">
														<ul class="fp_meta float-left mb0">
															<li class="list-inline-item"><a href="#"><img src="images/property/pposter1.png" alt="pposter1.png"></a></li>
															<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ali Tufan</font></font></a></li>
														</ul>
														<div class="fp_pdate float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4 năm trước</font></font></div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="feat_property list style2 hvr-bxshd bdrrn mb10">
												<div class="thumb">
													<img class="img-whp" src="images/property/fp3.jpg" alt="fp3.jpg">
													<div class="thmb_cntnt">
														<ul class="icon mb0">
															<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
															<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
														</ul>
													</div>
												</div>
												<div class="details">
													<div class="tc_content">
														<div class="dtls_headr">
															<ul class="tag">
																<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cho thuê</font></font></a></li>
																<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đặc sắc</font></font></a></li>
															</ul>
															<a class="fp_price" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 13,000 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">/ tháng</font></font></small></a>
														</div>
														<p class="text-thm"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ, chung cư</font></font></p>
														<h4><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nhà gia đình sang trọng</font></font></h4>
														<p><span class="flaticon-placeholder"></span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> 1421 San Pedro St, Los Angeles, CA 90015</font></font></p>
														<ul class="prop_details mb0">
															<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Số giường: 4</font></font></a></li>
															<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phòng tắm: 2</font></font></a></li>
															<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sq Ft: 5280</font></font></a></li>
														</ul>
													</div>
													<div class="fp_footer">
														<ul class="fp_meta float-left mb0">
															<li class="list-inline-item"><a href="#"><img src="images/property/pposter1.png" alt="pposter1.png"></a></li>
															<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ali Tufan</font></font></a></li>
														</ul>
														<div class="fp_pdate float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4 năm trước</font></font></div>
													</div>
												</div>
											</div>
										</div>
										<div class="col-lg-12">
											<div class="feat_property list style2 hvr-bxshd bdrrn">
												<div class="thumb">
													<img class="img-whp" src="images/property/fp2.jpg" alt="fp2.jpg">
													<div class="thmb_cntnt">
														<ul class="icon mb0">
															<li class="list-inline-item"><a href="#"><span class="flaticon-transfer-1"></span></a></li>
															<li class="list-inline-item"><a href="#"><span class="flaticon-heart"></span></a></li>
														</ul>
													</div>
												</div>
												<div class="details">
													<div class="tc_content">
														<div class="dtls_headr">
															<ul class="tag">
																<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cho thuê</font></font></a></li>
																<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đặc sắc</font></font></a></li>
															</ul>
															<a class="fp_price" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 13,000 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">/ tháng</font></font></small></a>
														</div>
														<p class="text-thm"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ, chung cư</font></font></p>
														<h4><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Biệt thự Tuyệt đẹp Hướng Vịnh</font></font></h4>
														<p><span class="flaticon-placeholder"></span><font style="vertical-align: inherit;"><font style="vertical-align: inherit;"> 1421 San Pedro St, Los Angeles, CA 90015</font></font></p>
														<ul class="prop_details mb0">
															<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Số giường: 4</font></font></a></li>
															<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phòng tắm: 2</font></font></a></li>
															<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sq Ft: 5280</font></font></a></li>
														</ul>
													</div>
													<div class="fp_footer">
														<ul class="fp_meta float-left mb0">
															<li class="list-inline-item"><a href="#"><img src="images/property/pposter1.png" alt="pposter1.png"></a></li>
															<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ali Tufan</font></font></a></li>
														</ul>
														<div class="fp_pdate float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">4 năm trước</font></font></div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="tab-pane fade" id="review" role="tabpanel" aria-labelledby="review-tab">
										<div class="product_single_content">
											<div class="mbp_pagination_comments">
												<ul class="total_reivew_view">
													<li class="list-inline-item sub_titles">896 Reviews</li>
													<li class="list-inline-item">
														<ul class="star_list">
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
															<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
														</ul>
													</li>
													<li class="list-inline-item avrg_review">( 4.5 out of 5 )</li>
													<li class="list-inline-item write_review">Write a Review</li>
												</ul>
												<div class="mbp_first media">
													<img src="images/testimonial/1.png" class="mr-3" alt="1.png">
													<div class="media-body">
												    	<h4 class="sub_title mt-0">Diana Cooper
															<div class="sspd_review dif">
																<ul class="ml10">
																	<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
																	<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
																	<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
																	<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
																	<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
																	<li class="list-inline-item"></li>
																</ul>
															</div>
												    	</h4>
												    	<a class="sspd_postdate fz14" href="#">December 28, 2020</a>
												    	<p class="mt10">Beautiful home, very picturesque and close to everything in jtree! A little warm for a hot weekend, but would love to come back during the cooler seasons!</p>
													</div>
												</div>
												<div class="custom_hr"></div>
												<div class="mbp_first media">
													<img src="images/testimonial/2.png" class="mr-3" alt="2.png">
													<div class="media-body">
												    	<h4 class="sub_title mt-0">Ali Tufan
															<div class="sspd_review dif">
																<ul class="ml10">
																	<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
																	<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
																	<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
																	<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
																	<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
																	<li class="list-inline-item"></li>
																</ul>
															</div>
												    	</h4>
												    	<a class="sspd_postdate fz14" href="#">December 28, 2020</a>
												    	<p class="mt10">Beautiful home, very picturesque and close to everything in jtree! A little warm for a hot weekend, but would love to come back during the cooler seasons!</p>
													</div>
												</div>
												<div class="custom_hr"></div>
												<div class="mbp_comment_form style2">
													<h4>Write a Review</h4>
													<ul class="sspd_review">
														<li class="list-inline-item">
															<ul class="mb0">
																<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
																<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
																<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
																<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
																<li class="list-inline-item"><a href="#"><i class="fa fa-star"></i></a></li>
															</ul>
														</li>
														<li class="list-inline-item review_rating_para">Your Rating &amp; Review</li>
													</ul>
													<form class="comments_form">
														<div class="form-group">
													    	<input type="text" class="form-control" id="exampleInputName1" aria-describedby="textHelp" placeholder="Review Title">
														</div>
														<div class="form-group">
														    <textarea class="form-control" id="exampleFormControlTextarea1" rows="12" placeholder="Your Review"></textarea>
														</div>
														<button type="submit" class="btn btn-thm">Submit Review <span class="flaticon-right-arrow-1"></span></button>
													</form>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-xl-4">
					<div class="sidebar_listing_grid1 dn-991">
						<div class="sidebar_listing_list">
							<div class="sidebar_advanced_search_widget">
								<h4 class="mb25"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Liên hệ với Christopher Pakulla</font></font></h4>
								<ul class="sasw_list mb0">
									<li class="search_area">
									    <div class="form-group">
									    	<input type="text" class="form-control" id="exampleInputName1" placeholder="Tên của bạn">
									    </div>
									</li>
									<li class="search_area">
									    <div class="form-group">
									    	<input type="number" class="form-control" id="exampleInputName2" placeholder="Điện thoại">
									    </div>
									</li>
									<li class="search_area">
									    <div class="form-group">
									    	<input type="email" class="form-control" id="exampleInputEmail" placeholder="E-mail">
									    </div>
									</li>
									<li class="search_area">
			                            <div class="form-group">
			                                <textarea id="form_message" name="form_message" class="form-control required" rows="5" required="required" placeholder="Tin nhắn của bạn"></textarea>
			                            </div>
									</li>
									<li>
										<div class="search_option_button">
										    <button type="submit" class="btn btn-block btn-thm"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Tìm kiếm</font></font></button>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div class="terms_condition_widget">
							<h4 class="title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Các thuộc tính</font></font></h4>
							<div class="sidebar_feature_property_slider owl-carousel owl-theme owl-loaded owl-hidden">
								
								
								
								
								
							<div class="owl-stage-outer"><div class="owl-stage" style="transform: translate3d(-30px, 0px, 0px); transition: all 0s ease 0s; width: 135px;"><div class="owl-item cloned" style="width: 0px; margin-right: 15px;"><div class="item">
									<div class="feat_property home7 agent">
										<div class="thumb">
											<img class="img-whp" src="images/property/fp4.jpg" alt="fp4.jpg">
											<div class="thmb_cntnt">
												<ul class="tag mb0">
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cho thuê</font></font></a></li>
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đặc sắc</font></font></a></li>
												</ul>
												<a class="fp_price" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 13,000 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">/ tháng</font></font></small></a>
												<h4 class="posr color-white"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ đã được Cải tạo</font></font></h4>
											</div>
										</div>
									</div>
								</div></div><div class="owl-item cloned" style="width: 0px; margin-right: 15px;"><div class="item">
									<div class="feat_property home7 agent">
										<div class="thumb">
											<img class="img-whp" src="images/property/fp5.jpg" alt="fp5.jpg">
											<div class="thmb_cntnt">
												<ul class="tag mb0">
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cho thuê</font></font></a></li>
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đặc sắc</font></font></a></li>
												</ul>
												<a class="fp_price" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 13,000 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">/ tháng</font></font></small></a>
												<h4 class="posr color-white"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ đã được Cải tạo</font></font></h4>
											</div>
										</div>
									</div>
								</div></div><div class="owl-item active" style="width: 0px; margin-right: 15px;"><div class="item">
									<div class="feat_property home7 agent">
										<div class="thumb">
											<img class="img-whp" src="images/property/fp1.jpg" alt="fp1.jpg">
											<div class="thmb_cntnt">
												<ul class="tag mb0">
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cho thuê</font></font></a></li>
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đặc sắc</font></font></a></li>
												</ul>
												<a class="fp_price" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 13,000 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">/ tháng</font></font></small></a>
												<h4 class="posr color-white"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ đã được Cải tạo</font></font></h4>
											</div>
										</div>
									</div>
								</div></div><div class="owl-item" style="width: 0px; margin-right: 15px;"><div class="item">
									<div class="feat_property home7 agent">
										<div class="thumb">
											<img class="img-whp" src="images/property/fp2.jpg" alt="fp2.jpg">
											<div class="thmb_cntnt">
												<ul class="tag mb0">
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cho thuê</font></font></a></li>
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đặc sắc</font></font></a></li>
												</ul>
												<a class="fp_price" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 13,000 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">/ tháng</font></font></small></a>
												<h4 class="posr color-white"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ đã được Cải tạo</font></font></h4>
											</div>
										</div>
									</div>
								</div></div><div class="owl-item" style="width: 0px; margin-right: 15px;"><div class="item">
									<div class="feat_property home7 agent">
										<div class="thumb">
											<img class="img-whp" src="images/property/fp3.jpg" alt="fp3.jpg">
											<div class="thmb_cntnt">
												<ul class="tag mb0">
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cho thuê</font></font></a></li>
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đặc sắc</font></font></a></li>
												</ul>
												<a class="fp_price" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 13,000 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">/ tháng</font></font></small></a>
												<h4 class="posr color-white"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ đã được Cải tạo</font></font></h4>
											</div>
										</div>
									</div>
								</div></div><div class="owl-item" style="width: 0px; margin-right: 15px;"><div class="item">
									<div class="feat_property home7 agent">
										<div class="thumb">
											<img class="img-whp" src="images/property/fp4.jpg" alt="fp4.jpg">
											<div class="thmb_cntnt">
												<ul class="tag mb0">
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cho thuê</font></font></a></li>
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đặc sắc</font></font></a></li>
												</ul>
												<a class="fp_price" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 13,000 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">/ tháng</font></font></small></a>
												<h4 class="posr color-white"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ đã được Cải tạo</font></font></h4>
											</div>
										</div>
									</div>
								</div></div><div class="owl-item" style="width: 0px; margin-right: 15px;"><div class="item">
									<div class="feat_property home7 agent">
										<div class="thumb">
											<img class="img-whp" src="images/property/fp5.jpg" alt="fp5.jpg">
											<div class="thmb_cntnt">
												<ul class="tag mb0">
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cho thuê</font></font></a></li>
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đặc sắc</font></font></a></li>
												</ul>
												<a class="fp_price" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 13,000 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">/ tháng</font></font></small></a>
												<h4 class="posr color-white"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ đã được Cải tạo</font></font></h4>
											</div>
										</div>
									</div>
								</div></div><div class="owl-item cloned" style="width: 0px; margin-right: 15px;"><div class="item">
									<div class="feat_property home7 agent">
										<div class="thumb">
											<img class="img-whp" src="images/property/fp1.jpg" alt="fp1.jpg">
											<div class="thmb_cntnt">
												<ul class="tag mb0">
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cho thuê</font></font></a></li>
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đặc sắc</font></font></a></li>
												</ul>
												<a class="fp_price" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 13,000 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">/ tháng</font></font></small></a>
												<h4 class="posr color-white"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ đã được Cải tạo</font></font></h4>
											</div>
										</div>
									</div>
								</div></div><div class="owl-item cloned" style="width: 0px; margin-right: 15px;"><div class="item">
									<div class="feat_property home7 agent">
										<div class="thumb">
											<img class="img-whp" src="images/property/fp2.jpg" alt="fp2.jpg">
											<div class="thmb_cntnt">
												<ul class="tag mb0">
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Cho thuê</font></font></a></li>
													<li class="list-inline-item"><a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đặc sắc</font></font></a></li>
												</ul>
												<a class="fp_price" href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 13,000 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">/ tháng</font></font></small></a>
												<h4 class="posr color-white"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ đã được Cải tạo</font></font></h4>
											</div>
										</div>
									</div>
								</div></div></div></div><div class="owl-controls"><div class="owl-nav"><div class="owl-prev" style=""><i class="flaticon-left-arrow-1"></i></div><div class="owl-next" style=""><i class="flaticon-right-arrow"></i></div></div><div class="owl-dots" style=""><div class="owl-dot active"><span></span></div><div class="owl-dot"><span></span></div><div class="owl-dot"><span></span></div><div class="owl-dot"><span></span></div><div class="owl-dot"><span></span></div></div></div></div>
						</div>
						<div class="terms_condition_widget">
							<h4 class="title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Danh mục tài sản</font></font></h4>
							<div class="widget_list">
								<ul class="list_details">
									<li><a href="#"><i class="fa fa-caret-right mr10"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ, chung cư </font></font><span class="float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">6 chỗ nghỉ</font></font></span></a></li>
									<li><a href="#"><i class="fa fa-caret-right mr10"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Căn hộ </font></font><span class="float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">12 chỗ nghỉ</font></font></span></a></li>
									<li><a href="#"><i class="fa fa-caret-right mr10"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Ngôi nhà gia đình </font></font><span class="float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">8 chỗ nghỉ</font></font></span></a></li>
									<li><a href="#"><i class="fa fa-caret-right mr10"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Biệt thự hiện đại </font></font><span class="float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">26 chỗ nghỉ</font></font></span></a></li>
									<li><a href="#"><i class="fa fa-caret-right mr10"></i><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Nhà phố </font></font><span class="float-right"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">89 chỗ nghỉ</font></font></span></a></li>
								</ul>
							</div>
						</div>
						<div class="sidebar_feature_listing">
							<h4 class="title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Đã xem gần đây</font></font></h4>
							<div class="media">
								<img class="align-self-start mr-3" src="images/blog/fls1.jpg" alt="fls1.jpg">
								<div class="media-body">
							    	<h5 class="mt-0 post_title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phòng đẹp có tầm nhìn</font></font></h5>
							    	<a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 13,000 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">/ tháng</font></font></small></a>
							    	<ul class="mb0">
							    		<li class="list-inline-item"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Số giường: 4</font></font></li>
							    		<li class="list-inline-item"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phòng tắm: 2</font></font></li>
							    		<li class="list-inline-item"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sq Ft: 5280</font></font></li>
							    	</ul>
								</div>
							</div>
							<div class="media">
								<img class="align-self-start mr-3" src="images/blog/fls2.jpg" alt="fls2.jpg">
								<div class="media-body">
							    	<h5 class="mt-0 post_title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Biệt thự có tên Archangel</font></font></h5>
							    	<a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 13,000 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">/ tháng</font></font></small></a>
							    	<ul class="mb0">
							    		<li class="list-inline-item"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Số giường: 4</font></font></li>
							    		<li class="list-inline-item"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phòng tắm: 2</font></font></li>
							    		<li class="list-inline-item"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sq Ft: 5280</font></font></li>
							    	</ul>
								</div>
							</div>
							<div class="media">
								<img class="align-self-start mr-3" src="images/blog/fls3.jpg" alt="fls3.jpg">
								<div class="media-body">
							    	<h5 class="mt-0 post_title"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Studio Sunset</font></font></h5>
							    	<a href="#"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">$ 13,000 </font></font><small><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">/ tháng</font></font></small></a>
							    	<ul class="mb0">
							    		<li class="list-inline-item"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Số giường: 4</font></font></li>
							    		<li class="list-inline-item"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Phòng tắm: 2</font></font></li>
							    		<li class="list-inline-item"><font style="vertical-align: inherit;"><font style="vertical-align: inherit;">Sq Ft: 5280</font></font></li>
							    	</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection