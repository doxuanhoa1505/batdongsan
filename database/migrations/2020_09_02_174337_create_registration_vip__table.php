<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistrationVipTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('registration_vip', function (Blueprint $table) {
            $table->increments('regi_vip_id');
            $table->string('user_id');
            $table->string('vip_id');
            $table->string('regi_vip_time_on');
            $table->string('regi_vip_time_off');
            $table->string('regi_vip_stt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('registration_vip');
    }
}
