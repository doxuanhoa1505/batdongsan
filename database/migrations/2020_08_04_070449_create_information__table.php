<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInformationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('information', function (Blueprint $table) {
            $table->increments('inf_id');
            $table->string('inf_name');
            $table->string('id_type');
            $table->string('id_user');
            $table->string('inf_address');
            $table->string('inf_to_own');
            $table->string('inf_des');
            $table->string('inf_price');
            $table->string('inf_bedrooms')->nullable();
            $table->string('inf_bathrooms')->nullable();
            $table->string('inf_garage')->nullable();
            $table->string('inf_garage_size')->nullable();
            $table->string('inf_property_size')->nullable();
            $table->string('inf_year_built')->nullable();
            $table->string('inf_last_remodel_year')->nullable();
            $table->string('inf_pool_size')->nullable();
            $table->string('inf_like')->nullable();
            $table->string('inf_air_conditioning')->nullable();
            $table->string('inf_barbeque')->nullable();
            $table->string('inf_dryer')->nullable();
            $table->string('inf_gym')->nullable();
            $table->string('inf_laundry')->nullable();
            $table->string('inf_microwave')->nullable();
            $table->string('inf_outdoor_shower')->nullable();
            $table->string('inf_refrigerator')->nullable();
            $table->string('inf_sauna')->nullable();
            $table->string('inf_swimming_pool')->nullable();
            $table->string('inf_tv')->nullable();
            $table->string('inf_washer')->nullable();
            $table->string('inf_wifi')->nullable();
            $table->string('inf_window')->nullable();
            $table->string('inf_img');
            $table->string('inf_stt')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('information');
    }
}
