<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('user_name');
            $table->string('user_birthday');
            $table->string('user_address');
            $table->string('id_vip')->nullable();
            $table->string('vip_stt')->nullable();
            $table->string('user_email');
            $table->string('user_password');
            $table->string('user_phone');
            $table->string('user_sex');
            $table->string('user_img');
            $table->string('time_vip')->nullable();
            $table->string('facebook')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
