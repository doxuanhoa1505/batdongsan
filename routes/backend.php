<?php

use Illuminate\Support\Facades\Route;


 



//admin
Route::get('/admin', 'AdminController@index');
Route::get('/dashboard', 'AdminController@show_dashboard');
Route::get('/logoutadmin','AdminController@logout');
Route::post('admin-dashboard','AdminController@dashboard');
Route::get('/addadmin', 'AdminController@addadmin');
Route::post('save-admin','AdminController@save_admin');

//typeblog
Route::get('/add-typeblog','TypeBlogController@add_typeblog');
Route::get('/edit-typeblog/{id}','TypeBlogController@edit_typeblog');
Route::get('/delete-typeblog/{id}','TypeBlogController@delete_typeblog');
Route::get('/all-typeblog','TypeBlogController@all_typeblog');
Route::get('/unactive-typeblog/{id}','TypeBlogController@unactive_typeblog');
Route::get('/active-typeblog/{id}','TypeBlogController@active_typeblog');
Route::post('save-typeblog','TypeBlogController@save_typeblog');
Route::post('/admin/update-typeblog/{id}','TypeBlogController@update_typeblog');
//blog
Route::get('/add-blog','BlogController@add_blog');
Route::get('/edit-blog/{id}','BlogController@edit_blog');
Route::get('/delete-blog/{id}','BlogController@delete_blog');
Route::get('/all-blog','BlogController@all_blog');
Route::get('/unactive-blog/{id}','BlogController@unactive_blog');
Route::get('/active-blog/{id}','BlogController@active_blog');
Route::post('save-blog','BlogController@save_blog');
Route::post('update-blog/{id}','BlogController@update_blog');
//slider
Route::get('/add-slider','SliderController@add_slider');
Route::get('/edit-slider/{id}','SliderController@edit_slider');
Route::get('/delete-slider/{id}','SliderController@delete_slider');
Route::get('/all-slider','SliderController@all_slider');
Route::get('/unactive-slider/{id}','SliderController@unactive_slider');
Route::get('/active-slider/{id}','SliderController@active_slider');
Route::post('save-slider','SliderController@save_slider');
Route::post('update-slider/{id}','SliderController@update_slider');
//partnerships
Route::get('/add-partnerships','PartnershipsController@add_partnerships');
Route::get('/edit-partnerships/{id}','PartnershipsController@edit_partnerships');
Route::get('/delete-partnerships/{id}','PartnershipsController@delete_partnerships');
Route::get('/all-partnerships','PartnershipsController@all_partnerships');
Route::get('/unactive-partnerships/{id}','PartnershipsController@unactive_partnerships');
Route::get('/active-partnerships/{id}','PartnershipsController@active_partnerships');
Route::post('save-partnerships','PartnershipsController@save_partnerships');
Route::post('update-partnerships/{id}','PartnershipsController@update_partnerships');
//typeasset
Route::get('/add-typeasset','TypeassetController@add_typeasset');
Route::get('/edit-typeasset/{id}','TypeassetController@edit_typeasset');
Route::get('/delete-typeasset/{id}','TypeassetController@delete_typeasset');
Route::get('/all-typeasset','TypeassetController@all_typeasset');
Route::get('/unactive-typeasset/{id}','TypeassetController@unactive_typeasset');
Route::get('/active-typeasset/{id}','TypeassetController@active_typeasset');
Route::post('save-typeasset','TypeassetController@save_typeasset');
Route::post('update-typeasset/{id}','TypeassetController@update_typeasset');
//information
Route::get('/add-information','InformationController@add_information');
Route::get('/edit-information/{id}','InformationController@edit_information');
Route::get('/delete-information/{id}','InformationController@delete_information');
Route::get('/all-information','InformationController@all_information');
Route::get('/unactive-information/{id}','InformationController@unactive_information');
Route::get('/active-information/{id}','InformationController@active_information');
Route::post('save-information','InformationController@save_information');
Route::post('update-information/{id}','InformationController@update_information');
Route::get('/xem-chi-tiet/{id}','InformationController@detailinf');
//information_image
Route::get('/add-information_image','InformationImageController@add_information_image');
Route::get('/edit-information_image/{id}','InformationImageController@edit_information_image');
Route::get('/delete-information_image/{id}','InformationImageController@delete_information_image');
Route::get('/all-information_image','InformationImageController@all_information_image');
Route::get('/unactive-information_image/{id}','InformationImageController@unactive_information_image');
Route::get('/active-information_image/{id}','InformationImageController@active_information_image');
Route::post('save-information_image','InformationImageController@save_information_image');
Route::post('update-information_image/{id}','InformationImageController@update_information_image');
//user
Route::get('/add-user','UserController@add_user');
Route::get('/edit-user/{id}','UserController@edit_user');
Route::get('/delete-user/{id}','UserController@delete_user');
Route::get('/all-user','UserController@all_user');
Route::get('/unactive-user/{id}','UserController@unactive_user');
Route::get('/active-user/{id}','UserController@active_user');
Route::post('save-user','UserController@save_user');
Route::post('update-user/{id}','UserController@update_user');
Route::get('/sent-user-email/{id}','UserController@detailuser');
//messenger
Route::get('/add-messenger','MessengerController@add_messenger');
Route::get('/edit-messenger/{id}','MessengerController@edit_messenger');
Route::get('/delete-messenger/{id}','MessengerController@delete_messenger');
Route::get('/all-messenger','MessengerController@all_messenger');
Route::post('save-messenger','MessengerController@save_messenger');
Route::post('update-messenger/{id}','MessengerController@update_messenger');
Route::get('/xem-chi-tiet-email/{id}','MessengerController@detailemail');
Route::get('/unactive-messenger/{id}','MessengerController@unactive_messenger');
Route::get('/active-messenger/{id}','MessengerController@active_messenger');
Route::get('/sent-email-user/{id}','MessengerController@detailuser');
//typevip
Route::get('/add-typevip','TypevipController@add_typevip');
Route::get('/edit-typevip/{id}','TypevipController@edit_typevip');
Route::get('/delete-typevip/{id}','TypevipController@delete_typevip');
Route::get('/all-typevip','TypevipController@all_typevip');
Route::get('/unactive-typevip/{id}','TypevipController@unactive_typevip');
Route::get('/active-typevip/{id}','TypevipController@active_typevip');
Route::post('save-typevip','TypevipController@save_typevip');
Route::post('/update-typevip/{id}','TypevipController@update_typevip');
//registration_vip
Route::get('/add-registration_vip','RegistrationvipController@add_vip');
Route::get('/edit-registration_vip/{id}','RegistrationvipController@edit_vip');
Route::get('/delete-registration_vip/{id}','RegistrationvipController@delete_vip');
Route::get('/all-registration_vip','RegistrationvipController@all_vip');
Route::get('/unactive-registration_vip/{id}','RegistrationvipController@unactive_vip');
Route::get('/active-registration_vip/{id}','RegistrationvipController@active_vip');
Route::post('save-registration_vip','RegistrationvipController@save_vip');
Route::post('/update-registration_vip/{id}','RegistrationvipController@update_vip');