<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
//registration_vip
Route::get('/add-registration_vip','Registration_vipController@add_vip');
Route::get('/edit-registration_vip/{id}','Registration_vipController@edit_vip');
Route::get('/delete-registration_vip/{id}','Registration_vipController@delete_vip');
Route::get('/all-registration_vip','Registration_vipController@all_vip');
Route::get('/unactive-registration_vip/{id}','Registration_vipController@unactive_vip');
Route::get('/active-registration_vip/{id}','Registration_vipController@active_vip');
Route::post('save-registration_vip','Registration_vipController@save_vip');
Route::post('/update-registration_vip/{id}','Registration_vipController@update_vip');
