<?php

// Route::get('/', function () {
//     return view('website.layout.index');
// });

Route::get('/login', 'LoginController@index')->name('login');
Route::get('/logoutuser','LoginController@logout')->name('logout');
Route::post('/user-dashboard','LoginController@dashboard');
Route::get('/register', 'LoginController@adduser')->name('register');
Route::post('/register','LoginController@postUser')->name('postregister');
//home
Route::get('/','HomeController@index')->name('home');
//all category
Route::get('nha-dat','HomeController@typeasset')->name('allcategory');
//category
Route::get('nha-dat/{type}','HomeController@getTyppeasset')->name('category');
//detail product
Route::get('/{id}{inf_name}/chitiet','HomeController@getChitiet')->name('chitiet');
//all blog
Route::get('/tin-tuc','HomeController@blog')->name('blog');
//blog type
Route::get('/tin-tuc/{type}','HomeController@getBlog')->name('blogtype');
// detail blog
Route::get('/{blog_name}/{id}/chitiet','HomeController@detailBlog')->name('detailBlog');
//all user
Route::get('dai-ly','HomeController@allUser')->name('alluser');
//product user
Route::get('dai-ly/{id}{user_name}','HomeController@user')->name('user');
